<?php

namespace spec\App\Sso;

use App\Sso\AmqpEventHandlerWrapper;
use Multisys\AmqpEventbusLibrary\EventHandler;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use SmartPldt\Deep\Sso\Event\HandlesAnEventMessage;

/**
 * @mixin AmqpEventHandlerWrapper
 */
class AmqpEventHandlerWrapperSpec extends ObjectBehavior
{
    private static function createRandomCrc32Hash(): string
    {
        return \hash('crc32', \random_bytes(4));
    }

    function let(HandlesAnEventMessage $ssoEventHandler)
    {
        $ssoEventHandler->eventNameToHandle()->willReturn('party-' . self::createRandomCrc32Hash());
        $ssoEventHandler->__invoke(Argument::type('string'));

        $this->beConstructedWith($ssoEventHandler);
    }

    function it_should_be_an_EventHandler_from_amqp_eventbus_library()
    {
        $this->shouldHaveType(EventHandler::class);
    }

    function it_should_return_the_event_name_it_handles_relative_to_the_sso_event_handler_it_wrapped(
        HandlesAnEventMessage $ssoEventHandler
    )
    {
        $ssoEventHandler->eventNameToHandle()->shouldBeCalledOnce()->willReturn($event = 'concert.ng.idol.mo');
        $this->eventNameToHandle()->shouldBe($event);
    }

    function it_should_delegate_method_calls_Sso_event_handler(HandlesAnEventMessage $ssoEventHandler)
    {
        $this($encodedEvent = 'Ang-laman-ng-payload');

        $ssoEventHandler->__invoke($encodedEvent)->shouldHaveBeenCalledOnce();
    }
}

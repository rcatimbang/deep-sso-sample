<?php

namespace spec\App\Sso;

use App\Sso\SendRegistrationOtp;
use PhpSpec\ObjectBehavior;
use PhpSpec\Wrapper\Collaborator;
use Psr\Container\ContainerInterface;
use SmartPldt\Deep\Sso\CanonicalUserId\Min;
use SmartPldt\Deep\Sso\CanonicalUserId\PlainSha256;
use SmartPldt\Deep\Sso\Event\HandlesAnEventMessage;
use SmartPldt\Deep\Sso\MranWasNominated;
use SmartPldt\Deep\Sso\UserService;
use SmartPldt\Deep\Sso\VerificationCode\Service;
use SmartPldt\Deep\Sso\VerificationCode\Transport;
use spec\App\ObjectBehaviorHelper;

/**
 * @mixin SendRegistrationOtp
 */
class SendRegistrationOtpSpec extends ObjectBehavior
{
    use ObjectBehaviorHelper;

    function let(
        ContainerInterface $container,
        UserService $userService,
        Service $verificationCodeService,
        Transport $verificationCodeTransport
    ) {
        $container->get(UserService::class)->shouldBeCalledOnce()->willReturn($userService);
        $container->get(Service::class)->shouldBeCalledOnce()->willReturn($verificationCodeService);
        $container->get(Transport::class)->shouldBeCalledOnce()->willReturn($verificationCodeTransport);

        $this->beConstructedThrough('fromContainer', [$container]);
    }

    function it_should_be_an_event_handler()
    {
        $this->shouldHaveType(HandlesAnEventMessage::class);
    }

    function it_should_return_the_event_name_it_handles()
    {
        $this->eventNameToHandle()->shouldBe('pldt-smart.deep.sso.mran_was_nominated');
    }

    function it_should_send_an_otp(
        UserService $userService,
        Transport $verificationCodeTransport,
        Service $verificationCodeService
    ) {
        $eventAsArray = \json_decode(self::eventAsJson(), true);
        $canonicalUserId = Min::create($eventAsArray['body']['payload']['mran'], PlainSha256::create());
        $userService->createCanonicalUserId($eventAsArray['body']['payload']['mran'])->willReturn($canonicalUserId);
        $verificationCodeService->generateCodeFor($canonicalUserId)->willReturn($code = '111111');
        $verificationCodeTransport->send($canonicalUserId, $code)->shouldBeCalledOnce();

        $event = self::createMranWasNominatedEvent($eventAsArray, $userService);

        $this((string)$event);
    }

    private static function createMranWasNominatedEvent(array $eventAsArray, Collaborator $userService): MranWasNominated
    {
        return MranWasNominated::fromArray(
            \json_decode(self::eventAsJson(), true),
            $userService->getWrappedObject()
        );
    }

    private static function eventAsJson(): string
    {
        return <<<JSON
        {
          "event_name": "pldt-smart.deep.sso.mran_was_nominated",
          "aggregate_id": "d36f1628-1021-4189-9683-37ac9821fa20",
          "occurred_on": 1597243711,
          "body": {
            "envelope": [],
            "payload": {
              "mran": "09190003331"
            }
          }
        }
        JSON;
    }
}

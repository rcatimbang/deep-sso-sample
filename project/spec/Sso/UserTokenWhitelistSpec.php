<?php

namespace spec\App\Sso;

use App\Sso\UserTokenWhitelist;
use Illuminate\Contracts\Cache\Repository as Cache;
use PhpSpec\ObjectBehavior;
use SmartPldt\Deep\Sso\UserTokenWhitelist as BaseTokenWhitelist;

/**
 * @mixin UserTokenWhitelist
 */
class UserTokenWhitelistSpec extends ObjectBehavior
{
    function let(Cache $cache)
    {
        $this->beConstructedWith($cache);
    }

    function it_should_be_a_UserTokenWhitelist()
    {
        $this->shouldHaveType(BaseTokenWhitelist::class);
    }

    function it_should_whitelist_a_given_user_access_token(Cache $cache)
    {
        $this->set($userId = 'kunware-user-id', $userToken = 'kunwareng-token');

        $cache->put(\sprintf('user-token-whitelist-for-%s', $userId),\hash('crc32', $userToken))
            ->shouldHaveBeenCalledOnce();
    }

    function it_should_return_true_if_token_is_whitelisted(Cache $cache)
    {
        $userId = 'kunware-user-id';
        $tokenHash = \hash('crc32', $userToken = 'kunwareng-token');
        $cache->get(\sprintf('user-token-whitelist-for-%s', $userId))->shouldBeCalledOnce()->willReturn($tokenHash);

        $this->isWhiteListed($userId, $userToken)->shouldBe(true);
    }

    function it_should_return_false_if_token_is_not_whitelisted(Cache $cache)
    {
        $userId = 'kunware-user-id';
        $tokenHash = \hash('crc32', $userToken = 'kunwareng-token');
        $cache->get(\sprintf('user-token-whitelist-for-%s', $userId))->shouldBeCalledOnce()->willReturn('maleng-hash');

        $this->isWhiteListed($userId, $userToken)->shouldBe(false);
    }
}

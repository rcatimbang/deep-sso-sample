<?php

namespace spec\App\Sso;

use App\Sso\RedisEventBus;
use App\Sso\UserService;
use Interop\Queue\Consumer;
use Interop\Queue\Context;
use Interop\Queue\Message;
use Interop\Queue\Producer;
use Interop\Queue\Topic;
use PhpSpec\ObjectBehavior;
use SmartPldt\Deep\Sso\CanonicalUserId\Min;
use SmartPldt\Deep\Sso\CanonicalUserId\PlainSha256;
use SmartPldt\Deep\Sso\Event\Exception;
use SmartPldt\Deep\Sso\Event\HandlesAnEventMessage;
use SmartPldt\Deep\Sso\Hash\Argon2idPassword;
use SmartPldt\Deep\Sso\Hash\PasswordHistory;
use SmartPldt\Deep\Sso\UserId;
use SmartPldt\Deep\Sso\UserWasRegistered;
use spec\App\ObjectBehaviorHelper;

/**
 * @mixin RedisEventBus
 */
class RedisEventBusSpec extends ObjectBehavior
{
    use ObjectBehaviorHelper;

    function let(Context $context)
    {
        $this->beConstructedWith($context);
    }

    function it_should_send_DomainEvent_as_message(
        UserService $userService,
        Context $context,
        Topic $topic,
        Message $message,
        Producer $producer
    ) {
        $event = $this->createObjectBuilderFor(UserWasRegistered::class)
            ->set('userId', UserId::fromString('de960b8a-1021-4189-9683-37ac9821fa20'))
            ->set('password', Argon2idPassword::fromPasswordHash(
                '$argon2id$v=19$m=65536,t=4,p=1$MjI4enhOdWlqQVdQNkdmRw$up21SANf1UkUjZfrbdPZfgOVgSAMqSDNToKzV5/cIAk'
            ))
            ->set('canonicalUserId', Min::create('09191112220', PlainSha256::create()))
            ->set('occurredOn', \microtime(true))
            ->set(
                'passwordHistory',
                PasswordHistory::fromArray(5, [$this->randomCrc32Hash(), $this->randomCrc32Hash()])
            )
            ->build()
        ;

        $context->createTopic($event->name())->shouldBeCalledOnce()->willReturn($topic);
        $context->createMessage($event->__toString())->shouldBeCalledOnce()->willReturn($message);

        $context->createProducer()->shouldBeCalledOnce()->willReturn($producer);
        $producer->send($topic, $message)->shouldBeCalledOnce();

        $this->send($event);
    }

    function it_should_re_throw_any_exception_as_event_Exception(Context $context, HandlesAnEventMessage $handleEvent)
    {
        $eventName = 'birthday-ng-tatay-mo';
        $error = new \Exception('Di natuloy birthday!');
        $context->createTopic($eventName)->shouldBeCalledOnce()->willThrow($error);

        $this->shouldThrow(Exception::class)->during('receive', [$eventName, $handleEvent]);
    }

    function it_should_receive_encoded_DomainEvent_message(
        Context $context,
        Topic $topic,
        Consumer $consumer,
        Message $message,
        HandlesAnEventMessage $handleEvent
    ) {
        $eventName = 'birthday-ng-tatay-mo-na-di-natuloy';
        $context->createTopic($eventName)->shouldBeCalledOnce()->willReturn($topic);
        $context->createConsumer($topic)->shouldBeCalledOnce()->willReturn($consumer);

        $consumer->receive(5)->shouldBeCalledTimes(3)->willReturn($message, $message, null);
        $messageBodies = ['nagwala tatay mo', 'kaya tinuloy kinabukasan'];
        $message->getBody()->shouldBeCalledTimes(2)->willReturn(...$messageBodies);

        $handleEvent->__invoke($messageBodies[0])->shouldBeCalled();
        $handleEvent->__invoke($messageBodies[1])->shouldBeCalled();

        $consumer->acknowledge($message)->shouldBeCalledTimes(2);

        $this->receive($eventName, $handleEvent);
    }
}

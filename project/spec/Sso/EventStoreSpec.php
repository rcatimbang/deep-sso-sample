<?php

namespace spec\App\Sso;

use App\Sso\ElasticsearchCriteria;
use App\Sso\EventStore;
use App\Sso\RangeCriteriaParam;
use App\Sso\TermCriteriaParam;
use Elasticsearch\Client;
use Elasticsearch\Namespaces\IndicesNamespace;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Psr\Container\ContainerInterface;
use SmartPldt\Deep\Sso\CanonicalUserId\CanonicalUserId;
use SmartPldt\Deep\Sso\CanonicalUserId\Email;
use SmartPldt\Deep\Sso\CanonicalUserId\Min;
use SmartPldt\Deep\Sso\CanonicalUserId\PlainSha256;
use SmartPldt\Deep\Sso\Criteria;
use SmartPldt\Deep\Sso\DomainEvent;
use SmartPldt\Deep\Sso\DomainEventFactory;
use SmartPldt\Deep\Sso\DomainEvents;
use SmartPldt\Deep\Sso\EncryptionService;
use SmartPldt\Deep\Sso\EventStore as BaseEventStore;
use SmartPldt\Deep\Sso\Hash\PasswordHistoryHash;
use SmartPldt\Deep\Sso\UserFailedToLogin;
use SmartPldt\Deep\Sso\UserId;
use SmartPldt\Deep\Sso\UserService;
use SmartPldt\Deep\Sso\UserWasRegistered;
use SmartPldt\Deep\Sso\SnapshotWasCreated;

/**
 * @mixin EventStore
 */
class EventStoreSpec extends ObjectBehavior
{
    function let(
        ContainerInterface $container,
        Client $esClient,
        EncryptionService $encryptionService,
        UserService $userService,
        IndicesNamespace $indicesNamespace
    ) {
        $userService->createCanonicalUserId(Email::create(
            'roy@gmail.com',
            PlainSha256::fromHash('a591a6d40bf420404a011733cfb7b190d62c65bf0bcda32b57b277d9ad9f146e')
        ));
        $domainEventFactory = (new DomainEventFactory($userService->getWrappedObject()))
            ->addSignature(UserFailedToLogin::class)
            ->addSignature(UserWasRegistered::class)
            ->addSignature(SnapshotWasCreated::class);

        $esClient->indices()->willReturn($indicesNamespace);
        $indicesNamespace->refresh()->willReturn([]);

        $container->get('service.sso.domain_event_factory')->willReturn($domainEventFactory);
        $container->get('service.sso.elasticsearch_client')->willReturn($esClient);
        $container->get(EncryptionService::class)->willReturn($encryptionService);
        $container->get('parameter.sso.event_store_index')->willReturn('sso_events');
        $container->get('parameter.sso.dormancy_in_days')->willReturn(90);

        $this->beConstructedWith($container);
    }

    function it_is_an_EventStore()
    {
        $this->shouldHaveType(BaseEventStore::class);
    }

    function it_should_save_DomainEvent(Client $esClient, EncryptionService $encryptionService, DomainEvent $event)
    {
        $eventAsArray = [
            'event_name' => 'pldt-smart.deep.sso.user_failed_to_login',
            'aggregate_id' => 'de960b8a-1021-4189-9683-37ac9821fa20',
            'occurred_on' => 1594030687,
            'body' => [
                'envelope' => [],
                'payload' => ['login_failure_code' => 2]
            ]
        ];
        $event->__toString()->shouldBeCalledOnce()->willReturn($eventAsJson = \json_encode($eventAsArray));
        $encryptionService
            ->encrypt('{"envelope":[],"payload":{"login_failure_code":2}}')
            ->shouldBeCalledOnce()
            ->willReturn($eventAsArray['body'] = 'abot-tenga-ngiti-ni-csog-dito');
        $esClient
            ->index(['index' => 'sso_events', 'refresh' => 'wait_for', 'body' => $eventAsArray])
            ->shouldBeCalled()
            ->willReturn(['result' => 'created']);

        $this->save($event);
    }

    function it_should_return_DomainEvents_by_AggregateId(Client $esClient, EncryptionService $encryptionService)
    {
        $aggregateId = UserId::fromString('de960b8a-1021-4189-9683-37ac9821fa20');
        $lastSnapshotTimestampQuery = self::lastSnapshotTimestampQuery('sso_events', (string)$aggregateId);
        $esClient->search($lastSnapshotTimestampQuery)->shouldBeCalledOnce()->willReturn(
            self::lastSnapshotResult(
                $lastSnapshot = 1594030111,
                $snapshotEncrypted = 'magagalit-si-csog-dito'
            )
        );

        $esQueryParameters = self::findByAggregateIdQuery('sso_events', (string)$aggregateId, $lastSnapshot);
        $esClient->search($esQueryParameters)->shouldBeCalledOnce()->willReturn([
            'hits' => [
                'hits' => [
                    [
                        '_source' => [
                            'event_name' => 'pldt-smart.deep.sso.user_failed_to_login',
                            'aggregate_id' => 'de960b8a-1021-4189-9683-37ac9821fa20',
                            'occurred_on' => 1594030687,
                            'body' => $kunwaringEncrypted = 'magagalit-si-csog-dito'
                        ]
                    ]
                ]
            ]
        ]);

        $encryptionService
            ->decrypt($kunwaringEncrypted)
            ->shouldBeCalledOnce()->willReturn('{"envelope":[],"payload":{"login_failure_code":2}}');

        $this->find($aggregateId)->shouldContainType(UserFailedToLogin::class);
    }

    function it_should_return_DomainEvents_by_CanonicalUserId(
        Client $esClient,
        EncryptionService $encryptionService,
        CanonicalUserId $canonicalUserId
    ) {
        $aggregateId = UserId::fromString('de960b8a-1021-4189-9683-37ac9821fa20');
        $canonicalUserId->hash()->shouldBeCalledOnce()->willReturn(
            $hash = 'a591a6d40bf420404a011733cfb7b190d62c65bf0bcda32b57b277d9ad9f146e'
        );
        $canonicalUserIdParameters = [
            'index' => 'sso_events',
            'size' => 5000,
            'body' => [
                'query' => [ 'term' => [ 'canonical_user_id_hash' => $hash ] ]
            ]
        ];

        $esClient->search($canonicalUserIdParameters)->shouldBeCalledOnce()->willReturn([
            'hits' => [
                'hits' => [
                    [
                        '_source' => [
                            'aggregate_id' => 'de960b8a-1021-4189-9683-37ac9821fa20',
                        ]
                    ]
                ]
            ]
        ]);

        $lastSnapshotTimestampQuery = self::lastSnapshotTimestampQuery('sso_events', (string)$aggregateId);
        $esClient->search($lastSnapshotTimestampQuery)->shouldBeCalledOnce()->willReturn(
            self::lastSnapshotResult(
                $lastSnapshot = 1594030111,
                $snapshotEncrypted = 'magagalit-si-csog-dito'
            )
        );

        $esQueryParameters = self::findByAggregateIdQuery('sso_events', (string)$aggregateId, $lastSnapshot);
        $esClient->search($esQueryParameters)->shouldBeCalledOnce()->willReturn([
            'hits' => [
                'hits' => [
                    [
                        '_source' => [
                            'event_name' => 'pldt-smart.deep.sso.user_was_registered',
                            'aggregate_id' => 'de960b8a-1021-4189-9683-37ac9821fa20',
                            'canonical_user_id_hash' => $hash,
                            'occurred_on' => 1594030687,
                            'body' => $kunwaringEncrypted = 'magagalit-si-csog-dito'
                        ]
                    ],
                    [
                        '_source' => [
                            'event_name' => 'pldt-smart.deep.sso.user_failed_to_login',
                            'aggregate_id' => 'de960b8a-1021-4189-9683-37ac9821fa20',
                            'occurred_on' => 1594030687,
                            'body' => $kunwaringEncrypted = 'magagalit-si-csog-dito'
                        ]
                    ]
                ]
            ]
        ]);
        $passwordHash = '$argon2id$v=19$m=65536,t=4,p=1$MjI4enhOdWlqQVdQNkdmRw$up21SANf1UkUjZfrbdPZfgOVgSAMqSDNToKzV5'
            . '/cIAk';
        $encryptionService
            ->decrypt($kunwaringEncrypted)
            ->shouldBeCalledTimes(2)->willReturn(
                \sprintf(
                    '{"envelope":[],"payload":{"canonical_user_id":"roy@gmail.com","password_hash":"%s", '
                    . '"password_history": ["%s", "%s"]}}',
                    $passwordHash,
                    new PasswordHistoryHash('9>WrUdT`'),
                    new PasswordHistoryHash('F}Bt6.he'),
                ),
                '{"envelope":[],"payload":{"login_failure_code":2}}'
            );
        $this->findByCanonicalUserId($canonicalUserId)->shouldContainType(UserWasRegistered::class);
    }

    function it_should_return_dormant_domain_user_ids(Client $esClient)
    {
        $result = [
            'hits' => ['hits' => [
                ['_source' => ['aggregate_id' => '5d824069-b76b-4099-ab4a-81f79f024217']],
                ['_source' => ['aggregate_id' => '8e7d8bd0-eb5d-48a2-9ea3-dd3721cb744d']],
            ]],
        ];
        $esClient->search(Argument::type('array'))->shouldBeCalledOnce()->willReturn($result);

        $dormantUserIds = $this->dormantUserIds();
        $dormantUserIds->shouldContainLike(
            UserId::fromString('5d824069-b76b-4099-ab4a-81f79f024217'),
            UserId::fromString('8e7d8bd0-eb5d-48a2-9ea3-dd3721cb744d')
        );
    }

    function it_should_purge_events_by_UserId(Client $esClient)
    {
        $userIds = [
            UserId::fromString('5d824069-b76b-4099-ab4a-81f79f024217'),
            UserId::fromString('8e7d8bd0-eb5d-48a2-9ea3-dd3721cb744d'),
        ];

        $esClient->search(self::containsUserWasDeletedEventCriteria($userIds[0]))
            ->shouldBeCalledOnce()
            ->willReturn(['hits' => ['total' => 1]])
        ;
        $esClient->search(self::containsUserWasDeletedEventCriteria($userIds[1]))
            ->shouldBeCalledOnce()
            ->willReturn(['hits' => ['total' => 1]])
        ;
        $esClient->deleteByQuery(self::deleteParams($userIds[0]))->shouldBeCalledOnce();
        $esClient->deleteByQuery(self::deleteParams($userIds[1]))->shouldBeCalledOnce();

        $this->purgeEventsMarkedDeletedByUserIds(...$userIds);
    }

    function it_should_return_events_based_on_criteria_provided(
        Client $esClient,
        EncryptionService $encryptionService
    ) {
        $aggregateId = UserId::fromString('de960b8a-1021-4189-9683-37ac9821fa20');

        $criteria = new ElasticsearchCriteria(
            TermCriteriaParam::equality('aggregate_id', (string)$aggregateId),
            new RangeCriteriaParam('occurred_on', null, 1594030687)
        );

        $esClient->search(Argument::cetera())->shouldBeCalledOnce()->willReturn(
            [
                'hits' => [
                    'hits' => [
                        [
                            '_source' => [
                                'event_name' => 'pldt-smart.deep.sso.user_was_registered',
                                'aggregate_id' => 'de960b8a-1021-4189-9683-37ac9821fa20',
                                'canonical_user_id_hash' => 'a591a6d40bf420404a011733cfb7b190d62c65bf0bcda32b57b277d9ad9f146e',
                                'occurred_on' => 1594030687,
                                'body' => $kunwaringEncrypted = 'magagalit-si-csog-dito'
                            ]
                        ],
                        [
                            '_source' => [
                                'event_name' => 'pldt-smart.deep.sso.user_failed_to_login',
                                'aggregate_id' => 'de960b8a-1021-4189-9683-37ac9821fa20',
                                'occurred_on' => 1594030687,
                                'body' => $kunwaringEncrypted = 'magagalit-si-csog-dito'
                            ]
                        ]
                    ]
                ]
            ]
        );
        $passwordHash = '$argon2id$v=19$m=65536,t=4,p=1$MjI4enhOdWlqQVdQNkdmRw$up21SANf1UkUjZfrbdPZfgOVgSAMqSDNToKzV5'
            . '/cIAk';
        $encryptionService
            ->decrypt($kunwaringEncrypted)
            ->shouldBeCalledTimes(2)->willReturn(
                \sprintf(
                    '{"envelope":[],"payload":{"canonical_user_id":"roy@gmail.com","password_hash":"%s", '
                    . '"password_history": ["%s", "%s"]}}',
                    $passwordHash,
                    new PasswordHistoryHash('9>WrUdT`'),
                    new PasswordHistoryHash('F}Bt6.he'),
                ),
                '{"envelope":[],"payload":{"login_failure_code":2}}'
            );
        $this->findByCriteria($criteria)->shouldContainType(UserWasRegistered::class);
    }

    private static function deleteParams(UserId $userId): array
    {
        return \json_decode(
            <<<PARAMS
            {
                "index": "sso_events",
                "body": {
                    "query": {
                        "bool": {
                            "must": {
                                "term": {
                                    "aggregate_id": "{$userId}"
                                }
                            },
                            "must_not": {
                                "term": {
                                    "event_name": "pldt-smart.deep.sso.user_was_deleted"
                                }
                            }
                        }
                    }
                }
            }
            PARAMS,
            true
        );
    }

    private static function containsUserWasDeletedEventCriteria(UserId $userId): array
    {
        return \json_decode(
            <<<PARAMS
            {
                "index": "sso_events",
                "body": {
                    "query": {
                        "bool": {
                            "must": [
                                {
                                    "term": {
                                        "event_name": "pldt-smart.deep.sso.user_was_deleted"
                                    }
                                },
                                {
                                    "term": {
                                        "aggregate_id": "{$userId}"
                                    }
                                }
                            ]
                        }
                    }
                }
            }
            PARAMS,
            true
        );
    }

    private static function lastSnapshotTimestampQuery(string $index, string $aggregateId): array
    {
        return \json_decode(
            \sprintf(
                <<<ES_QUERY
                {
                    "index": "%s",
                    "body": {
                        "sort": { "occurred_on" : { "order" : "desc" } },
                        "size": 1,
                        "query": {
                            "bool": {
                                "must": [
                                    { "term" : { "aggregate_id": "%s" } },
                                    { "term" : { "event_name": "pldt-smart.deep.sso.snapshot_was_created" } }
                                ]
                            }
                        }
                    }
                }
                ES_QUERY,
                $index,
                $aggregateId
            ),
            true
        );
    }

    private static function findByAggregateIdQuery(string $index, string $aggregateId, float $snapshotTimestamp): array
    {
        return \json_decode(
            \sprintf(
                <<<ES_QUERY
                {
                    "index": "%s",
                    "size": 5000,
                    "body": {
                        "query": {
                            "bool": {
                                "must": [
                                    { "term" : { "aggregate_id": "%s" } },
                                    { "range": { "occurred_on": { "gte": %f } } }
                                ]
                            }
                        },
                        "sort": { "occurred_on" : { "order" : "asc" } }
                    }
                }
                ES_QUERY,
                $index,
                $aggregateId,
                $snapshotTimestamp
            ),
            true
        );
    }

    private static function lastSnapshotResult(float $lastSnapshot, string $snapshotEncrypted): array
    {
        return \json_decode(
            \sprintf(
                <<<JSON
                {
                    "hits":{
                        "hits":[
                                {
                                "_source":{
                                    "event_name":"pldt-smart.deep.sso.snapshot_was_created",
                                    "aggregate_id":"de960b8a-1021-4189-9683-37ac9821fa20",
                                    "occurred_on": %f,
                                    "body": "%s"
                                }
                            }
                        ]
                    }
                }
                JSON,
                $lastSnapshot,
                $snapshotEncrypted
            ),
            true
        );
    }

    function getMatchers(): array
    {
        return [
            'containType' => function($subject, $type) {
                foreach ($subject as $itemFromSubject) {
                    if (\get_class($itemFromSubject) === $type) {
                        return true;
                    }
                }

                return false;
            },
            'containLike' => function($subject, ...$references) {
                foreach ($subject as $item) {
                    return \in_array($item, $references);
                };

                return false;
            }
        ];
    }
}

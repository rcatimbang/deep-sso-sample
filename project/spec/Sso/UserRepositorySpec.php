<?php

namespace spec\App\Sso;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use SmartPldt\Deep\Sso\{CanonicalUserId\Email,
    CanonicalUserId\PlainSha256,
    DomainEvent,
    EventStore,
    User,
    UserId,
    UserRepository as BaseUserRepository};
use SmartPldt\Deep\Sso\Event\EventBus;
use spec\App\ObjectBehaviorHelper;

class UserRepositorySpec extends ObjectBehavior
{
    use ObjectBehaviorHelper;

    function let(EventStore $eventStore, EventBus $eventBus)
    {
        $this->beConstructedWith($eventStore, $eventBus);
    }

    function it_should_be_a_UserRepository()
    {
        $this->shouldHaveType(BaseUserRepository::class);
    }

    function it_should_save_User_changes(DomainEvent $eventA, DomainEvent $eventB, EventStore $eventStore, EventBus $eventBus)
    {
        $user = $this->createFakeInstanceOf(User::class);
        $this->setPropertyOf($user, 'recordedEvents', [$eventA->getWrappedObject(), $eventB->getWrappedObject()]);
        $this->setPropertyOf($user, 'canonicalUserId', Email::create('foo@bar.com', PlainSha256::create()));
        $this->setPropertyOf($user, 'userId', UserId::fromString($this->createRandomAggregateId()));
        $this->setPropertyOf($user, 'nominatedMran', Email::create('foo@bar.com', PlainSha256::create()));

        $eventStore->save(Argument::type(DomainEvent::class))->shouldBeCalledTimes(2);
        $eventBus->send(Argument::type(DomainEvent::class))->shouldBeCalledTimes(2);

        $this->save($user);
    }
}

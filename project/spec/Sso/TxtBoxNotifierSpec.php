<?php

namespace spec\App\Sso;

use App\Sso\TxtBoxNotifier;
use App\Sso\TxtBoxService;
use PhpSpec\ObjectBehavior;
use SmartPldt\Deep\Sso\MinNotificationRecipient;
use SmartPldt\Deep\Sso\Notifier;

/**
 * @mixin TxtBoxNotifier
 */
class TxtBoxNotifierSpec extends ObjectBehavior
{
    function let(TxtBoxService $txtBoxService)
    {
        $this->beConstructedWith($txtBoxService);
    }

    function it_should_be_a_Notifier()
    {
        $this->shouldHaveType(Notifier::class);
    }

    function it_should_send_a_message_to_given_recipient(TxtBoxService $txtBoxService)
    {
        $notificationRecipient = MinNotificationRecipient::create('09191234567');
        $txtBoxService->sendSMS('639191234567', $message = 'Ang mahiwagang mensahe!')->shouldBeCalledOnce();

        $this->notify($notificationRecipient, $message = 'Ang mahiwagang mensahe!');

    }
}

<?php


namespace spec\App\Sso;


use App\Exceptions\InvalidOperatorException;
use App\Sso\ElasticsearchCriteria;
use App\Sso\RangeCriteriaParam;
use App\Sso\TermCriteriaParam;
use PhpSpec\ObjectBehavior;
use SmartPldt\Deep\Sso\Criteria;

/**
 * @mixin ElasticsearchCriteria
 */
class ElasticsearchCriteriaSpec extends ObjectBehavior
{
    private TermCriteriaParam $termCriteria;

    private RangeCriteriaParam $rangeCriteria;

    function let()
    {
        $this->termCriteria =  TermCriteriaParam::equality('hello', 'world');
        $this->rangeCriteria = new RangeCriteriaParam('occured_on', '2020-01-01 00:00:00');
        $this->beConstructedWith($this->termCriteria, $this->rangeCriteria);
    }

    function it_should_be_a_criteria()
    {
        $this->shouldBeAnInstanceOf(Criteria::class);
    }

    function it_can_be_transformed_to_an_array()
    {
        $this->toArray()->shouldReturn([
            $this->termCriteria,
            $this->rangeCriteria
        ]);
    }

    function it_can_return_as_string()
    {
        $this->__toString()->shouldBe(
            \json_encode([
                $this->termCriteria->toArray(),
                $this->rangeCriteria->toArray()
            ])
        );
    }
}

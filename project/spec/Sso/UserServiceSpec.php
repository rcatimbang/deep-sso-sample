<?php

namespace spec\App\Sso;

use App\Sso\UserRepository;
use App\Sso\UserService;
use Illuminate\Contracts\Cache\Repository as Cache;
use Lcobucci\JWT\Configuration;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use Lcobucci\JWT\Signer\Key\InMemory;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Psr\Container\ContainerInterface;
use SmartPldt\Deep\Sso\CanonicalUserId\CanonicalUserId;
use SmartPldt\Deep\Sso\CanonicalUserId\Email;
use SmartPldt\Deep\Sso\CanonicalUserId\HashFactory;
use SmartPldt\Deep\Sso\CanonicalUserId\Min;
use SmartPldt\Deep\Sso\CanonicalUserId\PlainSha256;
use SmartPldt\Deep\Sso\DeviceTokenWhitelist;
use SmartPldt\Deep\Sso\DomainEvents;
use SmartPldt\Deep\Sso\Event\EventBus;
use SmartPldt\Deep\Sso\EventStore;
use SmartPldt\Deep\Sso\Exception;
use SmartPldt\Deep\Sso\Hash\Argon2idPassword;
use SmartPldt\Deep\Sso\Hash\PasswordHistory;
use SmartPldt\Deep\Sso\Hash\Sha256Hmac;
use SmartPldt\Deep\Sso\MranWasNominated;
use SmartPldt\Deep\Sso\Password;
use SmartPldt\Deep\Sso\User;
use SmartPldt\Deep\Sso\UserId;
use SmartPldt\Deep\Sso\UserNotFoundError;
use SmartPldt\Deep\Sso\UserRegistrationVerified;
use SmartPldt\Deep\Sso\UserRepository as BaseUserRepository;
use SmartPldt\Deep\Sso\UserTokenWhitelist;
use SmartPldt\Deep\Sso\UserWasRegistered;
use SmartPldt\Deep\Sso\VerificationCode\Service as VerificationCodeService;
use spec\App\ObjectBehaviorHelper;

class UserServiceSpec extends ObjectBehavior
{
    use ObjectBehaviorHelper;

    private UserRepository $userRepository;

    function let(
        ContainerInterface $container,
        EventStore $eventStore,
        EventBus $eventBus,
        VerificationCodeService $verificationCodeService,
        Cache $cache,
        UserTokenWhitelist $tokenWhitelist,
        DeviceTokenWhitelist $deviceTokenWhitelist
    ) {
        $this->userRepository = $this->createFakeInstanceOf(UserRepository::class);
        $this->setPropertyOf($this->userRepository, 'eventStore', $eventStore->getWrappedObject());
        $this->setPropertyOf($this->userRepository, 'eventBus', $eventBus->getWrappedObject());

        $container->get(EventStore::class)->willReturn($eventStore);
        $container->get(VerificationCodeService::class)->willReturn($verificationCodeService);

        $container->get('parameter.sso.supported_canonical_id_types')->willReturn([Min::class, Email::class]);
        $container->get('parameter.sso.hmac_salt')->willReturn('stay-salty');
        $container->get(BaseUserRepository::class)->willReturn($this->userRepository);
        $container->get(Cache::class)->willReturn($cache);
        $container->get(UserTokenWhitelist::class)->willReturn($tokenWhitelist);
        $container->get(DeviceTokenWhitelist::class)->willReturn($deviceTokenWhitelist);


        $this->beConstructedWith($container);
    }

    function it_should_be_a_HashFactory()
    {
        $this->shouldHaveType(HashFactory::class);
    }

    function it_should_be_a_UserService()
    {
        $this->shouldHaveType(UserService::class);
    }

    function it_should_generate_Userid()
    {
       $this->generateUserId()->shouldHaveType(UserId::class);
    }

    function it_should_create_CanonicalUserid(ContainerInterface $container)
    {
        $container->get('parameter.sso.supported_canonical_id_types')->shouldBeCalled()->willReturn([Min::class, Email::class]);

        $this->createCanonicalUserId('sample@email.com')->shouldHaveType(Email::class);
        $this->createCanonicalUserId('09181112222')->shouldHaveType(Min::class);
    }

    function it_should_return_the_PasswordHash()
    {
        $this->createPasswordHash(new Password('p4s$W0r|)'))->shouldHaveType(Argon2idPassword::class);
    }

    function it_should_calculate_Hmac_hash_of_given_string()
    {
        $this->calculateHmacHash('ang-hash-ng-mundo')->shouldHaveType(Sha256Hmac::class);
    }

    function it_should_generate_a_user_access_token(ContainerInterface $container, UserTokenWhitelist $tokenWhitelist)
    {
        $config = Configuration::forSymmetricSigner(new Sha256(), InMemory::plainText('mayBuhokAngNgipinNgTitaMo'));
        $container->get('service.sso.jwt_service')->shouldBeCalledOnce()->willReturn($config);
        $container->get('parameter.sso.jwt_expiry')->shouldBeCalledOnce()->willReturn(600);

        $this->generateUserAccessToken($token = 'de960b8a-1021-4189-9683-37ac9821fa20');

        $tokenWhitelist->set($token, Argument::that(self::containsJsonWebToken()))
            ->shouldHaveBeenCalledOnce();
    }

    function it_should_generate_a_device_access_token(ContainerInterface $container, DeviceTokenWhitelist $deviceTokenWhitelist)
    {
        $config = Configuration::forSymmetricSigner(new Sha256(), InMemory::plainText('mayBuhokAngNgipinNgTitaMo'));
        $container->get('service.sso.jwt_service')->shouldBeCalledOnce()->willReturn($config);
        $container->get('parameter.sso.device_jwt_expiry')->shouldBeCalledOnce()->willReturn(600);

        $this->generateDeviceAccessToken($token = 'de960b8a-1021-4189-9683-37ac9821fa20');

        $deviceTokenWhitelist->set($token, Argument::that(self::containsJsonWebToken()))
            ->shouldHaveBeenCalledOnce();
    }

    private static function containsJsonWebToken()
    {
        return function (string $jwt) {
            return (bool)\preg_match(
                '/^[A-Za-z0-9-_=]+\.[A-Za-z0-9-_=]+\.?[A-Za-z0-9-_.+\/\=]*$/',
                $jwt
            );
        };
    }

    function it_should_save_a_User()
    {
        $user = $this->createFakeInstanceOf(User::class);
        $this->setPropertyOf($user, 'canonicalUserId', Min::create('09190000001', PlainSha256::create()));
        $this->setPropertyOf($user, 'userId', $userId = UserId::fromString($this->createRandomAggregateId()));

        $this->saveUser($user);
    }

    function it_should_return_a_User_by_user_id(EventStore $eventStore, UserService $userService)
    {
        $event = $this->createObjectBuilderFor(UserWasRegistered::class)
            ->set('userId', UserId::fromString($userIdAsString = 'de960b8a-1021-4189-9683-37ac9821fa20'))
            ->set('password', Argon2idPassword::fromPasswordHash(
                '$argon2id$v=19$m=65536,t=4,p=1$MjI4enhOdWlqQVdQNkdmRw$up21SANf1UkUjZfrbdPZfgOVgSAMqSDNToKzV5/cIAk'
            ))
            ->set('canonicalUserId', Min::create('09191112220', PlainSha256::create()))
            ->set('occurredOn', \microtime(true))
            ->set(
                'passwordHistory',
                PasswordHistory::fromArray(5, [$this->randomCrc32Hash(), $this->randomCrc32Hash()])
            )
            ->build()
        ;

        $eventStore->find(Argument::type(UserId::class))->shouldBeCalledOnce()->willReturn(new DomainEvents($event));
        $this->findUserByUserId($userIdAsString)->shouldHaveType(User::class);
    }

    function it_should_return_a_User_instance_by_canonical_user_id(
        ContainerInterface $container,
        EventStore $eventStore
    ) {
        $event = $this->createObjectBuilderFor(UserWasRegistered::class)
            ->set('userId', UserId::fromString('de960b8a-1021-4189-9683-37ac9821fa20'))
            ->set('password', Argon2idPassword::fromPasswordHash(
                '$argon2id$v=19$m=65536,t=4,p=1$MjI4enhOdWlqQVdQNkdmRw$up21SANf1UkUjZfrbdPZfgOVgSAMqSDNToKzV5/cIAk'
            ))
            ->set('canonicalUserId', Min::create('09191112220', PlainSha256::create()))
            ->set('occurredOn', \microtime(true))
            ->set(
                'passwordHistory',
                PasswordHistory::fromArray(5, [$this->randomCrc32Hash(), $this->randomCrc32Hash()])
            )
            ->build()
        ;
        $container->get(EventStore::class)->shouldBeCalledOnce()->willReturn($eventStore);
        $domainEvents = new DomainEvents($event);
        $eventStore->findByCanonicalUserId(Argument::type(CanonicalUserId::class))->willReturn($domainEvents);
        $this->findUserByCanonicalUserId((string)$event->canonicalUserId())->shouldHaveType(User::class);
    }

    function it_should_throw_an_error_if_DomainEvents_is_empty(EventStore $eventStore)
    {
        $eventStore->findByCanonicalUserId(Argument::type(Email::class))->willReturn(new DomainEvents());
        $this->shouldThrow(UserNotFoundError::class)->during('findUserByCanonicalUserId', ['sample@multisyscorp.com']);
    }

    function it_should_throw_an_error_if_the_verification_code_is_invalid(
        VerificationCodeService $verificationCodeService
    ) {
        $verificationCodeService->verificationCodeIsValid(Argument::cetera())->shouldBeCalled()->willReturn(false);

        $expectedException = new Exception('Invalid verification code.');
        $this->shouldThrow($expectedException)
            ->during('verifyRegistration', [$this->createRandomAggregateId(), '09191112220', '1234']);
    }

    function it_should_verify_if_the_user_is_registered(
        VerificationCodeService $verificationCodeService,
        EventStore $eventStore,
        EventBus $eventBus
    ) {
        $userWasRegistered = $this->createObjectBuilderFor(UserWasRegistered::class)
            ->set('userId', UserId::fromString('de960b8a-1021-4189-9683-37ac9821fa20'))
            ->set('password', Argon2idPassword::fromPasswordHash(
                '$argon2id$v=19$m=65536,t=4,p=1$MjI4enhOdWlqQVdQNkdmRw$up21SANf1UkUjZfrbdPZfgOVgSAMqSDNToKzV5/cIAk'
            ))
            ->set('canonicalUserId', Min::create('09191112220', PlainSha256::create()))
            ->set('occurredOn', \microtime(true))
            ->set(
                'passwordHistory',
                PasswordHistory::fromArray(5, [$this->randomCrc32Hash(), $this->randomCrc32Hash()])
            )
            ->build()
        ;
        $mranWasNominated = $this->fakeMranWasNominatedEvent();

        $eventStore->find($userWasRegistered->userId())
            ->shouldBeCalledOnce()
            ->willReturn(new DomainEvents($userWasRegistered, $mranWasNominated));

        $verificationCodeService
            ->verificationCodeIsValid(Argument::type(CanonicalUserId::class), '1234')
            ->willReturn(true);

        $this->verifyRegistration((string)$userWasRegistered->userId(), (string)$mranWasNominated->mran(), '1234');

        $eventStore->save(Argument::type(UserRegistrationVerified::class))->shouldHaveBeenCalledOnce();
        $eventBus->send(Argument::type(UserRegistrationVerified::class))->shouldHaveBeenCalledOnce();
    }

    private function fakeUserWasRegisteredEvent(): UserWasRegistered
    {
        $userId = UserId::fromString($this->createRandomAggregateId());
        $userWasRegistered = $this->createFakeInstanceOf(UserWasRegistered::class);
        $this->setFakePropertiesOfUserWasRegisteredEvent($userWasRegistered, $userId);

        return $userWasRegistered;
    }

    private function setFakePropertiesOfUserWasRegisteredEvent(object $userWasRegistered, UserId $userId): void
    {
        $this->setPropertyOf($userWasRegistered, 'userId', $userId);
        $this->setPropertyOf($userWasRegistered, 'occurredOn', \time() - 30);
        $canonicalUserId = Email::create('foo@bar.com', PlainSha256::create());
        $this->setPropertyOf($userWasRegistered, 'canonicalUserId', $canonicalUserId);
        $passwordHash = Argon2idPassword::fromPasswordHash(
            '$argon2id$v=19$m=65536,t=4,p=1$MjI4enhOdWlqQVdQNkdmRw$up21SANf1UkUjZfrbdPZfgOVgSAMqSDNToKzV5/cIAk'
        );
        $this->setPropertyOf($userWasRegistered, 'password', $passwordHash);
    }

    private function fakeMranWasNominatedEvent(): MranWasNominated
    {
        $mranWasNominated = $this->createFakeInstanceOf(MranWasNominated::class);
        $mran = Min::create('09191112220', PlainSha256::create());
        $this->setPropertyOf($mranWasNominated, 'mran', $mran);
        $this->setPropertyOf($mranWasNominated, 'occurredOn', \microtime(true));

        return $mranWasNominated;
    }
}

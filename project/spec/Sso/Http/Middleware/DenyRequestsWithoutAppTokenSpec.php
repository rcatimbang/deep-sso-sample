<?php

namespace spec\App\Sso\Http\Middleware;

use App\Sso\Http\Middleware\DenyRequestsWithoutAppToken;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Contracts\Container\Container;
use Illuminate\Http\Request;
use Lcobucci\Clock\SystemClock;
use Lcobucci\JWT\Configuration;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use Lcobucci\JWT\Signer\Key\InMemory;
use Lcobucci\JWT\Validation\Constraint\SignedWith;
use Lcobucci\JWT\Validation\Constraint\ValidAt;
use PhpSpec\Exception\Exception;
use PhpSpec\ObjectBehavior;

/**
 * @mixin DenyRequestsWithoutAppToken
 */
class DenyRequestsWithoutAppTokenSpec extends ObjectBehavior
{
    function let(Container $container, Request $request)
    {
        $request->header('X-Application-Token', false)->willReturn(false);

        $this->beConstructedWith($container);
    }

    function it_should_allow_requests_with_valid_application_token(Request $request, Container $container)
    {
        $config = Configuration::forSymmetricSigner(new Sha256(), InMemory::plainText('sekretong-malupit'));
        $config->setValidationConstraints(new SignedWith($config->signer(), $config->signingKey()),
            new ValidAt(new SystemClock(new \DateTimeZone(\date_default_timezone_get()))));
        $token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJhcHBfaWQiOiJwaHBzcGVjIiwiaWF0IjoxNTk3OTIwNDEzLCJuYmYiOjE1OTc5'
            . 'MjA0MTMsImV4cCI6MTU5ODUyNTIxM30.ZiP9S1vu5UyjeRBQ7RZIrXP4TOvPKnTf6YPhtCTdYdaZ0iyiWKYO7ul8jN_uPg_Q3e7l2tss'
            . 'Z4mOQ6eYdyqLYw';
        $request->header('X-Application-Token', false)->shouldBeCalledOnce()->willReturn($token);
        $container->get('service.sso.app_jwt_service')->shouldBeCalledOnce()->willReturn($config);

        $this->handle($request, \Closure::fromCallable(self::passedWith($request)));
    }

    function it_should_deny_requests_if_application_token_is_not_provided(Request $request, Container $container)
    {
        $request->header('X-Application-Token', false)->shouldBeCalledOnce()->willReturn(false);
        $response = $this->handle($request, \Closure::fromCallable(self::passedWith($request)));
        $response->getStatusCode()->shouldBe(401);
    }

    function it_should_deny_requests_if_application_token_is_invalid(Request $request, Container $container)
    {
        $config = Configuration::forSymmetricSigner(new Sha256(), InMemory::plainText('kunware-secret-key'));
        $config->setValidationConstraints(new SignedWith($config->signer(), $config->signingKey()),
            new ValidAt(new SystemClock(new \DateTimeZone(\date_default_timezone_get()))));
        $token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJhcHBfaWQiOiJwaHBzcGVjIiwiaWF0IjoxNTk3OTIwNDEzLCJuYmYiOjE1OTc5'
            . 'MjA0MTMsImV4cCI6MTU5ODUyNTIxM30.ZiP9S1vu5UyjeRBQ7RZIrXP4TOvPKnTf6YPhtCTdYdaZ0iyiWKYO7ul8jN_uPg_Q3e7l2tss'
            . 'Z4mOQ6eYdyqLYw';
        $request->header('X-Application-Token', false)->shouldBeCalledOnce()->willReturn($token);
        $container->get('service.sso.app_jwt_service')->shouldBeCalledOnce()->willReturn($config);

        $response = $this->handle($request, \Closure::fromCallable(self::passedWith($request)));
        $response->getStatusCode()->shouldBe(401);
    }

    private static function passedWith($expectedArgument): callable
    {
        return function ($subject) use ($expectedArgument) {
            $expectedArgument = $expectedArgument->getWrappedObject();
            if ($subject !== $expectedArgument) {
                throw new Exception(
                    \sprintf(
                        "Expected `%s` but got `%s`",
                        \is_scalar($expectedArgument) ? (string)$expectedArgument : \get_class($expectedArgument),
                        \is_scalar($subject) ? (string)$subject : \get_class($subject)
                    )
                );
            }
        };
    }
}

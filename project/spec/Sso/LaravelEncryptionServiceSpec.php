<?php

namespace spec\App\Sso;

use App\Sso\LaravelEncryptionService;
use Illuminate\Contracts\Console\Kernel;
use PhpSpec\ObjectBehavior;

class LaravelEncryptionServiceSpec extends ObjectBehavior
{
    public function let()
    {
        putenv('APP_KEY=base64:5nMDRneTTohpKH9dwkUSrxIalSvirS2ETbItEv4uslk=');

        $app = require __DIR__.'/../../bootstrap/app.php';
        $app->make(Kernel::class)->bootstrap();

        return $app;
    }

    function letGo()
    {
        putenv('APP_KEY');
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(LaravelEncryptionService::class);
    }

    function it_is_able_to_encrypt_data()
    {
        $encrypted = $this->encrypt('asdasdasdf');
        $encrypted->shouldBeString();
    }

    public function it_is_be_able_to_decrypt_data()
    {
        $toBeEncrypted = 'Bla bla bla';
        $encrypted = $this->encrypt($toBeEncrypted);
        $decrypted = $this->decrypt($encrypted);
        $decrypted->shouldBeEqualTo($toBeEncrypted);
    }
}

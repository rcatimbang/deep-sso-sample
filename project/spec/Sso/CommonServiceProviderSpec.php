<?php

namespace spec\App\Sso;

use App\Sso\CommonServiceProvider;
use App\Sso\Console\DeleteUser;
use App\Sso\Console\GenerateApplicationToken;
use App\Sso\Console\ReprocessUserVerification;
use App\Sso\Console\ShowEvents;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Support\DeferrableProvider;
use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;
use PhpSpec\ObjectBehavior;
use PhpSpec\Wrapper\Collaborator;
use Prophecy\Argument;
use SmartPldt\Deep\Sso\DeviceTokenWhitelist;
use SmartPldt\Deep\Sso\EncryptionService;
use SmartPldt\Deep\Sso\Event\EventBus;
use SmartPldt\Deep\Sso\EventStore;
use SmartPldt\Deep\Sso\Notifier;
use SmartPldt\Deep\Sso\UserRepository;
use SmartPldt\Deep\Sso\UserService;
use SmartPldt\Deep\Sso\UserTokenWhitelist;
use SmartPldt\Deep\Sso\VerificationCode\Service as VerificationCodeService;
use SmartPldt\Deep\Sso\VerificationCode\Transport;

/**
 * @mixin CommonServiceProvider
 */
class CommonServiceProviderSpec extends ObjectBehavior
{
    function let(Application $app)
    {
        $this->predictRequiredParametersButRegisteredElsewhere($app);

        $this->beConstructedWith($app);
    }

    private function servicesToBeRegistered(): array
    {
        return [
            'service.sso.domain_event_factory',
            UserService::class,
            'service.sso.elasticsearch_client',
            EventStore::class,
            UserRepository::class,
            EncryptionService::class,
            'service.sso.jwt_service',
            'service.sso.app_jwt_service',
        ];
    }

    private function parametersToBeRegistered(): array
    {
        return [
            'parameter.sso.elasticsearch_hosts',
            'parameter.sso.jwt_secret',
            'parameter.sso.jwt_expiry',
            'parameter.sso.device_jwt_expiry',
            'parameter.sso.otp_code_length',
            'parameter.sso.console.minimum_execution_time',
            'parameter.sso.dormancy_in_days',
            'parameter.sso.domain_prefix',
            'parameter.sso.event_handlers',
        ];
    }

    private function parametersNotToBeRegistered(): array
    {
        return [
            'parameter.sso.supported_canonical_id_types',
            'parameter.sso.hmac_salt',
            'parameter.sso.event_store_index',
            'parameter.sso.sqlite3_db_file',
        ];
    }

    private function servicesNotToBeRegistered(): array
    {
        return [
            'service.sso.message_queue_connection_factory',
            EventBus::class,
            VerificationCodeService::class,
            Transport::class,
            Notifier::class,
            'service.sso.read_write_sqlite3_db',
            'service.sso.read_only_sqlite3_db',
        ];
    }

    private function predictRequiredParametersButRegisteredElsewhere(Application $app): void
    {
        $app->get('parameter.sso.supported_canonical_id_types')->willReturn([]);
        $app->get('parameter.sso.hmac_salt')->willReturn('ang-alamat-ng-alat');
        $app->get('parameter.sso.elasticsearch_hosts')->willReturn([]);
    }

    function it_should_be_a_Laravel_DeferrableProvider()
    {
        $this->shouldHaveType(ServiceProvider::class);
        $this->shouldHaveType(DeferrableProvider::class);
    }

    function it_should_create_DomainEventFactory(Application $app, UserService $userService)
    {
        $app->get(UserService::class)->shouldBeCalledOnce()->willReturn($userService);

        $this->createDomainEventFactory();
        $app->get(UserService::class)->shouldHaveBeenCalledOnce();
    }

    function it_should_create_UserService(
        Application $app,
        UserTokenWhitelist $userTokenWhitelist,
        DeviceTokenWhitelist $deviceTokenWhitelist
    ) {
        $app->get(UserTokenWhitelist::class)->shouldBeCalledOnce()->willReturn($userTokenWhitelist);
        $app->get(DeviceTokenWhitelist::class)->shouldBeCalledOnce()->willReturn($deviceTokenWhitelist);
        $this->createUserService();
    }

    function it_should_create_Elasticsearch_Client()
    {
        $this->createElasticsearchClient();
    }

    function it_should_register_relevant_services_or_parameters(Application $app)
    {
        $this->register();
        $this->assertThatTheseServicesOrParametersWereRegistered(
            $app,
            [...$this->servicesToBeRegistered(), ...$this->parametersToBeRegistered()]
        );
        $this->assertThatTheseServicesOrParametersWereNotRegistered(
            $app,
            [...$this->servicesNotToBeRegistered(), ...$this->parametersNotToBeRegistered()]
        );
    }

    function it_should_register_console_commands(Application $app, Router $router)
    {
        $app->runningInConsole()->shouldBeCalledOnce()->willReturn(true);
        $app->get('router')->willReturn($router);
        $this->boot();
    }

    function it_should_return_the_console_commands_to_register()
    {
        $this->consoleCommands()->shouldBe([
            DeleteUser::class,
            GenerateApplicationToken::class,
            ShowEvents::class,
            ReprocessUserVerification::class
        ]);
    }

    private function assertThatTheseServicesOrParametersWereRegistered(Collaborator $app, array $serviceIds): void
    {
        foreach ($serviceIds as $serviceId) {
            $this->provides()->shouldContain($serviceId);
            $app->singleton($serviceId, Argument::type('callable'))->shouldHaveBeenCalledOnce();
        }
    }

    private function assertThatTheseServicesOrParametersWereNotRegistered(Collaborator $app, array $serviceIds): void
    {
        foreach ($serviceIds as $serviceId) {
            $this->provides()->shouldNotContain($serviceId);
            $app->singleton($serviceId, Argument::any())->shouldNotHaveBeenCalled();
        }
    }

}

<?php

namespace spec\App\Sso;

use App\Sso\MultisysAmqpLibEventBus;
use Multisys\AmqpEventbusLibrary\Consumer;
use Multisys\AmqpEventbusLibrary\EventHandler;
use Multisys\AmqpEventbusLibrary\Sender;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use SmartPldt\Deep\Sso\AggregateId;
use SmartPldt\Deep\Sso\DomainEvent;
use SmartPldt\Deep\Sso\Event\EventBus;
use SmartPldt\Deep\Sso\Event\HandlesAnEventMessage;
use SmartPldt\Deep\Sso\UserService;

/**
 * @mixin MultisysAmqpLibEventBus
 */
class MultisysAmqpLibEventBusSpec extends ObjectBehavior
{
    private PseudoSsoEventHandler $eventHandler;

    private int $consumerTtlInMilliseconds;

    function let(Consumer $consumer, Sender $sender)
    {
        $this->consumerTtlInMilliseconds = \mt_rand(100, 200);
        $this->eventHandler = new PseudoSsoEventHandler();

        $this->beConstructedWith($consumer, $sender, $this->consumerTtlInMilliseconds);
    }

    function letGo()
    {
        PseudoSsoEventHandler::$eventName = null;
        PseudoDomainEvent::$eventName = null;
    }

    function it_should_be_an_EventBus()
    {
        $this->shouldHaveType(EventBus::class);
    }

    function it_should_forward_received_events_from_Consumer_to_its_EventHandler(Consumer $consumer)
    {
        PseudoSsoEventHandler::$eventName = $eventName = 'birthday-ng-nanay-mo-' . \hash('crc32', \random_bytes(4));
        $consumer->delegateMessageToHandler(
            Argument::that(self::isAnAmqpLibraryEventHandlerWrappingSsoEventHandler($eventName)),
            $this->consumerTtlInMilliseconds
        )->shouldBeCalledOnce();

        $this->delegateEventTo($this->eventHandler);

        $this->eventHandler->assertThatEventWasHandled();
    }

    function it_should_delegate_sending_of_payload_to_the_sender(Sender $sender)
    {
        $event = new PseudoDomainEvent();
        PseudoDomainEvent::$eventName = 'birthday-ng-kuting-mong-namatay';
        $eventPayload = 'Nagdiwang ka dahil wala na cya kasi lagi kinakain ng kuting mo paksiw mong ulam';
        PseudoDomainEvent::$eventPayload = $eventPayload;

        $sender->send(PseudoDomainEvent::$eventName, $event::$eventPayload)->shouldBeCalledOnce();

        $this->send($event);
    }

    private static function isAnAmqpLibraryEventHandlerWrappingSsoEventHandler(
        string $expectedEventName
    ): callable {
        return function (EventHandler $eventHandler) use ($expectedEventName) {
            $eventHandler('message-is-not-important-here');
            return $eventHandler->eventNameToHandle() === $expectedEventName;
        };
    }
}

class PseudoSsoEventHandler implements HandlesAnEventMessage
{
    public static ?string $eventName;

    private static bool $eventWasHandled = false;

    public function eventNameToHandle(): string
    {
        return self::$eventName;
    }

    public function __invoke(string $encodedDomainEvent): void
    {
        self::$eventWasHandled = true;
    }

    public function assertThatEventWasHandled(): void
    {
        if (!self::$eventWasHandled) {
            throw new \Exception('Sorry, minura ka ng nanay mo dahil mali regalo mo!');
        }
    }
}

class PseudoDomainEvent implements DomainEvent
{
    public static ?string $eventName;

    public static string $eventPayload;

    public static function name(): string
    {
        return self::$eventName;
    }

    public function __toString(): string
    {
        return self::$eventPayload;
    }

    public function aggregateId(): AggregateId
    {
    }

    public function occurredOn(): \DateTimeInterface
    {
    }

    public function occurredOnTimestamp(): float
    {
    }

    public static function fromArray(array $eventAsArray, UserService $userService = null): DomainEvent
    {
    }
}

<?php

namespace spec\App\Sso\Event\Handler;

use App\Sso\Event\Handler\SilentWrapper;
use Multisys\AmqpEventbusLibrary\EventHandler;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use SmartPldt\Deep\Sso\Event\HandlesAnEventMessage;

class SilentWrapperSpec extends ObjectBehavior
{
    private static function createRandomCrc32Hash(): string
    {
        return \hash('crc32', \random_bytes(4));
    }

    function it_is_an_EventHandler()
    {
        $this->shouldHaveType(EventHandler::class);
    }

    function let(HandlesAnEventMessage $ssoEventHandler)
    {
        $ssoEventHandler->eventNameToHandle()->willReturn('party-' . self::createRandomCrc32Hash());
        $ssoEventHandler->__invoke(Argument::type('string'));

        $this->beConstructedWith($ssoEventHandler);
    }

    function it_should_return_the_event_name_it_handles_relative_to_the_sso_event_handler_it_wrapped(
        HandlesAnEventMessage $ssoEventHandler
    )
    {
        $ssoEventHandler->eventNameToHandle()->shouldBeCalledOnce()->willReturn($event = 'concert.ng.idol.mo');
        $this->eventNameToHandle()->shouldBe($event);
    }

    function it_should_delegate_method_calls_Sso_event_handler(HandlesAnEventMessage $ssoEventHandler)
    {
        $this($encodedEvent = 'Ang-laman-ng-payload');

        $ssoEventHandler->__invoke($encodedEvent)->shouldHaveBeenCalledOnce();
    }

    function it_should_suppress_exceptions_thrown_by_the_event_handler_it_invokes(
        HandlesAnEventMessage $ssoEventHandler
    ) {
        $exception = new \Exception('sarap mag type exception');

        $ssoEventHandler->__invoke($encodedEvent = 'mechanical-keyboard-numbawan')
            ->shouldBeCalledOnce()
            ->willThrow($exception);

        $this($encodedEvent)->shouldNotThrow($exception);
    }
}

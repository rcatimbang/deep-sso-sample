<?php

namespace spec\App\Sso;

use App\Sso\DeviceTokenWhitelist;
use Illuminate\Contracts\Cache\Repository as Cache;
use PhpSpec\ObjectBehavior;
use SmartPldt\Deep\Sso\DeviceTokenWhitelist as BaseTokenWhitelist;

/**
 * @mixin DeviceTokenWhitelist
 */
class DeviceTokenWhitelistSpec extends ObjectBehavior
{
    function let(Cache $cache)
    {
        $this->beConstructedWith($cache);
    }

    function it_should_be_a_DeviceTokenWhitelist()
    {
        $this->shouldHaveType(BaseTokenWhitelist::class);
    }

    function it_should_whitelist_a_given_device_access_token(Cache $cache)
    {
        $this->set($deviceId = 'kunware-device-id', $userToken = 'kunwareng-token');

        $cache->put(\sprintf('device-token-whitelist-for-%s', $deviceId),\hash('crc32', $userToken))
            ->shouldHaveBeenCalledOnce();
    }

    function it_should_return_true_if_token_is_whitelisted(Cache $cache)
    {
        $deviceId = 'kunware-device-id';
        $tokenHash = \hash('crc32', $userToken = 'kunwareng-token');
        $cache->get(\sprintf('device-token-whitelist-for-%s', $deviceId))->shouldBeCalledOnce()->willReturn($tokenHash);

        $this->isWhiteListed($deviceId, $userToken)->shouldBe(true);
    }

    function it_should_return_false_if_token_is_not_whitelisted(Cache $cache)
    {
        $deviceId = 'kunware-device-id';
        $tokenHash = \hash('crc32', $userToken = 'kunwareng-token');
        $cache->get(\sprintf('device-token-whitelist-for-%s', $deviceId))->shouldBeCalledOnce()->willReturn('maleng-hash');

        $this->isWhiteListed($deviceId, $userToken)->shouldBe(false);
    }
}

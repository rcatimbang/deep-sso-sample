<?php

declare(strict_types=1);

namespace spec\App\Sso;

use App\Sso\LaravelServiceProvider;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Support\DeferrableProvider;
use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;
use PhpSpec\ObjectBehavior;
use PhpSpec\Wrapper\Collaborator;
use Prophecy\Argument;
use SmartPldt\Deep\Sso\DeviceTokenWhitelist;
use SmartPldt\Deep\Sso\EncryptionService;
use SmartPldt\Deep\Sso\Event\EventBus;
use SmartPldt\Deep\Sso\EventStore;
use SmartPldt\Deep\Sso\Notifier;
use SmartPldt\Deep\Sso\UserRepository;
use SmartPldt\Deep\Sso\UserService;
use SmartPldt\Deep\Sso\UserTokenWhitelist;
use SmartPldt\Deep\Sso\VerificationCode\Service as VerificationCodeService;
use SmartPldt\Deep\Sso\VerificationCode\TimestampAwareService;
use SmartPldt\Deep\Sso\VerificationCode\Transport;

/**
 * @mixin LaravelServiceProvider
 */
class LaravelServiceProviderSpec extends ObjectBehavior
{
    private function parametersToBeRegistered(): array
    {
        return [
            'parameter.sso.supported_canonical_id_types',
            'parameter.sso.hmac_salt',
            'parameter.sso.event_store_index',
            'parameter.sso.domain_prefix',
        ];
    }

    private function servicesToBeRegistered(): array
    {
        return [
            EventBus::class,
            VerificationCodeService::class,
            Transport::class,
            Notifier::class,
            UserTokenWhitelist::class,
            DeviceTokenWhitelist::class,
            TimestampAwareService::class,
        ];
    }

    private function parametersNotToBeRegistered(): array
    {
        return [
            'parameter.sso.elasticsearch_hosts',
            'parameter.sso.sqlite3_db_file',
        ];
    }

    private function servicesNotToBeRegistered(): array
    {
        return [
            'service.sso.elasticsearch_client',
            'service.sso.domain_event_factory',
            UserService::class,
            EventStore::class,
            UserRepository::class,
            EncryptionService::class,
            'service.sso.read_write_sqlite3_db',
            'service.sso.read_only_sqlite3_db',
        ];
    }

    function let(Application $app)
    {
        $this->beConstructedWith($app);
    }

    function it_should_be_a_Laravel_DeferrableProvider()
    {
        $this->shouldHaveType(ServiceProvider::class);
        $this->shouldHaveType(DeferrableProvider::class);
    }

    function it_should_register_relevant_services_or_parameters(Application $app)
    {
        $this->register();
        $this->assertThatTheseServicesOrParametersWereRegistered(
            $app,
            [...$this->servicesToBeRegistered(), ...$this->parametersToBeRegistered()]
        );
        $this->assertThatTheseServicesOrParametersWereNotRegistered(
            $app,
            [...$this->servicesNotToBeRegistered(), ...$this->parametersNotToBeRegistered()]
        );
    }

    function it_should_register_routes_on_boot(Application $app, Router $router)
    {
        $app->get('router')->willReturn($router);
        $this->boot();
    }

    private function assertThatTheseServicesOrParametersWereRegistered(Collaborator $app, array $serviceIds): void
    {
        foreach ($serviceIds as $serviceId) {
            $this->provides()->shouldContain($serviceId);
            $app->singleton($serviceId, Argument::type('callable'))->shouldHaveBeenCalledOnce();
        }
    }

    private function assertThatTheseServicesOrParametersWereNotRegistered(Collaborator $app, array $serviceIds): void
    {
        foreach ($serviceIds as $serviceId) {
            $this->provides()->shouldNotContain($serviceId);
            $app->singleton($serviceId, Argument::type('callable'))->shouldNotBeCalled();
        }
    }
}

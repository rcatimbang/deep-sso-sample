<?php

namespace spec\App\Sso\Console;

use App\Sso\Console\DeleteUser;
use Illuminate\Console\Command;
use Illuminate\Contracts\Container\Container;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use SmartPldt\Deep\Sso\CanonicalUserId\Min;
use SmartPldt\Deep\Sso\CanonicalUserId\PlainSha256;
use SmartPldt\Deep\Sso\DomainEvents;
use SmartPldt\Deep\Sso\EventStore;
use SmartPldt\Deep\Sso\FailedLoginTimestamps;
use SmartPldt\Deep\Sso\Hash\PasswordHistory;
use SmartPldt\Deep\Sso\User;
use SmartPldt\Deep\Sso\UserId;
use SmartPldt\Deep\Sso\UserService;
use SmartPldt\Deep\Sso\UserWasDeleted;
use spec\App\ObjectBehaviorHelper;
use Symfony\Component\Console\Input\InputInterface;

/**
 * @mixin DeleteUser
 */
class DeleteUserSpec extends ObjectBehavior
{
    use ObjectBehaviorHelper;

    function let(Container $container, InputInterface $input, EventStore $eventStore, UserService $userService)
    {
        $container->get(EventStore::class)->willReturn($eventStore);
        $container->get(UserService::class)->willReturn($userService);

        $eventStore->dormantUserIds(Argument::type('int'))->willReturn(new \ArrayIterator());

        $this->setLaravel($container);
        $this->setInput($input);

        $input->getOption('user-ids')->willReturn(null);
        $input->getOption('mins')->willReturn(null);
    }

    function it_should_be_a_Command()
    {
        $this->shouldHaveType(Command::class);
    }

    function it_should_return_the_command_name()
    {
        $this->getName()->shouldBe('sso:delete-user');
    }

    function it_should_return_the_command_description()
    {
        $this->getDescription()->shouldBe('Delete a user by given UserIds or CanonicalUserIds');
    }

    function it_should_define_the_user_id_option()
    {
        $option = $this->getDefinition()->getOption('user-ids');

        $option->getDescription()->shouldBe('Comma-delimited UserIds to delete');
        $option->getShortcut()->shouldBe('u');
        $option->getDefault()->shouldBe(null);
    }

    function it_should_define_the_min_option()
    {
        $option = $this->getDefinition()->getOption('mins');

        $option->getDescription()->shouldBe('Comma-delimited mobile identification numbers (min)s to delete');
        $option->getShortcut()->shouldBe('m');
        $option->getDefault()->shouldBe(null);
    }

    function it_should_delete_given_user_ids(InputInterface $input, EventStore $eventStore, UserService $userService)
    {
        $users = [
            $this->createObjectBuilderFor(User::class)
                ->set('userId', $userIds[] = UserId::fromString('5d824069-b76b-4099-ab4a-81f79f024217'))
                ->set('canonicalUserId', $mins[] = Min::create('09191234567', PlainSha256::create()))
                ->set('failedLoginTimestamps', new FailedLoginTimestamps())
                ->set('passwordHistory', PasswordHistory::create())
                ->build()
            ,
            $this->createObjectBuilderFor(User::class)
                ->set('userId', $userIds[] = UserId::fromString('8e7d8bd0-eb5d-48a2-9ea3-dd3721cb744d'))
                ->set('canonicalUserId', $mins[] = Min::create('09181234567', PlainSha256::create()))
                ->set('failedLoginTimestamps', new FailedLoginTimestamps())
                ->set('passwordHistory', PasswordHistory::create())
                ->build()
            ,
        ];
        $userService->findUserByUserId((string)$userIds[0])->shouldBeCalledOnce()->willReturn($users[0]);
        $userService->findUserByUserId((string)$userIds[1])->shouldBeCalledOnce()->willReturn($users[1]);
        $userService->saveUser(Argument::that(self::containsUserWasDeletedEvent()))->shouldBeCalledTimes(2);

        $input->getOption('user-ids')->shouldBeCalledOnce()->willReturn(\implode(',', $userIds));

        $this->handle()->shouldBe(0);
    }

    function it_should_delete_given_mins(InputInterface $input, EventStore $eventStore, UserService $userService)
    {
        $users = [
            $this->createObjectBuilderFor(User::class)
                ->set('userId', $userIds[] = UserId::fromString('5d824069-b76b-4099-ab4a-81f79f024217'))
                ->set('canonicalUserId', $mins[] = Min::create('09191234567', PlainSha256::create()))
                ->set('failedLoginTimestamps', new FailedLoginTimestamps())
                ->set('passwordHistory', PasswordHistory::create())
                ->build()
            ,
            $this->createObjectBuilderFor(User::class)
                ->set('userId', $userIds[] = UserId::fromString('8e7d8bd0-eb5d-48a2-9ea3-dd3721cb744d'))
                ->set('canonicalUserId', $mins[] = Min::create('09181234567', PlainSha256::create()))
                ->set('failedLoginTimestamps', new FailedLoginTimestamps())
                ->set('passwordHistory', PasswordHistory::create())
                ->build()
            ,
        ];
        $userService->createCanonicalUserId((string)$mins[0])->shouldBeCalledOnce()->willReturn($mins[0]);
        $userService->findUserByCanonicalUserId((string)$mins[0])->shouldBeCalledOnce()->willReturn($users[0]);
        $userService->createCanonicalUserId((string)$mins[1])->shouldBeCalledOnce()->willReturn($mins[1]);
        $userService->findUserByCanonicalUserId((string)$mins[1])->shouldBeCalledOnce()->willReturn($users[1]);

        $userService->saveUser(Argument::that(self::containsUserWasDeletedEvent()))->shouldBeCalledTimes(2);

        $input->getOption('mins')->shouldBeCalledOnce()->willReturn(\implode(',', $mins));

        $this->handle()->shouldBe(0);
    }

    function it_should_delete_dormant_users(EventStore $eventStore, UserService $userService)
    {
        $users = [
            $this->createObjectBuilderFor(User::class)
                ->set('userId', $userIds[] = UserId::fromString('5d824069-b76b-4099-ab4a-81f79f024217'))
                ->set('canonicalUserId', $mins[] = Min::create('09191234567', PlainSha256::create()))
                ->set('failedLoginTimestamps', new FailedLoginTimestamps())
                ->set('passwordHistory', PasswordHistory::create())
                ->build()
            ,
            $this->createObjectBuilderFor(User::class)
                ->set('userId', $userIds[] = UserId::fromString('8e7d8bd0-eb5d-48a2-9ea3-dd3721cb744d'))
                ->set('canonicalUserId', $mins[] = Min::create('09181234567', PlainSha256::create()))
                ->set('failedLoginTimestamps', new FailedLoginTimestamps())
                ->set('passwordHistory', PasswordHistory::create())
                ->build()
            ,
        ];
        $userService->findUserByUserId((string)$userIds[0])->shouldBeCalledOnce()->willReturn($users[0]);
        $userService->findUserByUserId((string)$userIds[1])->shouldBeCalledOnce()->willReturn($users[1]);
        $userService->saveUser(Argument::that(self::containsUserWasDeletedEvent()))->shouldBeCalledTimes(2);
        $eventStore->dormantUserIds(10)->shouldBeCalledOnce()->willReturn(new \ArrayIterator($userIds));

        $this->handle()->shouldBe(0);
    }

    function it_should_return_a_non_zero_exit_status_when_any_collaborator_throws_an_error(EventStore $eventStore)
    {
        $eventStore->dormantUserIds(10)->shouldBeCalledOnce()->willThrow(new \Exception('Just an error'));

        $this->handle()->shouldNotBe(0);
    }

    private static function containsUserWasDeletedEvent(): callable
    {
        return function (User $user) {
            return self::domainEventsContainsType($user->recordedEvents(), UserWasDeleted::class);
        };
    }

    private static function domainEventsContainsType(DomainEvents $domainEvents, string $type): bool
    {
        foreach ($domainEvents as $domainEvent) {
            return $domainEvent instanceof $type;
        }

        return false;
    }
}

<?php

namespace spec\App\Sso\Console;

use App\Sso\Console\GenerateApplicationToken;
use Illuminate\Console\Command;
use Illuminate\Console\OutputStyle;
use Illuminate\Contracts\Container\Container;
use Lcobucci\Clock\SystemClock;
use Lcobucci\JWT\Configuration;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use Lcobucci\JWT\Signer\Key\InMemory;
use Lcobucci\JWT\Validation\Constraint\SignedWith;
use Lcobucci\JWT\Validation\Constraint\ValidAt;
use PhpSpec\ObjectBehavior;
use Symfony\Component\Console\Input\InputInterface;

/**
 * @mixin GenerateApplicationToken
 */
class GenerateApplicationTokenSpec extends ObjectBehavior
{
    function let(Container $container, InputInterface $input, OutputStyle $output)
    {
        $this->setLaravel($container);
        $this->setInput($input);
        $this->setOutput($output);
    }

    function it_should_be_a_Command()
    {
        $this->shouldHaveType(Command::class);
    }

    function it_should_return_the_command_name()
    {
        $this->getName()->shouldBe('sso:generate-app-token');
    }

    function it_should_return_the_command_description()
    {
        $this->getDescription()->shouldBe('Generate an application token');
    }

    function it_should_define_the_application_id_argument()
    {
        $argument = $this->getDefinition()->getArgument('app-id');

        $argument->getDescription()->shouldBe('Application ID');
        $argument->getDefault()->shouldBe(null);
    }

    function it_should_define_the_expiration_option()
    {
        $option = $this->getDefinition()->getOption('expiration');

        $option->getDescription()->shouldBe('Expiration in days');
        $option->getShortcut()->shouldBe('e');
        $option->getDefault()->shouldBe(7);
    }

    function it_should_create_an_application_token(Container $container)
    {
        $config = Configuration::forSymmetricSigner(new Sha256(), InMemory::plainText('mayBuhokAngNgipinNgTitaMo'));
        $container->get('service.sso.app_jwt_service')->shouldBeCalledOnce()->willReturn($config);
        $this->handle()->shouldBe(0);
    }
}

<?php

namespace spec\App\Sso\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Contracts\Container\Container;
use Multisys\AmqpEventbusLibrary\EventHandler;
use PhpSpec\ObjectBehavior;
use PhpSpec\Wrapper\Collaborator;
use Prophecy\Argument;
use SmartPldt\Deep\Sso\Event\EventBus;
use SmartPldt\Deep\Sso\Event\HandlesAnEventMessage;
use SmartPldt\Deep\Sso\EventStore;
use Symfony\Component\Console\Input\InputInterface;

class ProcessEventSpec extends ObjectBehavior
{
    function let(
        Container $container,
        EventBus $eventBus,
        EventStore $eventStore,
        InputInterface $input
    )
    {
        $this->setLaravel($container);
        $this->setInput($input);

        $container->get(EventBus::class)->willReturn($eventBus);
        $container->get(EventStore::class)->willReturn($eventStore);
    }

    function it_should_be_a_Command()
    {
        $this->shouldHaveType(Command::class);
    }

    function it_should_return_the_command_name()
    {
        $this->getName()->shouldBe('sso:process-event');
    }

    function it_should_return_the_command_description()
    {
        $this->getDescription()->shouldBe('Process the specified event name');
    }

    function it_should_exit_if_an_event_name_do_not_have_an_event_handler(Container $container, InputInterface $input)
    {
        $container->get('parameter.sso.event_handlers')->shouldBeCalledOnce()->willReturn([]);
        $input->getArgument('event-name')->shouldBeCalledOnce()->willReturn('birthday-ng-hamster-mo');

        $this->handle()->shouldBe(Command::SUCCESS);
    }

    function it_should_process_event_that_was_passed_as_argument(
        EventBus $eventBus,
        Container $container,
        InputInterface $input,
        HandlesAnEventMessage $eventHandler
    ) {
        $eventName = 'birthday-ng-hamster-mo';
        $container->get('parameter.sso.event_handlers')
            ->shouldBeCalledOnce()
            ->willReturn([$eventName => $eventHandler]);
        $eventBus->delegateEventTo($eventHandler)->shouldBeCalledOnce();
        $input->getArgument('event-name')->shouldBeCalledOnce()->willReturn($eventName);

        $this->handle()->shouldBe(Command::SUCCESS);
    }
}

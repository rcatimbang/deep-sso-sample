<?php

namespace spec\App\Sso\Console;

use App\Sso\Console\ShowEvents;
use Illuminate\Console\Command;
use Illuminate\Console\OutputStyle;
use Illuminate\Contracts\Foundation\Application;
use PhpSpec\ObjectBehavior;
use SmartPldt\Deep\Sso\DomainEvents;
use SmartPldt\Deep\Sso\EventStore;
use SmartPldt\Deep\Sso\MranWasNominated;
use SmartPldt\Deep\Sso\UserId;
use SmartPldt\Deep\Sso\UserService;
use SmartPldt\Deep\Sso\UserWasRegistered;
use spec\App\ObjectBehaviorHelper;
use Symfony\Component\Console\Input\InputInterface;

/**
 * @mixin ShowEvents
 */
class ShowEventsSpec extends ObjectBehavior
{
    use ObjectBehaviorHelper;

    function let(
        Application $app,
        EventStore $eventStore,
        InputInterface $input,
        OutputStyle $output,
        UserService $userService
    ) {
        $app->get(EventStore::class)->willReturn($eventStore);
        $app->get(UserService::class)->willReturn($userService);

        $this->setLaravel($app);
        $this->setInput($input);
        $this->setOutput($output);
    }

    function it_is_a_Command(Application $app)
    {
        $this->shouldHaveType(Command::class);
    }

    function it_should_return_the_command_name()
    {
        $this->getName()->shouldBe('sso:show-events');
    }

    function it_should_return_the_command_description()
    {
        $this->getDescription()->shouldBe('Show events by given canonical user id');
    }

    function it_should_define_the_min_argument()
    {
        $argument = $this->getDefinition()->getArgument('cuid');
        $argument->getDescription()->shouldBe('Canonical user id');
    }

    function it_should_display_an_error_indicating_that_no_events_were_found(
        EventStore $eventStore,
        InputInterface $input,
        OutputStyle $output,
        UserService $userService
    ) {
        $input->getArgument('cuid')->shouldBeCalledOnce()->willReturn((string)$min = $this->randomMin());
        $userService->createCanonicalUserId((string)$min)->shouldBeCalledOnce()->willReturn($min);
        $eventStore->findByCanonicalUserId($min)->shouldBeCalledOnce()->willReturn(new DomainEvents());

        $output->writeln('<fg=red>No events were found for the given canonical user id</>');

        $this->handle()->shouldNotBe(0);
    }

    function it_should_display_the_sequence_of_events(
        EventStore $eventStore,
        InputInterface $input,
        OutputStyle $output,
        UserService $userService
    ) {
        $input->getArgument('cuid')->shouldBeCalledOnce()->willReturn((string)$min = $this->randomMin());
        $userService->createCanonicalUserId((string)$min)->shouldBeCalledOnce()->willReturn($min);

        $userId = UserId::fromString($this->createRandomAggregateId());
        $userWasRegistered = $this->createObjectBuilderFor(UserWasRegistered::class)
            ->set('userId', $userId)
            ->set('occurredOn', \microtime(true) - 120)
            ->build()
        ;

        $mranWasNominated = $this->createObjectBuilderFor(MranWasNominated::class)
            ->set('userId', $userId)
            ->set('occurredOn', \microtime(true) - 60)
            ->build()
        ;

        $eventStore->findByCanonicalUserId($min)->shouldBeCalledOnce()->willReturn(
            new DomainEvents($userWasRegistered, $mranWasNominated)
        );

        $output->writeln(
            \sprintf(
                '<fg=yellow>%s: %s: </><fg=green>%s</>',
                $userWasRegistered->aggregateId(),
                $userWasRegistered->occurredOn()->format('Y-m-d H:i:s T'),
                UserWasRegistered::name()
            )
        )->shouldBeCalledOnce();

        $output->writeln(
            \sprintf(
                '<fg=yellow>%s: %s: </><fg=green>%s</>',
                $mranWasNominated->aggregateId(),
                $mranWasNominated->occurredOn()->format('Y-m-d H:i:s T'),
                MranWasNominated::name()
            )
        )->shouldBeCalledOnce();

        $this->handle()->shouldBe(0);
    }
}

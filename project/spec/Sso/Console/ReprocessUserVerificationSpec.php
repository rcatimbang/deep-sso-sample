<?php

namespace spec\App\Sso\Console;

use App\Sso\Console\ReprocessUserVerification;
use Elasticsearch\Client;
use Illuminate\Console\Command;
use Illuminate\Contracts\Cache\Repository as Cache;
use Illuminate\Contracts\Foundation\Application;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use SmartPldt\Deep\Sso\CanonicalUserId\CanonicalUserId;
use SmartPldt\Deep\Sso\EncryptionService;
use SmartPldt\Deep\Sso\Event\EventBus;
use SmartPldt\Deep\Sso\UserRegistrationReVerified;
use SmartPldt\Deep\Sso\UserService;
use spec\App\ObjectBehaviorHelper;

/**
 * @mixin ReprocessUserVerification
 */
class ReprocessUserVerificationSpec extends ObjectBehavior
{
    use ObjectBehaviorHelper;

    function let(
        Application $laravel,
        Client $elasticsearch,
        EncryptionService $encryptionService,
        EventBus $eventBus,
        UserService $userService,
        CanonicalUserId $canonicalUserId,
        Cache $cache
    ) {
        $laravel->get('service.sso.elasticsearch_client')->willReturn($elasticsearch);
        $laravel->get(EncryptionService::class)->willReturn($encryptionService);
        $laravel->get(EventBus::class)->willReturn($eventBus);
        $laravel->get(UserService::class)->willReturn($userService);
        $laravel->get(Cache::class)->willReturn($cache);
        $cache->get('reprocess-user-verification-last-timestamp', 0.0)->willReturn(0.0);
        $cache->put('reprocess-user-verification-last-timestamp', Argument::type('float'))->willReturn(true);

        $elasticsearch->scroll(Argument::cetera())->willReturn([]);

        $userService->createCanonicalUserId(Argument::cetera())->willReturn($canonicalUserId);

        $this->setLaravel($laravel);
    }

    function it_should_be_an_Artisan_Command()
    {
        $this->shouldHaveType(Command::class);
    }

    function it_should_return_the_command_signature_or_name()
    {
        $this->getName()->shouldBe('sso:reprocess-user-verification');
    }

    function it_should_return_its_description()
    {
        $this->getDescription()
            ->shouldBe('Forward UserRegistrationVerification event for re-processing by other services');
    }

    function it_should_re_send_UserRegistrationReVerified_events_to_the_EventBus(
        Application $laravel,
        Client $elasticsearch,
        EncryptionService $encryptionService,
        EventBus $eventBus
    ) {
        $hits = $this->mergePayload(
            $this->createRandomOldUserRegistrationVerifiedPayload(10),
            $this->createRandomNewUserRegistrationVerifiedPayload(10)
        );
        $laravel->get('parameter.sso.event_store_index')->willReturn($eventStoreIndex = 'tindahan-ng-atoys-mo');

        $elasticsearchHits = $this->createElasticsearchHits($hits, $encryptionService);
        $scrollResponses = \iterator_to_array($this->chunkHitsIntoScrollResponses($elasticsearchHits, 4));

        $eventBus->send(Argument::type(UserRegistrationReVerified::class))->shouldBeCalledTimes(10);

        $elasticsearch
            ->search(self::searchCriteria($eventStoreIndex))
            ->shouldBeCalledOnce()
            ->willReturn(\array_shift($scrollResponses));

        foreach ($scrollResponses as $scrollResponse) {
            $elasticsearch
                ->scroll(['scroll_id' => (string)($scrollResponse['_scroll_id'] - 1), 'scroll' => '1m'])
                ->shouldBeCalledOnce()
                ->willReturn($scrollResponse)
            ;
        }

        $this->handle()->shouldReturn(0);
    }

    private function createRandomOldUserRegistrationVerifiedPayload(int $count): \Generator
    {
        for ($i = 0; $i < $count; $i++) {
            yield \json_decode(
                self::oldUserRegistrationVerifiedJson(
                    $this->createRandomAggregateId(),
                    \microtime(true),
                    (string)$this->randomMin(),
                    (string)$this->randomMin()
                ),
                true
            );
        }
    }

    private static function oldUserRegistrationVerifiedJson(
        string $aggregateId,
        float $occurredOn,
        string $canonicalUserId,
        string $mran
    ): string {
        return \sprintf(<<<'EVENT_AS_JSON'
            {
              "event_name": "pldt-smart.deep.sso.user_registration_verified",
              "aggregate_id": "%s",
              "occurred_on": %f,
              "body": {
                "envelope": [],
                "payload": {
                  "mran": "%s",
                  "canonical_user_id": "%s"
                }
              }
            }
            EVENT_AS_JSON,
            $aggregateId,
            $occurredOn,
            $mran,
            $canonicalUserId
        );
    }

    private function createRandomNewUserRegistrationVerifiedPayload(int $count): \Generator
    {
        for ($i = 0; $i < $count; $i++) {
            yield \json_decode(
                self::newUserRegistrationVerifiedJson(
                    $this->createRandomAggregateId(),
                    \microtime(true),
                    (string)$this->randomMin(),
                    (string)$this->randomMin()
                ),
                true
            );
        }
    }

    private static function newUserRegistrationVerifiedJson(
        string $aggregateId,
        float $occurredOn,
        string $canonicalUserId,
        string $mran
    ): string {
        return \sprintf(<<<'EVENT_AS_JSON'
            {
              "event_name": "pldt-smart.deep.sso.user_registration_verified",
              "aggregate_id": "%s",
              "occurred_on": %f,
              "body": {
                "envelope": {
                  "canonical_user_id": "%s"
                },
                "payload": {
                  "mran": "%s"
                }
              }
            }
            EVENT_AS_JSON,
            $aggregateId,
            $occurredOn,
            $canonicalUserId,
            $mran
        );
    }

    private function mergePayload(\Generator $events, \Generator $anotherEvents): \Iterator
    {
        $iterator = new \AppendIterator();
        $iterator->append($events);
        $iterator->append($anotherEvents);

        return $iterator;
    }

    private function createElasticsearchHits(
        \Iterator $events,
        $encryptionService
    ): array {
        $hits = [];
        $decryptedReturnValues = [];
        foreach ($events as $event) {
            $hits[] = $this->createSingleHit($event['aggregate_id'], $event['occurred_on']);
            $decryptedReturnValues[] = \json_encode($event['body']);
        }
        $encryptionService->decrypt('ang-sekreto-ni-ate')->willReturn(...$decryptedReturnValues);

        return $hits;
    }

    private function createSingleHit(string $aggregateId, float $occurredOn): array
    {
        return \json_decode(
            \sprintf(
                <<<'ES_HIT'
                {
                    "_source": {
                        "event_name": "pldt-smart.deep.sso.user_registration_verified",
                        "aggregate_id": "%s",
                        "occurred_on": %f,
                        "body": "ang-sekreto-ni-ate"
                    }
                }
                ES_HIT,
                $aggregateId,
                $occurredOn
            ),
            true
        );
    }

    private function chunkHitsIntoScrollResponses(array $hits, int $hitsPerChunk): \Generator
    {
        for ($offset = 0; $offset < \count($hits); $offset += $hitsPerChunk) {
            $pseudoScrollId = $pseudoScrollId ?? 1;
            yield $this->createElasticSearchScrollResult(
                \array_slice($hits, $offset, $hitsPerChunk),
                $pseudoScrollId++
            );
        }
    }

    private function createElasticSearchScrollResult(array $hits, int $scrollId): array
    {
        $encodedHits = \json_encode($hits);

        return \json_decode(
            <<<ES_RESULT
            {
                "_scroll_id": "{$scrollId}",
                "hits": {
                    "hits": {$encodedHits}
                }
            }
            ES_RESULT,
            true
        );
    }

    private static function searchCriteria(string $eventStoreIndex): array {
        return \json_decode(
            <<<SEARCH_CRITERIA
            {
                "index": "{$eventStoreIndex}",
                "size": 1000,
                "scroll": "2m",
                "body": {
                    "query": {
                        "bool": {
                            "must": [
                                {
                                    "term": {
                                        "event_name": "pldt-smart.deep.sso.user_registration_verified"
                                    }
                                },
                                {
                                    "range": {
                                        "occurred_on": {
                                            "gte": 0
                                        }
                                    }
                                }
                            ]
                        }
                    },
                    "sort": {
                        "occurred_on": {
                            "order": "asc"
                        }
                    }
                }
            }
            SEARCH_CRITERIA,
            true
        );
    }
}

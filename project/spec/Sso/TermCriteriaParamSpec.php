<?php

namespace spec\App\Sso;

use App\Exceptions\InvalidOperatorException;
use App\Sso\TermCriteriaParam;
use PhpSpec\ObjectBehavior;

class TermCriteriaParamSpec extends ObjectBehavior
{
    function let()
    {
        $this->beConstructedWith('name', '=','john dog');
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(TermCriteriaParam::class);
    }

    function it_should_not_accept_invalid_operators()
    {
        $this->beConstructedWith('asdf', 'asdf', 'asdf');
        $this->shouldThrow(InvalidOperatorException::class)->duringInstantiation();
    }

    function it_should_return_asArray()
    {
        $this->toArray()->shouldReturn([
            'term' => [
                'name' => 'john dog'
            ]
        ]);
    }

    function it_should_return_as_string()
    {
        $this->__toString()->shouldReturn(\json_encode([
            'term' => [
                'name' => 'john dog'
            ]
        ]));
    }

    function it_should_create_thru_factory_method_equality()
    {
        $this->beConstructedThrough('equality', [$key = 'coffee', $value ='coldbrew']);
        $expectedArray = \json_decode(<<<JSON
        {
            "term": {
                "{$key}": "{$value}"
            }
        }
        JSON,
        true
        );

        $this->toArray()->shouldBe($expectedArray);
        $this->__toString()->shouldBe(\json_encode($expectedArray));
    }
}

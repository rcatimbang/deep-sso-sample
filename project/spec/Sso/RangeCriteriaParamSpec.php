<?php

namespace spec\App\Sso;

use App\Sso\RangeCriteriaParam;
use PhpSpec\ObjectBehavior;

/**
 * @mixin RangeCriteriaParam
 */
class RangeCriteriaParamSpec extends ObjectBehavior
{
    function let()
    {
        $this->beConstructedWith('occurred_on', '2021-01-01 00:00:00');
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(RangeCriteriaParam::class);
    }

    function it_should_return_asArray()
    {
        $this->toArray()->shouldReturn([
            'range' => [
                'occurred_on' => [
                    'gt' => '2021-01-01 00:00:00'
                ]
            ]
        ]);
    }

    function it_should_return_as_string()
    {
        $this->__toString()->shouldReturn(\json_encode([
            'range' => [
                'occurred_on' => [
                    'gt' => '2021-01-01 00:00:00'
                ]
            ]
        ]));
    }
}

<?php

namespace spec\App\Sso\VerificationCode;

use App\Sso\VerificationCode\OtpVerificationCodeService;
use Elasticsearch\Client;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Psr\Container\ContainerInterface;
use SmartPldt\Deep\Sso\CanonicalUserId\Min;
use SmartPldt\Deep\Sso\CanonicalUserId\PlainSha256;
use SmartPldt\Deep\Sso\VerificationCode\Service as VerificationCodeService;

/**
 * @mixin OtpVerificationCodeService
 */
class OtpVerificationCodeServiceSpec extends ObjectBehavior
{
    function let(ContainerInterface $container, Client $elasticSearch)
    {
        $container->get('service.sso.elasticsearch_client')->willReturn($elasticSearch);
        $container->get('parameter.sso.otp_code_length')->willReturn(6);

        $this->beConstructedThrough('fromContainer', [$container]);
    }

    function it_should_be_a_VerificationCode_Service()
    {
        $this->shouldHaveType(VerificationCodeService::class);
    }

    function it_should_generate_verification_code_for_CanonicalUserId(Client $elasticSearch)
    {
        $canonicalUserId = Min::create('09192221119', $hash = PlainSha256::create());

        $params = Argument::allOf(
            Argument::withEntry('index', 'otp'),
            Argument::withEntry('id', $hash),
            Argument::withEntry('body', Argument::allOf(
                Argument::withEntry('otp', Argument::type('string')),
                Argument::withEntry('valid_until_timestamp', Argument::type('int')),
            )),
        );

        $elasticSearch->index($params)->shouldBeCalledOnce();

        $this->generateCodeFor($canonicalUserId)->shouldMatch('/^(\d){6}/');
    }

    public function it_should_verify_code_that_is_generated_for_CanonicalUserId(Client $elasticSearch)
    {
        $canonicalUserId = Min::create('09192221119', $hash = PlainSha256::create());
        $params['index'] = 'otp';
        $params['id'] = (string)$hash;

        $result['_source']['otp'] = $otp = (string)mt_rand(111111, 999999);
        $result['_source']['valid_until_timestamp'] = \time() + 300;

        $elasticSearch->get($params)->shouldBeCalled()->willReturn($result);
        $this->verificationCodeIsValid($canonicalUserId, $otp)->shouldReturn(true);
    }

    public function it_should_return_false_when_code_is_empty()
    {
        $canonicalUserId = Min::create('09192221119', $hash = PlainSha256::create());

        $this->verificationCodeIsValid($canonicalUserId, '')->shouldBe(false);
    }

    public function it_should_return_false_when_code_is_incorrect(Client $elasticSearch)
    {
        $canonicalUserId = Min::create('09192221119', $hash = PlainSha256::create());
        $params['index'] = 'otp';
        $params['id'] = (string)$hash;

        $result['_source']['otp'] = (string)mt_rand(111111, 999999);
        $result['_source']['valid_until_timestamp'] = \time() + 300;

        $elasticSearch->get($params)->shouldBeCalled()->willReturn([]);
        $this->verificationCodeIsValid($canonicalUserId, 'incorrect-code')->shouldReturn(false);
    }

    public function it_should_return_false_when_otp_is_expired(Client $elasticSearch)
    {
        $canonicalUserId = Min::create('09192221119', $hash = PlainSha256::create());
        $params['index'] = 'otp';
        $params['id'] = (string)$hash;

        $result['_source']['otp'] = $otp = (string)mt_rand(111111, 999999);
        $result['_source']['valid_until_timestamp'] = \time() - 1;

        $elasticSearch->get($params)->shouldBeCalled()->willReturn([]);
        $this->verificationCodeIsValid($canonicalUserId, $otp)->shouldReturn(false);
    }
}

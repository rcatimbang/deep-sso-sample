<?php

namespace spec\App\Sso\VerificationCode;

use App\Sso\TxtBoxService;
use App\Sso\VerificationCode\SmsTransport;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use SmartPldt\Deep\Sso\CanonicalUserId\{Email, Min, PlainSha256};
use SmartPldt\Deep\Sso\VerificationCode\Transport;

/**
 * @mixin SmsTransport
 */
class SmsTransportSpec extends ObjectBehavior
{
    function let(TxtBoxService $txtBoxService)
    {
        $this->beConstructedWith($txtBoxService);
    }

    function it_is_a_Verification_Transport()
    {
        $this->shouldHaveType(Transport::class);
    }

    function it_should_only_send_verification_code_to_sms_recipients(TxtBoxService $txtBoxService)
    {
        $email = Email::create('ang-dagat@aymaalat.com', PlainSha256::create());
        $this->send($email, '123456');

        $txtBoxService->sendSMS(Argument::cetera())->shouldNotHaveBeenCalled();
    }

    function it_should_send_verification_code(TxtBoxService $txtBoxService)
    {
        $min = Min::create('09191234567', PlainSha256::create());
        $this->send($min, $code = '654321');

        $txtBoxService->sendSMS((string)$min, $code)->shouldHaveBeenCalledOnce();
    }

    function it_should_re_throw_errors_from_TxtBoxService(TxtBoxService $txtBoxService)
    {
        $txtBoxService->sendSMS(Argument::cetera())->willThrow($throwError = new \Exception('Ang error'));
        $min = Min::create('09191234567', PlainSha256::create());

        $this->shouldThrow($throwError)->during('send', [$min, '654321']);
    }
}

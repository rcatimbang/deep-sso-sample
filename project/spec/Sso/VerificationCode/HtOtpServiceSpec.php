<?php

namespace spec\App\Sso\VerificationCode;

use App\Sso\VerificationCode\HtOtpService;
use Multisys\PhpHotp\OTP;
use PhpSpec\ObjectBehavior;
use SmartPldt\Deep\Sso\CanonicalUserId\CanonicalUserId;
use SmartPldt\Deep\Sso\VerificationCode\TimestampAwareService as Service;

/**
 * @mixin HtOtpService
 */
class HtOtpServiceSpec extends ObjectBehavior
{
    private int $timestamp;

    function let(OTP $otp)
    {
        $this->beConstructedWith($otp, $this->timestamp = \mt_rand(1, 100));
    }

    function it_should_be_a_verification_code_service()
    {
        $this->shouldHaveType(Service::class);
    }

    function it_should_delegate_generation_of_verification_code(OTP $otp, CanonicalUserId $canonicalUserId)
    {
        $canonicalUserId->__toString()->willReturn($recipient = '+43');
        $verificationCode = (string)\mt_rand(111111,999999);
        $otp->create($recipient, $this->timestamp)->shouldBeCalledOnce()->willReturn($verificationCode);

        $this->generateCodeFor($canonicalUserId)->shouldReturn($verificationCode);
    }

    function it_should_delegate_validation_of_verification_code(OTP $otp, CanonicalUserId $canonicalUserId)
    {
        $canonicalUserId->__toString()->willReturn($recipient = '+43');
        $verificationCode = (string)\mt_rand(111111,999999);

        $otp->verify($recipient, $verificationCode, $this->timestamp)
            ->shouldBeCalledOnce()
            ->willReturn($validOrInvalid = (bool)\mt_rand(0,1));

        $this->verificationCodeIsValid($canonicalUserId, $verificationCode)->shouldReturn($validOrInvalid);
    }

    function it_should_accept_a_different_timestamp(OTP $otp, CanonicalUserId $canonicalUserId)
    {
        $verificationCode = (string)\mt_rand(111111,999999);
        $canonicalUserId->__toString()->willReturn($recipient = '+43');

        $otp->create($recipient, $this->timestamp)->shouldNotBeCalled();
        $otp->verify($recipient, $verificationCode, $this->timestamp)->shouldNotBeCalled();

        $newTimestamp = 999;
        $otp->create($recipient, $newTimestamp)->shouldBeCalledOnce()->willReturn($verificationCode);
        $otp->verify($recipient, $verificationCode, $newTimestamp)
            ->shouldBeCalledOnce()
            ->willReturn($validOrInvalid = (bool)\mt_rand(0,1));

        $anotherInstance = $this->setTimestamp($newTimestamp);
        $anotherInstance->generateCodeFor($canonicalUserId)->shouldReturn($verificationCode);
        $anotherInstance->verificationCodeIsValid($canonicalUserId, $verificationCode)->shouldReturn($validOrInvalid);
    }
}

<?php

namespace spec\App\Sso;

use App\Sso\ManyConsumerAtOnceAmqpEventBus;
use Illuminate\Log\LogManager;
use Interop\Amqp\AmqpBind;
use Interop\Amqp\AmqpConsumer;
use Interop\Amqp\AmqpContext;
use Interop\Amqp\AmqpMessage;
use Interop\Amqp\AmqpProducer;
use Interop\Amqp\AmqpQueue;
use Interop\Amqp\AmqpTopic;
use PhpSpec\ObjectBehavior;
use PhpSpec\Wrapper\Collaborator;
use Prophecy\Argument;
use SmartPldt\Deep\Sso\Event\EventBus;
use SmartPldt\Deep\Sso\Event\HandlesAnEventMessage;
use SmartPldt\Deep\Sso\UserId;
use SmartPldt\Deep\Sso\UserWasDeleted;
use spec\App\ObjectBehaviorHelper;

/**
 * @mixin ManyConsumerAtOnceAmqpEventBus
 */
class ManyConsumerAtOnceAmqpEventBusSpec extends ObjectBehavior
{
    private const EXPECTED_AMQP_TOPIC_TYPE = AmqpTopic::TYPE_FANOUT;

    private const EXPECTED_RECEIVE_TIMEOUT = 5000;

    private const RECEIVE_INTERVAL_IN_MICROSECONDS = 0;

    private const MESSAGE_TIME_TO_LIVE_IN_MILLISECONDS = 120000;

    use ObjectBehaviorHelper;

    function let(
        AmqpContext $context,
        AmqpTopic $topic,
        AmqpQueue $queue,
        AmqpProducer $producer,
        AmqpConsumer $consumer,
        AmqpMessage $message,
        LogManager $logManager
    ) {
        $context->createTopic(Argument::type('string'))->willReturn($topic);
        $context->createProducer()->willReturn($producer);
        $context->createQueue(Argument::type('string'))->willReturn($queue);
        $context->createConsumer($topic)->willReturn($consumer);
        $context->bind(Argument::that(self::containsBoundTopicAndQueue($topic, $queue)));
        $context->declareTopic($topic);

        $producer->setTimeToLive(self::MESSAGE_TIME_TO_LIVE_IN_MILLISECONDS)->willReturn($producer);
        $consumer->receive(self::EXPECTED_RECEIVE_TIMEOUT)->willReturn($message, null);

        $this->beConstructedWith($context, $logManager);
    }

    function it_should_be_an_EventBus()
    {
        $this->shouldHaveType(EventBus::class);
    }

    function it_should_send_DomainEvent_as_message(
        AmqpContext $context,
        AmqpTopic $topic,
        AmqpProducer $producer,
        AmqpMessage $message
    ) {
        $topic->setType(self::EXPECTED_AMQP_TOPIC_TYPE)->shouldBeCalledOnce();

        /** @var UserWasDeleted $event */
        $event = $this->createObjectBuilderFor(UserWasDeleted::class)
            ->set('userId', UserId::fromString($this->createRandomAggregateId()))
            ->set('occurredOn', \microtime(true))
            ->build();
        $context->createMessage((string)$event)->shouldBeCalledOnce()->willReturn($message);

        $this->send($event);

        $context->createTopic($event::name())->shouldHaveBeenCalledOnce();
        $context->declareTopic($topic)->shouldHaveBeenCalledOnce();
        $producer->send($topic, $message)->shouldHaveBeenCalledOnce();
        $producer->setTimeToLive(self::MESSAGE_TIME_TO_LIVE_IN_MILLISECONDS)->shouldBeCalledOnce();
    }

    // TODO: perhaps separate send and receive concerns?
    function it_should_receive_encoded_DomainEvent_messages(
        AmqpContext $context,
        AmqpTopic $topic,
        AmqpMessage $message,
        AmqpConsumer $consumer,
        AmqpQueue $queue,
        HandlesAnEventMessage $eventHandler
    ) {
        $consumer
            ->receive(self::EXPECTED_RECEIVE_TIMEOUT)
            ->shouldBeCalledTimes(3)
            ->willReturn($message, $message, null);
        $consumer->acknowledge($message)->shouldBeCalledTimes(2);
        $context->bind(Argument::that(self::containsBoundTopicAndQueue($topic, $queue)))->shouldBeCalledOnce();
        $context->createConsumer($topic)->shouldBeCalledOnce()->willReturn($consumer);
        $message->getBody()->shouldBeCalledTimes(2)->willReturn(...$messages = ['hindi natuloy', 'dahil sa covid']);
        $context->declareQueue($queue)->shouldBeCalledOnce()->willReturn(2);

        $this->receive('birthday-ng-lola-mo', $eventHandler);

        $context->declareTopic($topic)->shouldHaveBeenCalledOnce();

        $topic->setType(self::EXPECTED_AMQP_TOPIC_TYPE)->shouldHaveBeenCalledOnce();
        $eventHandler->__invoke($messages[0])->shouldHaveBeenCalledOnce();
        $eventHandler->__invoke($messages[1])->shouldHaveBeenCalledOnce();
    }

    function it_should_log_any_error_triggered_during_processing_of_DomainEvent_messages(
        AmqpContext $context,
        AmqpMessage $message,
        AmqpConsumer $consumer,
        AmqpQueue $queue,
        HandlesAnEventMessage $eventHandler,
        LogManager $logManager
    ) {
        $consumer
            ->receive(self::EXPECTED_RECEIVE_TIMEOUT)
            ->willReturn($message, $message, null);
        $consumer->acknowledge($message)->shouldBeCalledTimes(2);
        $message->getBody()->shouldBeCalledTimes(2)->willReturn(...$messages = ['hindi natuloy', 'dahil sa covid']);
        $context->declareQueue($queue)->shouldBeCalledOnce()->willReturn(2);
        $eventHandler
            ->__invoke(Argument::cetera())
            ->shouldBeCalledTimes(2)
            ->willThrow($exception = new \Exception('Nagalit pati lola mo'));

        $this->receive('birthday-ng-lola-mo', $eventHandler);

        $logManager->error($exception)->shouldHaveBeenCalledTimes(2);
    }

    // TODO: perhaps separate send and receive concerns?
    function it_should_create_a_consumer_only_once_for_every_event_name_it_handles(
        AmqpContext $context,
        AmqpTopic $topic,
        AmqpMessage $message,
        AmqpConsumer $consumer,
        AmqpQueue $queue,
        HandlesAnEventMessage $eventHandler
    ) {
        $consumer
            ->receive(self::EXPECTED_RECEIVE_TIMEOUT)
            ->willReturn($message, $message, $message, $message, null);
        $consumer->acknowledge($message)->shouldBeCalledTimes(4);
        $message->getBody()->shouldBeCalledTimes(4)->willReturn('walang-dumating');
        $context->declareQueue($queue)->shouldBeCalledOnce()->willReturn(4);
        $eventName = 'birthday-ng-lola-mo';

        $this->receive($eventName, $eventHandler);
        $this->receive($eventName, $eventHandler);

        $context->createTopic($eventName)->shouldHaveBeenCalledOnce();
        $topic->setType(self::EXPECTED_AMQP_TOPIC_TYPE)->shouldHaveBeenCalledOnce();
        $context->declareTopic($topic)->shouldHaveBeenCalledOnce();
        $context->createQueue($eventName)->shouldHaveBeenCalledOnce();
        $context->declareQueue($queue)->shouldHaveBeenCalledOnce();
        $context->bind(Argument::cetera())->shouldHaveBeenCalledOnce();
        $context->createConsumer($topic)->shouldHaveBeenCalledOnce();
    }

    private static function containsBoundTopicAndQueue(Collaborator $topic, Collaborator $queue): callable
    {
        return function (AmqpBind $bind) use ($topic, $queue) {
            return $bind->getTarget() === $topic->getWrappedObject()
                && $bind->getSource() === $queue->getWrappedObject();
        };
    }
}

<?php

namespace spec\App\Providers;

use App\Providers\Exception;
use PhpSpec\ObjectBehavior;

/**
 * @mixin Exception
 */
class ExceptionSpec extends ObjectBehavior
{
    function it_should_be_an_Exception()
    {
        $this->shouldHaveType(\Exception::class);
    }
}

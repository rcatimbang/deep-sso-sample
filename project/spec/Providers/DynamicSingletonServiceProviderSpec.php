<?php

declare(strict_types=1);

namespace spec\App\Providers;

use App\Providers\DynamicSingletonServiceProvider;
use App\Providers\Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Support\DeferrableProvider;
use Illuminate\Support\ServiceProvider;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

/**
 * @mixin DynamicSingletonServiceProvider
 */
class DynamicSingletonServiceProviderSpec extends ObjectBehavior
{
    function let(Application $app)
    {
        $this->beConstructedWith($app);
    }

    function it_should_be_a_Laravel_DeferrableProvider()
    {
        $this->shouldHaveType(ServiceProvider::class);
        $this->shouldHaveType(DeferrableProvider::class);
    }

    function it_should_not_register_a_service_id_more_than_once(Application $app)
    {
        $app->singleton($serviceId = 'foobar', Argument::type(\Closure::class))->shouldBeCalledOnce();

        $this->registerServiceOrParameter($serviceId, function () {});
        $this->shouldThrow(Exception::class)->during('registerServiceOrParameter', [$serviceId, function () {}]);
    }

    function it_should_return_the_services_or_parameters_it_registered()
    {
        $this->registerServiceOrParameter('foo', function () {});
        $this->registerServiceOrParameter('bar', function () {});
        $this->registerServiceOrParameter('baz', function () {});

        $this->provides()->shouldBe([
            'foo',
            'bar',
            'baz'
        ]);
    }
}

<?php

declare(strict_types=1);

namespace spec\App;

use PhpSpec\Wrapper\Collaborator;
use SmartPldt\Deep\Sso\CanonicalUserId\Min;
use SmartPldt\Deep\Sso\CanonicalUserId\PlainSha256;
use SmartPldt\Deep\Sso\UserWasRegistered;

trait ObjectBehaviorHelper
{
    private function randomMin(): Min
    {
        return Min::create(
            '0919' . \substr(\str_shuffle('1234567890'), 0, 7),
            PlainSha256::create()
        );
    }

    /**
     * @return object
     */
    protected function createFakeInstanceOf(string $fqcn)
    {
        return (new \ReflectionClass($fqcn))->newInstanceWithoutConstructor();
    }

    protected function setPropertyOf($fakeObject, string $property, $value): void
    {
        $property = (new \ReflectionProperty(\get_class($fakeObject), $property));
        $property->setAccessible(true);
        $property->setValue($fakeObject, $value);
    }

    protected function createUserWasRegisteredEvent(
        string $argon2IdPasswordHash,
        int $timestamp,
        Collaborator $userService
    ): UserWasRegistered {
        $userService
            ->createCanonicalUserId('09181234567')
            ->shouldBeCalledOnce()
            ->willReturn($canonicalUserId = Min::create('09181234567', PlainSha256::create()));

        return UserWasRegistered::fromArray(
            [
                'event_name' => 'pldt-smart.deep.sso.user_was_registered',
                'aggregate_id' => $this->createRandomAggregateId(),
                'occurred_on' => $timestamp,
                'canonical_user_id_hash' => $canonicalUserId->hash(),
                'body' => [
                    'envelope' => [],
                    'payload' => [
                        'canonical_user_id' => (string)$canonicalUserId,
                        'password_hash' => $argon2IdPasswordHash
                    ]
                ]
            ],
            $userService->getWrappedObject()
        );
    }

    private function createRandomAggregateId(): string
    {
        return \hash('crc32', \random_bytes(4))
            . '-1021-4189-9683-37ac'
            . \hash('crc32', \random_bytes(4))
        ;
    }

    private function createObjectBuilderFor(string $fullyQualifiedClassName): object
    {
        $object = (new \ReflectionClass($fullyQualifiedClassName))->newInstanceWithoutConstructor();

        return new class ($object)
        {
            private $object;

            public function __construct($object)
            {
                $this->object = $object;
            }

            public function set(string $property, $value): self
            {
                $property = (new \ReflectionProperty(\get_class($this->object), $property));
                $property->setAccessible(true);
                $property->setValue($this->object, $value);

                return $this;
            }

            public function build(): object
            {
                return $this->object;
            }
        };
    }

    private function randomCrc32Hash(): string
    {
        return \hash('crc32', \random_bytes(4));
    }
}

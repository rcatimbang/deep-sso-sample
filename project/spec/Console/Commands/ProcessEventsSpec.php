<?php

namespace spec\App\Console\Commands;

use App\Console\Commands\ProcessEvents;
use Illuminate\Console\Command;
use Illuminate\Contracts\Cache\Repository as Cache;
use Illuminate\Contracts\Container\Container;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use SmartPldt\Deep\Sso\Event\EventBus;
use SmartPldt\Deep\Sso\Event\HandlesAnEventMessage;
use SmartPldt\Deep\Sso\EventStore;
use SmartPldt\Deep\Sso\Notifier;
use SmartPldt\Deep\Sso\UserService;
use SmartPldt\Deep\Sso\VerificationCode\Service;
use SmartPldt\Deep\Sso\VerificationCode\Transport;

/**
 * @mixin ProcessEvents
 */
class ProcessEventsSpec extends ObjectBehavior
{
    function let(
        Container $container,
        EventBus $eventBus,
        UserService $userService,
        Service $service,
        Transport $transport,
        EventStore $eventStore,
        Notifier $notifier,
        Cache $cache
    )
    {
        $this->setLaravel($container);
        $container->get(EventBus::class)->willReturn($eventBus);
        $container->get(UserService::class)->willReturn($userService);
        $container->get(Service::class)->willReturn($service);
        $container->get(Transport::class)->willReturn($transport);
        $container->get(EventStore::class)->willReturn($eventStore);
        $container->get(Notifier::class)->willReturn($notifier);
        $container->get(Cache::class)->willReturn($cache);
    }

    function it_should_be_a_Command()
    {
        $this->shouldHaveType(Command::class);
    }

    function it_should_return_its_name()
    {
        $this->getName()->shouldBe('sso:process-events');
    }

    function it_should_return_its_description()
    {
        $this->getDescription()->shouldBe('Delegate processing of events received from EventBus to its event handler');
    }

    function it_should_delegate_processing_of_received_Events_to_event_handlers(
        EventBus $eventBus,
        Container $container,
        Cache $cache
    ) {
        $container->get('parameter.sso.event_handlers')->shouldBeCalledOnce()->willReturn($eventHandlers = [
            new ConcreteEventHandler(),
            new ConcreteEventHandler(),
            new ConcreteEventHandler(),
        ]);
        $container->get('parameter.sso.console.minimum_execution_time')->shouldBeCalledOnce()->willReturn(0);
        $eventBus->receive(Argument::type('string'), Argument::type(HandlesAnEventMessage::class))
            ->shouldBeCalledTimes(\count($eventHandlers));
        $cache->get($this->instanceId(), 0)->willReturn(0);
        $cache->put(
            $this->instanceId(),
            Argument::type('integer'),
            ProcessEvents::MINIMUM_INSTANCE_TTL_IN_SECONDS
        )->shouldBeCalled();
        $cache->delete($this->instanceId())->shouldBeCalledOnce();

        $this->handle()->shouldBe(0);
    }

    function it_should_exit_when_another_instance_is_still_running_within_the_minimum_instance_ttl(Cache $cache)
    {
        $hashOfFqcnAsInstanceIdId = $this->instanceId();
        $timestamp5SecondsAgo = \time() - 5;
        $cache->get($hashOfFqcnAsInstanceIdId, 0)->shouldBeCalledOnce()->willReturn($timestamp5SecondsAgo);
        $this->handle()->shouldNotBe(0);
    }

    private function instanceId(): string
    {
        return \sprintf('deep_sso_event_consumer_last_timestamp_%s', \hash('crc32', ProcessEvents::class));
    }
}

class ConcreteEventHandler implements HandlesAnEventMessage
{
    public function eventNameToHandle(): string
    {
        return 'burol-ng-pusa-mo';
    }

    public function __invoke(string $encodedDomainEvent): void
    {
    }
}

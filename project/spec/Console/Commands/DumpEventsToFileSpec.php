<?php

namespace spec\App\Console\Commands;

use App\Console\Commands\DumpEventsToFile;
use Illuminate\Console\Command;
use PhpSpec\ObjectBehavior;

/**
 * @mixin DumpEventsToFile
 */
class DumpEventsToFileSpec extends ObjectBehavior
{
    function it_is_a_command()
    {
        $this->shouldHaveType(Command::class);
    }

    function it_should_return_its_name()
    {
        $this->getname()->shouldBe('sso:dump-events-to-file');
    }

    function it_should_return_its_description()
    {
        $this->getDescription()->shouldBe('Dump events payload to a file');
    }
}

<?php

namespace App\Repositories;

use App\Exceptions\ParseJwtException;
use Exception;
use Lcobucci\JWT\Configuration as JwtService;
use Lcobucci\JWT\Token;

class AuthRepository
{
    public function verifyToken(Token $token): bool
    {
        return $this->jwtService()->validator()->validate($token, ...$this->jwtService()->validationConstraints());
    }

    public function parseToken(string $token): Token
    {
        try {
            return $this->jwtService()->parser()->parse($token);
        } catch (Exception $e) {
            throw new ParseJwtException();
        }
    }

    private function jwtService(): JwtService
    {
        return app()->get('service.sso.jwt_service');
    }
}

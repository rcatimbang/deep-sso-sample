<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class LoginRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'canonical_user_id' => Rule::requiredIf(!request()->header('X-Device-Token')),
            'password' => Rule::requiredIf(!request()->header('X-Device-Token'))
        ];
    }
}

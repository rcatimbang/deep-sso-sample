<?php

namespace App\Http\Middleware;

use App\Repositories\AuthRepository;
use Closure;
use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Http\Request;
use Lcobucci\JWT\Configuration as JwtService;
use SmartPldt\Deep\Sso\UserTokenWhitelist;

class JWTAuthMiddleware
{
    public function handle(Request $request, Closure $next)
    {
        $bearer = $request->bearerToken();

        if (!$bearer) {
            return response()->json(trans('errors.noToken'), 401);
        }

        try {
            $authRepo = new AuthRepository();
            $token = $authRepo->parseToken($bearer);
            $user_id = $token->claims()->get('user_id');

            if (!$this->getTokenWhitelist()->isWhitelisted($user_id, $bearer)) {
                return response()->json(trans('errors.invalidToken'), 401);
            }

            if (!$authRepo->verifyToken($token)) {
                return response()->json(trans('errors.invalidToken'), 401);
            }

            if (!$this->jwtService()->validator()->validate($token, ...$this->jwtService()->validationConstraints())) {
                return response()->json(trans('errors.invalidToken'), 401);
            }
            $request->merge(compact('user_id'));
        } catch (Exception $e) {
            return response()->json(trans('errors.invalidToken'), 401);
        }

        return $next($request);
    }

    private function getTokenWhitelist(): UserTokenWhitelist
    {
        return $this->container()->get(UserTokenWhitelist::class);
    }

    private function container(): Application
    {
        return app();
    }

    private function jwtService(): JwtService
    {
        return $this->container()->get('service.sso.jwt_service');
    }
}

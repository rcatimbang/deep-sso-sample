<?php

namespace App\Http\Controllers;

use App\Http\Requests\ChangePasswordRequest;
use App\Http\Requests\NewPasswordRequest;
use App\Http\Requests\RegisterRequest;
use App\Http\Requests\VerifyUserRequest;
use Elasticsearch\Common\Exceptions\Missing404Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Http\Request;
use Lcobucci\JWT\Parser;
use SmartPldt\Deep\Sso\CanonicalUserId\InvalidCanonicalUserId;
use SmartPldt\Deep\Sso\InvalidPassword;
use SmartPldt\Deep\Sso\InvalidUserId;
use SmartPldt\Deep\Sso\User;
use SmartPldt\Deep\Sso\UserNotFoundError;
use SmartPldt\Deep\Sso\UserService;
use SmartPldt\Deep\Sso\UserTokenWhitelist;
use SmartPldt\Deep\Sso\VerificationCode\Service as VerificationCodeService;
use Lcobucci\JWT\Configuration as JwtService;

class UserController extends Controller
{
    public function post(UserService $userService)
    {
    }

    public function register(RegisterRequest $request)
    {
        try {
            $userService = $this->userService();
            $requestBody = $request->json()->all();
            $canonicalUserId = $requestBody['canonical_user_id'];
            $mran = $userService->createCanonicalUserId($requestBody['mran'] ?? $canonicalUserId);
            $user = $userService->findUserByCanonicalUserId($canonicalUserId);

            if ($user->isVerified()) {
                return response()->json(
                    __('errors.already_registered'),
                    400,
                    ['X-Status-Reason' => 'Canonical user id is already registered']
                );
            }

            $userService->saveUser($user->nominateMran($mran)->setNewPassword($requestBody['password'], $userService));

            return response()->json(['user_id' => (string)$user->aggregateId()], 201);
        } catch (InvalidCanonicalUserId $exception) {
            return response()->json(__('errors.invalid_min'), 422);
        } catch (UserNotFoundError $exception) {
        } catch (Missing404Exception $exception) {
        } catch (\Throwable $exception) {
            throw $exception;
        }

        $user = User::register($canonicalUserId, $requestBody['password'], $userService)->nominateMran($mran);
        $userService->saveUser($user);

        return response()->json(['user_id' => (string)$user->aggregateId()], 201);
    }

    public function verifyUser(VerifyUserRequest $request, UserService $userService)
    {
        try {
            $requestBody = $request->json()->all();
            $user = $this->userService()->findUserByUserId($requestBody['user_id']);
            $mran = $this->userService()->createCanonicalUserId($requestBody['mran']);

            $verifiedUser = $user->verify($mran);

            if ((string)$verifiedUser->nominatedMran() === (string)$mran) {
                $this->userService()->saveUser($verifiedUser);
                $userToken = $userService->generateUserAccessToken($requestBody['user_id']);
                $deviceToken = $userService->generateDeviceAccessToken($requestBody['user_id']);

                return response()->json(
                    [
                        'token' => $userToken->token(),
                        'valid_until' => $userToken->validUntilTimestamp(),
                        'device_token' => $deviceToken->token(),
                        'device_valid_until' => $deviceToken->validUntilTimestamp()
                    ],
                    201
                );
            }
        } catch (\Throwable $exception) {
        }

        return response()->json(__('errors.invalid_otp'), 422);
    }

    public function changePassword(ChangePasswordRequest $request)
    {
        try {
            $userService = $this->userService();

            $user = $userService->findUserByUserId($request->user_id);
            $userWithPossiblyNewPassword = $user->changePassword(
                $request->input('old_password'),
                $request->input('new_password'),
                $userService
            );
            $userService->saveUser($userWithPossiblyNewPassword);

            if ($userWithPossiblyNewPassword->lastLoginFailureCode()) {
                return response()->json(__('errors.credentials'), 422);
            }

            if (!$this->passwordChanged($userWithPossiblyNewPassword, $user)) {
                return response()->json(__('errors.recently_used_password'), 422);
            }
        } catch (InvalidPassword $exception) {
            return response()->json(__('errors.invalid_password'), 422);
        }
        $userToken = $userService->generateUserAccessToken($user->userId());
        $deviceToken = $userService->generateDeviceAccessToken($user->userId());

        return response()->json([
            'token' => $userToken->token(),
            'valid_until' => $userToken->validUntilTimestamp()
        ], 201);
    }

    /**
     * @deprecated
     */
    public function requestPasswordReset(Request $request, UserService $userService)
    {
        try {
            $user = $userService->findUserByCanonicalUserId($request->input('canonical_user_id'));
            $userService->saveUser($user->requestNewPassword());

            return response()->json(['user_id' => (string)$user->userId()], 201);
        } catch (UserNotFoundError $e) {
            return response()->noContent(404);
        } catch (InvalidUserId $e) {
            return response()->noContent(400);
        }
    }

    public function verifyForgotPassword(NewPasswordRequest $request, UserService $userService)
    {
        try {
            $requestBody = $request->all();
            $user = $this->userService()->findUserByCanonicalUserId($requestBody['canonical_user_id']);

            $userWithPossiblyNewPassword = $user->setNewPassword($request->input('new_password'), $userService);
            $this->userService()->saveUser($userWithPossiblyNewPassword);

            if (!$this->passwordChanged($userWithPossiblyNewPassword, $user)) {
                return response()->json(__('errors.recently_used_password'), 422);
            }
        } catch (\Throwable $exception) {
            return response()->json(__('errors.forgot_password'), 422);
        }
        $userToken = $userService->generateUserAccessToken($user->userId());
        $deviceToken = $userService->generateDeviceAccessToken($user->userId());

        return response()->json([
            'token' => $userToken->token(),
            'valid_until' => $userToken->validUntilTimestamp(),
            'device_token' => $deviceToken->token(),
            'device_valid_until' => $deviceToken->validUntilTimestamp()
        ], 201);
    }

    /**
     * @deprecated
     */
    public function resendRegistrationOtp(Request $request)
    {
        $canonicalUserId = $request->canonical_user_id;
        $userService = $this->userService();

        try {
            $user = $userService->findUserByCanonicalUserId($canonicalUserId);

            if (!$user->isVerified()) {
                $user = $user->nominateMran($user->mran());
                $userService->saveUser($user);
            }
        } catch (UserNotFoundError $exception) {
            return response()->json(__('errors.user_not_yet_registered'));
        }

        return response(null, 201);
    }

    public function refreshAccessToken(Request $request)
    {
        if ($this->getTokenWhitelist()->isWhitelisted($request->user_id, $request->bearerToken())) {
            $userToken = $this->userService()->generateUserAccessToken($request->user_id);
            $user = $this->userService()->findUserByUserId($request->user_id);

            $anotherInstance = $user->declareActive();
            $this->userService()->saveUser($anotherInstance);

            return response()->json([
                'token' => $userToken->token(),
                'valid_until' => $userToken->validUntilTimestamp()
            ], 201);
        };
        return response()->json(__('errors.invalid_token'), 422);
    }

    private function getTokenWhitelist(): UserTokenWhitelist
    {
        return $this->container()->get(UserTokenWhitelist::class);
    }

    private function container(): Application
    {
        return app();
    }

    private function userService(): UserService
    {
        return $this->container()->get(UserService::class);
    }

    public function deleteUser(
        Request $request,
        UserService $userService,
        VerificationCodeService $verificationCodeService
    ) {
        $verificationCode = $this->verificationCodeFromRequest($request);
        $requestDeletion = $this->requestDeletion($request, $userService);
        $executeDeletion = $this->executeDeletion(...\func_get_args());

        if ($requestDeletion($verificationCode) || $executeDeletion($verificationCode)) {
            $userService->generateUserAccessToken($this->userIdFromRequest($request));
            $userService->generateDeviceAccessToken($this->userIdFromRequest($request));
            return response(null, 202);
        }

        return response(null, 405);
    }

    private function requestDeletion(
        Request $request,
        UserService $userService
    ): callable {
        return function (string $verificationCode) use ($request, $userService) {
            if (!$verificationCode) {
                $user = $userService->findUserByUserId($this->userIdFromRequest($request));
                $userService->saveUser($user->requestDeletion());

                return true;
            }

            return false;
        };
    }

    private function executeDeletion(
        Request $request,
        UserService $userService,
        VerificationCodeService $verificationCodeService
    ): callable {
        return function (string $verificationCode) use ($request, $userService, $verificationCodeService) {
            $user = $userService->findUserByUserId($this->userIdFromRequest($request));
            if ($verificationCodeService->verificationCodeIsValid($user->mran(), $verificationCode)) {
                $userService->saveUser($user->delete());

                return true;
            }

            return false;
        };
    }

    private function userIdFromRequest(Request $request): string
    {
        $jwtService = $this->jwtService();
        return $jwtService->parser()->parse($request->bearerToken())->claims()->get('user_id');
    }

    private function verificationCodeFromRequest(Request $request): string
    {
        return $request->json()->all()['verification_code'] ?? '';
    }

    private function passwordChanged(User $userWithNewPassword, User $user): bool
    {
        return $userWithNewPassword->passwordHistory()->toArray() != $user->passwordHistory()->toArray();
    }

    private function jwtService(): JwtService
    {
        return $this->container()->get('service.sso.jwt_service');
    }
}

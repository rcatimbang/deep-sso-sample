<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\Sso\UserService;
use Lcobucci\JWT\Parser;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use Lcobucci\JWT\Signer\Key;
use Lcobucci\JWT\Token;
use Psr\Container\ContainerInterface;
use SmartPldt\Deep\Sso\DeviceTokenWhitelist;
use SmartPldt\Deep\Sso\UserTokenWhitelist;

class LoginController extends Controller
{
    private UserService $userService;

    private ContainerInterface $container;

    public function __construct(UserService $userService, ContainerInterface $container)
    {
        $this->userService = $userService;
        $this->container = $container;
    }

    public function login(LoginRequest $request)
    {
        if ($tokenAsString = $request->header('X-Device-Token')) {
            try {
                $token = (new Parser())->parse($tokenAsString);
                $this->throwIfTokenIsNotVerifiedOrExpired($token);
                $this->throwIfDeviceTokenIsNotWhitelisted($token);

                $userAccessToken = $this->userService->generateUserAccessToken($token->getClaim('user_id'));
            } catch (\Throwable $e) {
                return response()->json(trans('errors.invalidToken'), 422);
            }

            return response()->json([
                'token' => $userAccessToken->token(),
                'valid_until' => $userAccessToken->validUntilTimestamp()
            ]);
        }

        try {
            $user = $this->userService->findUserByCanonicalUserId($request->input('canonical_user_id'));
            $user->userId();
        } catch (\Throwable $e) {
            return response()->json(__('errors.user_not_yet_registered'), 422);
        }

        $this->userService->saveUser($user = $user->login($request->input('password'), $this->userService));
        $loginFailureCode = $user->lastLoginFailureCode();

        if ($loginFailureCode) {
            return response()->json(['error_code' => (string)$loginFailureCode], 401);
        }

        $token = $this->userService->generateUserAccessToken($user->userId());
        $deviceAccessToken = $this->userService->generateDeviceAccessToken($user->userId());

        return response()->json([
            'token' => $token->token(),
            'valid_until' => $token->validUntilTimestamp(),
            'device_token' => $deviceAccessToken->token(),
            'device_valid_until' => $deviceAccessToken->validUntilTimestamp()
        ], 201);
    }

    /**
     * @throws \Exception
     */
    private function throwIfTokenIsNotVerifiedOrExpired(Token $token): void
    {
        $isVerified = $token->verify(new Sha256(), new Key($this->container->get('parameter.sso.jwt_secret')));
        $isExpired = $token->isExpired(\now());
        if (!$isVerified || $isExpired) {
            throw new \Exception('Token is expired or not verified.');
        }
    }

    /**
     * @throws \Exception
     */
    private function throwIfDeviceTokenIsNotWhitelisted(Token $token)
    {
        $userId = $token->getClaim('user_id');
        $deviceTokenWhiteList = $this->container->get(DeviceTokenWhitelist::class);
        if (!$deviceTokenWhiteList->isWhitelisted($userId, (string)$token)) {
            throw new \Exception('Token is not whitelisted');
        }
    }
}

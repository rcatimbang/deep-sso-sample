<?php

namespace App\Providers;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * @inheritDoc
     */
    public function __construct($app)
    {
        $serviceProviders = $this->resolveServiceProviders($app);
        foreach ($serviceProviders as $provider) {
            $app->register($provider);
        }

        parent::__construct($app);
    }

    private function isBehatTest($environment): bool
    {
        return 'behat' === $environment
            || ('production' !== $environment && 'behat' === request()->header('X-BehatTestContext', $environment))
        ;
    }

    private function resolveServiceProviders(Application $app): array
    {
        return $this->isBehatTest($app->environment())
            ? config('deep-sso.service-provider.behat')
            : config('deep-sso.service-provider.default');
    }
}

<?php

declare(strict_types=1);

namespace App\Providers;

use Illuminate\Contracts\Support\DeferrableProvider;
use Illuminate\Support\ServiceProvider;

class DynamicSingletonServiceProvider extends ServiceProvider implements DeferrableProvider
{
    /** @var array */
    private $serviceOrParameterIdsRegistered = [];

    final public function registerServiceOrParameter(string $serviceOrParameterId, callable $serviceCallable): void
    {
        $this->throwErrorIfIdIsAlreadyRegistered($serviceOrParameterId);

        $this->serviceOrParameterIdsRegistered[] = $serviceOrParameterId;
        $this->app->singleton($serviceOrParameterId, \Closure::fromCallable($serviceCallable));
    }

    final public function provides()
    {
        return $this->serviceOrParameterIdsRegistered;
    }

    /**
     * @throws Exception
     */
    private function throwErrorIfIdIsAlreadyRegistered(string $serviceOrParameterId): void
    {
        if (\in_array($serviceOrParameterId, $this->provides())) {
            throw new Exception("The service or parameter `{$serviceOrParameterId}` is already registered");
        }
    }
}

<?php

declare(strict_types=1);

namespace App\Sso;

use SmartPldt\Deep\Sso\Criteria;
use SmartPldt\Deep\Sso\CriteriaParameter;

final class ElasticsearchCriteria implements Criteria
{
    private array $criteriaParams;

    public function __construct(CriteriaParameter ...$criteriaParams)
    {
        $this->criteriaParams = $criteriaParams;
    }

    public function toArray(): array
    {
        return $this->criteriaParams;
    }

    public function __toString(): string
    {
        $paramAsArray = [];
        foreach ($this->criteriaParams as $param) {
            $paramAsArray[] = $param->toArray();
        }
        return \json_encode($paramAsArray);
    }
}

<?php

declare(strict_types=1);

namespace App\Sso;

use Elasticsearch\Client;
use Psr\Container\ContainerInterface;
use SmartPldt\Deep\Sso\AggregateId;
use SmartPldt\Deep\Sso\Criteria;
use SmartPldt\Deep\Sso\DomainEvent;
use SmartPldt\Deep\Sso\DomainEventFactory;
use SmartPldt\Deep\Sso\DomainEvents;
use SmartPldt\Deep\Sso\EncryptionService;
use SmartPldt\Deep\Sso\CanonicalUserId\CanonicalUserId;
use SmartPldt\Deep\Sso\EventStore as BaseEventStore;
use SmartPldt\Deep\Sso\UserId;

class EventStore implements BaseEventStore
{
    private const SECONDS_IN_A_DAY = 86400;

    private ContainerInterface $container;

    private int $dormancyInDays;

    private bool $waitForRefresh = true;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->dormancyInDays = $container->get('parameter.sso.dormancy_in_days');
    }

    private function elasticSearchClient(): Client
    {
        return $this->container->get('service.sso.elasticsearch_client');
    }

    public function disableWaitForRefreshOption(): self
    {
        $anotherInstance = clone $this;
        $anotherInstance->waitForRefresh = false;

        return $anotherInstance;
    }

    private function encryptionService(): EncryptionService
    {
        return $this->container->get(EncryptionService::class);
    }

    private function domainEventFactory(): DomainEventFactory
    {
        return $this->container->get('service.sso.domain_event_factory');
    }

    public function find(AggregateId $aggregateId): DomainEvents
    {
        $params = self::findByAggregateIdQuery(
            $this->container->get('parameter.sso.event_store_index'),
            (string)$aggregateId,
            $this->getTimestampOfLastSnapshot($aggregateId)
        );
        $this->elasticSearchClient()->indices()->refresh();
        $eventAsArray = $this->elasticSearchClient()->search($params);

        return new DomainEvents(
            ...$this->arrayOfEventsToArrayOfDomainEvent($eventAsArray['hits']['hits'] ?? [])
        );
    }

    private function getTimestampOfLastSnapshot(AggregateId $aggregateId): float
    {
        $params = self::lastSnapshotTimestampQuery(
            $this->container->get('parameter.sso.event_store_index'),
            (string)$aggregateId
        );
        $this->elasticSearchClient()->indices()->refresh();
        $eventAsArray = $this->elasticSearchClient()->search($params);
        $snapshot = $eventAsArray['hits']['hits'][0]['_source']['occurred_on'] ?? 0;

        return $snapshot;
    }

    public function findByCanonicalUserId(CanonicalUserId $canonicalUserId): DomainEvents
    {
        $params = [
            'index' => $this->container->get('parameter.sso.event_store_index'),
            'size' => 5000,
            'body' => [
                'query' => [
                    'term' => [
                        'canonical_user_id_hash' => $canonicalUserId->hash()
                    ]
                ]
            ]
        ];
        $this->elasticSearchClient()->indices()->refresh();
        $eventAsArray = $this->elasticSearchClient()->search($params);

        if ($aggregateId = $eventAsArray['hits']['hits'][0]['_source']['aggregate_id'] ?? '') {
            return $this->find(UserId::fromString($aggregateId));
        }

        return new DomainEvents();
    }

    private function arrayOfEventsToArrayOfDomainEvent(array $arrayOfEvents): \Generator
    {
        foreach ($arrayOfEvents as $anEventArray) {
            $decryptedBody = $this->encryptionService()->decrypt($anEventArray['_source']['body']);
            $anEventArray['_source']['body'] = \json_decode($decryptedBody, true);

            yield $this->domainEventFactory()->create(
                $anEventArray['_source']['event_name'],
                $anEventArray['_source']
            );
        }
    }

    public function save(DomainEvent $domainEvent): void
    {
        $eventAsArray = \json_decode((string)$domainEvent, true);
        $eventAsArray['body'] = $this->encryptionService()->encrypt(\json_encode($eventAsArray['body']));

        $saveParams = $this->saveParams($eventAsArray);
        $this->elasticSearchClient()->index($saveParams);
    }

    private function saveParams(array $eventAsArray): array
    {
        if ($this->waitForRefresh) {
            return [
                'index' => $this->container->get('parameter.sso.event_store_index'),
                'refresh' => 'wait_for',
                'body' => $eventAsArray,
            ];
        }

        return [
            'index' => $this->container->get('parameter.sso.event_store_index'),
            'body' => $eventAsArray,
        ];
    }

    public function dormantUserIds(int $size = 1): \Iterator
    {
        $params = self::dormantUserIdsQuery(
            $this->container->get('parameter.sso.event_store_index'),
            $this->dormantTimestamp()
        );

        $this->elasticSearchClient()->indices()->refresh();
        $results = $this->elasticSearchClient()->search($params)['hits']['hits'] ?? [];
        foreach ($results as $result) {
            yield UserId::fromString($result['_source']['aggregate_id']);
        }
    }

    private static function findByAggregateIdQuery(string $index, string $aggregateId, float $snapshotTimestamp): array
    {
        return \json_decode(
            \sprintf(
                <<<ES_QUERY
                {
                    "index": "%s",
                    "size": 5000,
                    "body": {
                        "query": {
                            "bool": {
                                "must": [
                                    { "term" : { "aggregate_id": "%s" } },
                                    { "range": { "occurred_on": { "gte": %f } } }
                                ]
                            }
                        },
                        "sort": { "occurred_on" : { "order" : "asc" } }
                    }
                }
                ES_QUERY,
                $index,
                $aggregateId,
                $snapshotTimestamp
            ),
            true
        );
    }

    private static function lastSnapshotTimestampQuery(string $index, string $aggregateId): array
    {
        return \json_decode(
            \sprintf(
                <<<ES_QUERY
                {
                    "index": "%s",
                    "body": {
                        "sort": { "occurred_on" : { "order" : "desc" } },
                        "size": 1,
                        "query": {
                            "bool": {
                                "must": [
                                    { "term" : { "aggregate_id": "%s" } },
                                    { "term" : { "event_name": "pldt-smart.deep.sso.snapshot_was_created" } }
                                ]
                            }
                        }
                    }
                }
                ES_QUERY,
                $index,
                $aggregateId
            ),
            true
        );
    }

    private static function dormantUserIdsQuery(string $index, float $dormantTimestamp): array
    {
        // TODO: matcher for ensuring the json matches a sub-pattern
        return \json_decode(
            \sprintf(
                <<<ES_QUERY
                {
                    "index": "%s",
                    "body": {
                        "query": {
                            "bool": {
                                "must": [
                                    {
                                        "term": {
                                            "event_name": "pldt-smart.deep.sso.user_has_logged_in"
                                        }
                                    },
                                    {
                                        "range": {
                                            "occurred_on": {
                                                "lte": %f
                                            }
                                        }
                                    }
                                ],
                                "must_not": {
                                    "term": {
                                        "event_name": "pldt-smart.deep.sso.user_was_deleted"
                                    }
                                }
                            }
                        }
                    }
                }
                ES_QUERY,
                $index,
                $dormantTimestamp
            ),
            true
        );
    }

    private function userIdToTermQuery(UserId $userId): array
    {
        return ['term' => ['aggregate_id' => (string)$userId]];
    }

    private function dormantTimestamp(): float
    {
        return (float)\microtime(true) - ($this->dormancyInDays * self::SECONDS_IN_A_DAY);
    }

    public function purgeEventsMarkedDeletedByUserIds(UserId ...$userIds): void
    {
        foreach ($userIds as $userId) {
            $this->deleteEventsExcludingUserWasDeleted($userId);
        }
    }

    private function containsUserWasDeletedCriteria(UserId $userId): array
    {
        return \json_decode(
            <<<CRITERIA
            {
                "index": "{$this->container->get('parameter.sso.event_store_index')}",
                "body": {
                    "query": {
                        "bool": {
                            "must": [
                                {
                                    "term": {
                                        "event_name": "pldt-smart.deep.sso.user_was_deleted"
                                    }
                                },
                                {
                                    "term": {
                                        "aggregate_id": "{$userId}"
                                    }
                                }
                            ]
                        }
                    }
                }
            }
            CRITERIA,
            true
        );
    }

    private function deleteEventsCriteria(UserId $userId): array
    {
        return \json_decode(
            <<<CRITERIA
            {
                "index": "{$this->container->get('parameter.sso.event_store_index')}",
                "body": {
                    "query": {
                        "bool": {
                            "must": {
                                "term": {"aggregate_id": "{$userId}"}
                            },
                            "must_not": {
                                "term": {"event_name": "pldt-smart.deep.sso.user_was_deleted"}
                            }
                        }
                    }
                }
            }
            CRITERIA,
            true
        );
    }

    private function containsUserWasDeletedEvent(UserId $userId): bool
    {
        $this->elasticSearchClient()->indices()->refresh();
        $result = $this->elasticSearchClient()->search($this->containsUserWasDeletedCriteria($userId));

        return ($result['hits']['total'] ?? 0) > 0;
    }

    private function deleteEventsExcludingUserWasDeleted(UserId $userId): void
    {
        if ($this->containsUserWasDeletedEvent($userId)) {
            $this->elasticSearchClient()->deleteByQuery($this->deleteEventsCriteria($userId));
        }
    }

    public function findByCriteria(Criteria $criteria): DomainEvents
    {
        $query = $this->buildESQueryByCriteria($criteria);

        $this->elasticSearchClient()->indices()->refresh();
        $eventsAsArray = $this->elasticSearchClient()->search($query);

        return new DomainEvents(
            ...$this->arrayOfEventsToArrayOfDomainEvent($eventsAsArray['hits']['hits'] ?? [])
        );
    }

    private function buildESQueryByCriteria(Criteria $criteria)
    {
        $index = $this->container->get('parameter.sso.event_store_index');
        $params = [];
        foreach ($criteria->toArray() as $param) {
            $params[] = $param->toArray();
        }
        $stringParams = \json_encode($params);

        return \json_decode(
            <<<ES_QUERY
            {
                "index": "$index",
                "body": {
                    "query": {
                        "bool": {
                            "must": $stringParams
                        }
                    }
                }
            }
            ES_QUERY,
            true
        );
    }
}

<?php

namespace App\Sso;

use App\Http\Controllers\LoginController;
use App\Http\Controllers\UserController;
use App\Providers\DynamicSingletonServiceProvider;
use App\Sso\Console\DeleteUser;
use App\Sso\Console\GenerateApplicationToken;
use App\Sso\Console\ReprocessUserVerification;
use App\Sso\Console\ShowEvents;
use App\Sso\SendRegistrationOtp as SendRegistrationOtpHandler;
use Elasticsearch\Client;
use Elasticsearch\ClientBuilder;
use Illuminate\Routing\Router;
use Lcobucci\Clock\SystemClock;
use Lcobucci\JWT\Configuration;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use Lcobucci\JWT\Signer\Hmac\Sha512;
use Lcobucci\JWT\Signer\Key\InMemory;
use Lcobucci\JWT\Validation\Constraint\SignedWith;
use Lcobucci\JWT\Validation\Constraint\ValidAt;
use SmartPldt\Deep\Sso\DeleteWasRequested;
use SmartPldt\Deep\Sso\DomainEventFactory;
use SmartPldt\Deep\Sso\EncryptionService;
use SmartPldt\Deep\Sso\Event\EventBus;
use SmartPldt\Deep\Sso\Event\Handler\SendOtpToServiceNumberToBeLinked;
use SmartPldt\Deep\Sso\Event\PurgeDeletedUser;
use SmartPldt\Deep\Sso\Event\SendVerificationCodeForUserDeletion;
use SmartPldt\Deep\Sso\Event\Handler\SendForgotPasswordOtp;
use SmartPldt\Deep\Sso\EventStore as BaseEventStore;
use SmartPldt\Deep\Sso\ForgotPasswordRequested;
use SmartPldt\Deep\Sso\MranWasConfirmed;
use SmartPldt\Deep\Sso\MranWasNominated;
use SmartPldt\Deep\Sso\NewPasswordWasSet;
use SmartPldt\Deep\Sso\Notifier;
use SmartPldt\Deep\Sso\PasswordChangeFailed;
use SmartPldt\Deep\Sso\PasswordWasReset;
use SmartPldt\Deep\Sso\SnapshotWasCreated;
use SmartPldt\Deep\Sso\UserChangedPassword;
use SmartPldt\Deep\Sso\UserDeclaredActive;
use SmartPldt\Deep\Sso\UserFailedToLogin;
use SmartPldt\Deep\Sso\UserHasLoggedIn;
use SmartPldt\Deep\Sso\UserRegistrationVerified;
use SmartPldt\Deep\Sso\UserRepository as BaseUserRepository;
use SmartPldt\Deep\Sso\UserService as BaseUserService;
use SmartPldt\Deep\Sso\UserWasDeleted;
use SmartPldt\Deep\Sso\UserWasRegistered;
use SmartPldt\Deep\Sso\VerificationCode\Service as VerificationCodeService;
use SmartPldt\Deep\Sso\VerificationCode\TimestampAwareService;
use SmartPldt\Deep\Sso\VerificationCode\Transport as VerificationCodeTransport;

class CommonServiceProvider extends DynamicSingletonServiceProvider
{
    private const EVENT_CONSUMER_EXECUTION_TIME_LIMIT = 60;

    private function registerParameters(): void
    {
        $this->registerServiceOrParameter('parameter.sso.elasticsearch_hosts', function () {
            return config('elasticsearch.hosts');
        });
        $this->registerServiceOrParameter('parameter.sso.jwt_secret', function () {
            return config('deep-sso.jwt.secret');
        });
        $this->registerServiceOrParameter('parameter.sso.jwt_expiry', function () {
            return config('deep-sso.jwt.expiry');
        });
        $this->registerServiceOrParameter('parameter.sso.device_jwt_expiry', function () {
            return config('deep-sso.jwt.device_expiry');
        });
        $this->registerServiceOrParameter('parameter.sso.otp_code_length', function () {
            return config('deep-sso.otp_code_length');
        });
        $this->registerServiceOrParameter('parameter.sso.console.minimum_execution_time', function () {
            return config('deep-sso.event_consumer.execution_time_limit', self::EVENT_CONSUMER_EXECUTION_TIME_LIMIT);
        });
        $this->registerServiceOrParameter('parameter.sso.dormancy_in_days', function () {
            return config('deep-sso.dormancy_in_days', 90);
        });
        $this->registerServiceOrParameter('parameter.sso.domain_prefix', function () {
            return config('deep-sso.domain_prefix', '');
        });
        $this->registerServiceOrParameter('parameter.sso.event_handlers', function ($app) {
            return [
                SendRegistrationOtpHandler::EVENT_NAME_TO_HANDLE => SendRegistrationOtpHandler::fromContainer($app),
                SendVerificationCodeForUserDeletion::EVENT_NAME_TO_HANDLE => new SendVerificationCodeForUserDeletion(
                    $app->get(BaseUserService::class),
                    $app->get(VerificationCodeService::class),
                    $app->get(VerificationCodeTransport::class)
                ),
                SendForgotPasswordOtp::EVENT_NAME_TO_HANDLE => new SendForgotPasswordOtp(
                    $app->get(BaseUserService::class),
                    $app->get(TimestampAwareService::class),
                    $app->get(VerificationCodeTransport::class)
                ),
                PurgeDeletedUser::EVENT_NAME_TO_HANDLE => new PurgeDeletedUser(
                    $app->get(BaseEventStore::class),
                    $app->get(BaseUserService::class),
                    $app->get(Notifier::class)
                ),
                SendOtpToServiceNumberToBeLinked::EVENT_NAME_TO_HANDLE => new SendOtpToServiceNumberToBeLinked(
                    $app->get(BaseUserService::class),
                    $app->get(VerificationCodeService::class),
                    $app->get(VerificationCodeTransport::class)
                )
            ];
        });
        $this->registerServiceOrParameter('service.sso.jwt_service', function ($app) {
            $configuration = Configuration::forSymmetricSigner(
                new Sha256(),
                InMemory::plainText($app->get('parameter.sso.jwt_secret'))
            );
            $configuration->setValidationConstraints(
                new SignedWith($configuration->signer(), $configuration->signingKey()),
                new ValidAt(new SystemClock(new \DateTimeZone(\date_default_timezone_get())))
            );

            return $configuration;
        });
        $this->registerServiceOrParameter('service.sso.app_jwt_service', function ($app) {
            $configuration = Configuration::forSymmetricSigner(
                new Sha512(),
                InMemory::plainText($app->get('parameter.sso.jwt_secret'))
            );
            $configuration->setValidationConstraints(
                new SignedWith($configuration->signer(), $configuration->signingKey()),
                new ValidAt(new SystemClock(new \DateTimeZone(\date_default_timezone_get())))
            );

            return $configuration;
        });
    }

    private function registerServices(): void
    {
        $this->registerServiceOrParameter('service.sso.domain_event_factory', [$this, 'createDomainEventFactory']);
        $this->registerServiceOrParameter(BaseUserService::class, [$this, 'createUserService']);
        $this->registerServiceOrParameter('service.sso.elasticsearch_client', [$this, 'createElasticsearchClient']);
        $this->registerServiceOrParameter(BaseEventStore::class, [$this, 'createEventStore']);
        $this->registerServiceOrParameter(BaseUserRepository::class, [$this, 'createUserRepository']);
        $this->registerServiceOrParameter(EncryptionService::class, [$this, 'createEncryptionService']);
    }

    public function register()
    {
        $this->registerParameters();
        $this->registerServices();
    }

    public function createDomainEventFactory(): DomainEventFactory
    {
        return (new DomainEventFactory($this->app->get(BaseUserService::class)))
            ->addSignature(UserWasRegistered::class)
            ->addSignature(UserRegistrationVerified::class)
            ->addSignature(UserFailedToLogin::class)
            ->addSignature(PasswordChangeFailed::class)
            ->addSignature(UserChangedPassword::class)
            ->addSignature(PasswordWasReset::class)
            ->addSignature(MranWasNominated::class)
            ->addSignature(MranWasConfirmed::class)
            ->addSignature(UserHasLoggedIn::class)
            ->addSignature(DeleteWasRequested::class)
            ->addSignature(UserWasDeleted::class)
            ->addSignature(ForgotPasswordRequested::class)
            ->addSignature(NewPasswordWasSet::class)
            ->addSignature(SnapshotWasCreated::class)
            ->addSignature(UserDeclaredActive::class)
        ;
    }

    public function createUserService(): BaseUserService
    {
        return new UserService($this->app);
    }

    public function createElasticsearchClient(): Client
    {
        return (new ClientBuilder())::create()->setHosts($this->app->get('parameter.sso.elasticsearch_hosts'))->build();
    }

    public function createEventStore(): BaseEventStore
    {
        return (new EventStore($this->app))->disableWaitForRefreshOption();
    }

    public function createUserRepository(): BaseUserRepository
    {
        return new UserRepository($this->app->get(EventStore::class), $this->app->get(EventBus::class));
    }

    public function createEncryptionService(): EncryptionService
    {
        return new LaravelEncryptionService();
    }

    private function router(): Router
    {
        return $this->app->get('router');
    }

    public function boot()
    {
        // TODO: create tests for this method
        $this->router()->group(['middleware' => ['api']], function () {
            $this->modifyApiUri();
            $this->modifyApiUriJwt();
        });

        if ($this->app->runningInConsole()) {
            $this->commands($this->consoleCommands());
        }
    }

    private function modifyApiUri()
    {
        foreach ($this->getArrayofUris() as $action => $uri) {
            $method = $uri['method'];
            $this->router()->prefix('api')->$method(
                $this->app->get('parameter.sso.domain_prefix') . '/' . $uri['link'],
                $uri['class'] . '@' . $action
            );
        }
    }

    private function modifyApiUriJwt()
    {
        $this->router()->group(['middleware' => ['jwt']], function () {
            foreach ($this->getArrayofUrisJwt() as $action => $uri) {
                $method = $uri['method'];
                $this->router()->prefix('api')->$method(
                    $this->app->get('parameter.sso.domain_prefix') . '/' . $uri['link'],
                    $uri['class'] . '@' . $action
                );
            }
        });
    }

    private function getArrayofUrisJwt(): array
    {
        return [
            'deleteUser' => $this->generateUriArray('delete', UserController::class, 'user'),
            'changePassword' => $this->generateUriArray('patch', UserController::class, 'user/change-password'),
            'refreshAccessToken' => $this->generateUriArray('post', UserController::class, 'user/refresh-token'),
        ];
    }

    private function getArrayofUris(): array
    {
        return [
            'register' => $this->generateUriArray('post', UserController::class, 'user'),
            'login' => $this->generateUriArray('post', LoginController::class, 'login'),
            'verifyUser' => $this->generateUriArray('patch', UserController::class, 'user'),
            'requestPasswordReset' => $this->generateUriArray(
                'patch',
                UserController::class,
                'user/request-password-reset'
            ),
            'verifyForgotPassword' => $this->generateUriArray('patch', UserController::class, 'user/set-new-password'),
            'resendRegistrationOtp' => $this->generateUriArray(
                'post',
                UserController::class,
                'user/resend-registration-otp'
            )
        ];
    }

    private function generateUriArray(string $method, string $class, string $uri): array
    {
        return ['method' => $method, 'class' => $class, 'link' => $uri];
    }

    public function consoleCommands(): array
    {
        return [
            DeleteUser::class,
            GenerateApplicationToken::class,
            ShowEvents::class,
            ReprocessUserVerification::class
        ];
    }
}

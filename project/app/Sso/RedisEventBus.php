<?php

declare(strict_types=1);

namespace App\Sso;

use Interop\Queue\{Consumer, Context};
use SmartPldt\Deep\Sso\DomainEvent;
use SmartPldt\Deep\Sso\Event\{EventBus, Exception, HandlesAnEventMessage};

class RedisEventBus implements EventBus
{
    public const RECEIVE_TIMEOUT = 5;

    /** @var Context */
    private $context;

    public function __construct(Context $context)
    {
        $this->context = $context;
    }

    public function send(DomainEvent $domainEvent)
    {
        $topic = $this->context->createTopic($domainEvent->name());
        $message = $this->context->createMessage((string)$domainEvent);
        $this->context->createProducer()->send($topic, $message);
    }

    private function consumeEventsThroughEventHandler(Consumer $consumer, HandlesAnEventMessage $handleEvent): void
    {
        // TODO: URGENT!!! handle thrown errors here
        while ($message = $consumer->receive(self::RECEIVE_TIMEOUT)) {
            $handleEvent($message->getBody());
            $consumer->acknowledge($message);
            usleep(10000);
        }
    }

    public function receive(string $eventName, HandlesAnEventMessage $handleEvent): void
    {
        try {
            $topic = $this->context->createTopic($eventName);
            $consumer = $this->context->createConsumer($topic);
            $this->consumeEventsThroughEventHandler($consumer, $handleEvent);
        } catch (\Throwable $exception) {
            throw new Exception($exception->getMessage(), $exception->getCode(), $exception);
        }
    }

    public function delegateEventTo(HandlesAnEventMessage $eventHandler): void
    {
        $this->receive($eventHandler->eventNameToHandle(), $eventHandler);
    }
}

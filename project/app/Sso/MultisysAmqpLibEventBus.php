<?php

namespace App\Sso;

use App\Sso\Event\Handler\SilentWrapper;
use Multisys\AmqpEventbusLibrary\Consumer;
use Multisys\AmqpEventbusLibrary\EventHandler;
use Multisys\AmqpEventbusLibrary\Sender;
use SmartPldt\Deep\Sso\DomainEvent;
use SmartPldt\Deep\Sso\Event\EventBus;
use SmartPldt\Deep\Sso\Event\HandlesAnEventMessage;

class MultisysAmqpLibEventBus implements EventBus
{
    private Consumer $consumer;

    private Sender $sender;

    /** @var EventHandler[] */
    private $handlerHashTable = [];

    private int $consumerTtlInMilliseconds;

    public function __construct(Consumer $consumer, Sender $sender, int $consumerTtlInMilliseconds)
    {
        $this->consumer = $consumer;
        $this->sender = $sender;
        $this->consumerTtlInMilliseconds = $consumerTtlInMilliseconds;
    }

    public function send(DomainEvent $domainEvent)
    {
        $this->sender->send($domainEvent::name(), (string)$domainEvent);
    }

    public function receive(string $eventName, HandlesAnEventMessage $ssoEventHandler): void
    {
        $this->delegateEventTo($ssoEventHandler);
    }

    public function delegateEventTo(HandlesAnEventMessage $ssoEventHandler): void
    {
        $this->consumer->delegateMessageToHandler(
            $this->getOrCreateWrapperFor($ssoEventHandler),
            $this->consumerTtlInMilliseconds
        );
    }

    private function getOrCreateWrapperFor(HandlesAnEventMessage $ssoEventHandler): EventHandler
    {
        $eventNameToHandle = $ssoEventHandler->eventNameToHandle();
        if (!isset($this->handlerHashTable[$eventNameToHandle])) {
            $this->handlerHashTable[$eventNameToHandle] = new SilentWrapper($ssoEventHandler);
        }

        return $this->handlerHashTable[$eventNameToHandle];
    }
}

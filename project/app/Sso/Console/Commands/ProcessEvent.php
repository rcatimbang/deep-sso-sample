<?php

declare(strict_types=1);

namespace App\Sso\Console\Commands;

use Illuminate\Console\Command;
use Psr\Container\ContainerInterface;
use SmartPldt\Deep\Sso\Event\EventBus;
use SmartPldt\Deep\Sso\Event\HandlesAnEventMessage;
use Symfony\Component\Console\Input\InputArgument;

class ProcessEvent extends Command
{
    public function __construct()
    {
        parent::__construct();
        $this
            ->setName('sso:process-event')
            ->setDescription('Process the specified event name')
            ->addArgument('event-name', InputArgument::REQUIRED, 'Event name to handle')
        ;
    }

    public function handle()
    {
        try {
            $eventHandler = $this->getEventHandler($this->argument('event-name'));

            if ($eventHandler) {
                $this->eventBus()->delegateEventTo($eventHandler);
            }
        } catch (\Throwable $exception) {
            throw $exception;
        }

        return Command::SUCCESS;
    }

    private function serviceContainer(): ContainerInterface
    {
        return $this->getLaravel();
    }

    private function getEventHandler(string $eventName): ?HandlesAnEventMessage
    {
        return $this->serviceContainer()->get('parameter.sso.event_handlers')[$eventName] ?? null;
    }

    private function eventBus(): EventBus
    {
        return $this->serviceContainer()->get(EventBus::class);
    }
}

<?php

namespace App\Sso\Console;

use Illuminate\Console\Command;
use SmartPldt\Deep\Sso\CanonicalUserId\CanonicalUserId;
use SmartPldt\Deep\Sso\EventStore;
use SmartPldt\Deep\Sso\UserId;
use SmartPldt\Deep\Sso\UserService;
use Symfony\Component\Console\Input\InputOption;

class DeleteUser extends Command
{
    public function __construct()
    {
        parent::__construct();

        $this
            ->setName('sso:delete-user')
            ->setDescription('Delete a user by given UserIds or CanonicalUserIds')
            ->addOption(
                'user-ids',
                'u',
                InputOption::VALUE_REQUIRED,
                'Comma-delimited UserIds to delete'
            )
            ->addOption(
                'mins',
                'm',
                InputOption::VALUE_REQUIRED,
                'Comma-delimited mobile identification numbers (min)s to delete'
            );
    }

    private function inputToArrayOfUserIds(): \Generator
    {
        $userIds = \explode(',', $this->option('user-ids'));
        while ($userId = \array_shift($userIds)) {
            yield self::stringToUserId($userId);
        }
    }

    private function inputToArrayOfMins(): \Generator
    {
        $canonicalUserIds = \explode(',', $this->option('mins'));
        while ($userId = \array_shift($canonicalUserIds)) {
            yield self::stringToCanonicalUserIds($userId, $this->userService());
        }
    }

    private static function stringToUserId(string $userIdAsString): UserId
    {
        return UserId::fromString($userIdAsString);
    }

    private static function stringToCanonicalUserIds(string $userIdAsString, UserService $userService): CanonicalUserId
    {
        return $userService->createCanonicalUserId($userIdAsString);
    }

    private function findAndDeleteUsersByUserIds(UserId ...$userIds)
    {
        while ($userId = \array_shift($userIds)) {
            $user = $this->userService()->findUserByUserId($userId)->delete();
            $this->userService()->saveUser($user);
        }
    }

    private function findAndDeleteUsersByCanonicalUserIds(CanonicalUserId ...$canonicalUserIds)
    {
        while ($canonicalUserId = \array_shift($canonicalUserIds)) {
            $user = $this->userService()->findUserByCanonicalUserId($canonicalUserId)->delete();
            $this->userService()->saveUser($user);
        }
    }

    public function handle(): int
    {
        try {
            $this->findAndDeleteUsersByUserIds(...$this->inputToArrayOfUserIds());
            $this->findAndDeleteUsersByCanonicalUserIds(...$this->inputToArrayOfMins());
            $this->findAndDeleteUsersByUserIds(...$this->eventStore()->dormantUserIds(10));
        } catch (\Throwable $exception) {
            return __LINE__;
        }

        return 0;
    }

    public function eventStore(): EventStore
    {
        return $this->getLaravel()->get(EventStore::class);
    }

    public function userService(): UserService
    {
        return $this->getLaravel()->get(UserService::class);
    }
}

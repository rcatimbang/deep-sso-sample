<?php

declare(strict_types=1);

namespace App\Sso\Console;

use Illuminate\Console\Command;
use SmartPldt\Deep\Sso\EventStore;
use SmartPldt\Deep\Sso\UserService;
use Symfony\Component\Console\Input\InputArgument;

class ShowEvents extends Command
{
    public function __construct()
    {
        parent::__construct();

        $this
            ->setName('sso:show-events')
            ->setDescription('Show events by given canonical user id')
            ->addArgument('cuid', InputArgument::REQUIRED, 'Canonical user id')
        ;
    }

    public function handle(): int
    {
        $min = $this->userService()->createCanonicalUserId($this->argument('cuid'));
        $domainEvents = $this->eventStore()->findByCanonicalUserId($min);

        if (!\count($domainEvents)) {
            $this->getOutput()->writeln('<fg=red>No events were found for the given canonical user id</>');
            return __LINE__;
        }

        foreach ($domainEvents as $event) {
            $this->getOutput()->writeln(
                \sprintf(
                    '<fg=yellow>%s: %s: </><fg=green>%s</>',
                    $event->aggregateId(),
                    $event->occurredOn()->format('Y-m-d H:i:s T'),
                    $event::name()
                )
            );
        }

        return 0;
    }

    private function userService(): UserService
    {
        return $this->getLaravel()->get(UserService::class);
    }

    private function eventStore(): EventStore
    {
        return $this->getLaravel()->get(EventStore::class);
    }
}

<?php

namespace App\Sso\Console;

use Elasticsearch\Client as ElasticSearchClient;
use Illuminate\Console\Command;
use Illuminate\Contracts\Cache\Repository;
use SmartPldt\Deep\Sso\EncryptionService;
use SmartPldt\Deep\Sso\Event\EventBus;
use SmartPldt\Deep\Sso\UserRegistrationReVerified;
use SmartPldt\Deep\Sso\UserService;

class ReprocessUserVerification extends Command
{
    public function __construct()
    {
        parent::__construct();

        $this
            ->setName('sso:reprocess-user-verification')
            ->setDescription('Forward UserRegistrationVerification event for re-processing by other services')
        ;
    }

    public function handle(): int
    {
        $results = $this->elasticSearchClient()->search($this->findUserRegistrationVerifiedQuery());

        while (count($results['hits']['hits'] ?? []) > 0) {
            $events = $this->encryptedHitsToUserRegistrationReverifiedEvent($results['hits']['hits']);
            $this->sendEventsToTheEventBus($events);

            $results = $this->elasticSearchClient()->scroll(self::scrollParameters($results['_scroll_id']));
        }

        return 0;
    }

    private function findUserRegistrationVerifiedQuery(): array
    {
        $lastTimeStamp = $this->lastTimestampOfReprocessedEvent();

        return \json_decode(
            <<<SEARCH_CRITERIA
            {
                "index": "{$this->getLaravel()->get('parameter.sso.event_store_index')}",
                "size": 1000,
                "scroll": "2m",
                "body": {
                    "query": {
                        "bool": {
                            "must": [
                                {
                                    "term": {
                                        "event_name": "pldt-smart.deep.sso.user_registration_verified"
                                    }
                                },
                                {
                                    "range": {
                                        "occurred_on": {
                                            "gte": {$lastTimeStamp}
                                        }
                                    }
                                }
                            ]
                        }
                    },
                    "sort": {
                        "occurred_on": {
                            "order": "asc"
                        }
                    }
                }
            }
            SEARCH_CRITERIA,
            true
        );
    }

    /**
     * @return UserRegistrationReVerified[]
     */
    private function encryptedHitsToUserRegistrationReVerifiedEvent(array $hits): \Generator
    {
        foreach ($hits as $hit) {
            $eventAsArray = $hit['_source'];
            $eventAsArray['body'] = \json_decode($this->encryptionService()->decrypt($eventAsArray['body']), true);

            if ($this->isOldEventPayload($eventAsArray)) {
                $event = UserRegistrationReVerified::fromArray($eventAsArray, $this->userService());
                $this->updateLastTimestampOfReprocessedEvent($event);

                yield $event;
            }
        }
    }

    private function encryptionService(): EncryptionService
    {
        return $this->getLaravel()->get(EncryptionService::class);
    }

    private function isOldEventPayload($eventAsArray): bool
    {
        return isset($eventAsArray['body']['payload']['canonical_user_id']);
    }

    private function updateLastTimestampOfReprocessedEvent(UserRegistrationReVerified $event): void
    {
        $this->cacheRepository()->put('reprocess-user-verification-last-timestamp', $event->occurredOnTimestamp());
    }

    private function lastTimestampOfReprocessedEvent(): float
    {
        return $this->cacheRepository()->get('reprocess-user-verification-last-timestamp', 0.0);
    }

    private function userService(): UserService
    {
        return $this->getLaravel()->get(UserService::class);
    }

    private function sendEventsToTheEventBus(\Iterator $events): void
    {
        $eventBus = $this->eventBus();
        foreach ($events as $event) {
            $eventBus->send($event);
        }
    }

    private function eventBus(): EventBus
    {
        return $this->getLaravel()->get(EventBus::class);
    }

    private function elasticSearchClient(): ElasticSearchClient
    {
        return $this->getLaravel()->get('service.sso.elasticsearch_client');
    }

    private function cacheRepository(): Repository
    {
        return $this->getLaravel()->get(Repository::class);
    }

    private static function scrollParameters(string $scrollId): array
    {
        return \json_decode(
            <<<JSON
            {
                "scroll_id": "{$scrollId}",
                "scroll": "1m"
            }
            JSON,
            true
        );
    }
}

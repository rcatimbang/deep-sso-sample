<?php

declare(strict_types=1);

namespace App\Sso\Console;

use Illuminate\Console\Command;
use Lcobucci\JWT\Configuration as JwtService;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class GenerateApplicationToken extends Command
{
    private const SECONDS_IN_A_DAY = 86400;

    public function __construct()
    {
        parent::__construct();

        $this->setName('sso:generate-app-token')
            ->setDescription('Generate an application token')
            ->addArgument('app-id', InputArgument::REQUIRED, 'Application ID')
            ->addOption('expiration', 'e', InputOption::VALUE_OPTIONAL, 'Expiration in days', 7)
            ->addUsage('-e [expiration-in-days] <your-app-id-or-name>')
        ;
    }

    public function handle()
    {
        $issuedAt = new \DateTimeImmutable();
        $jwtService = $this->jwtService();
        $accesstoken = $jwtService->builder()
            ->withClaim('app_id', $appId = (string)$this->argument('app-id'))
            ->issuedAt($issuedAt)
            ->canOnlyBeUsedAfter($issuedAt)
            ->expiresAt(\DateTimeImmutable::createFromFormat('U', (string)$this->expiresAt()))
        ;

        $newLine = PHP_EOL;
        $output = \sprintf(
            "<fg=red>Application Token For `%s`:</>{$newLine}<fg=green>%s</>",
            $appId,
            $accesstoken->getToken($jwtService->signer(), $jwtService->signingKey())->toString()
        );

        $this->getOutput()->writeln($output);

        return 0;
    }

    private function expiresAt(): int
    {
        return time() + (self::SECONDS_IN_A_DAY * (int)$this->option('expiration'));
    }

    private function jwtService(): JwtService
    {
        return $this->getLaravel()->get('service.sso.app_jwt_service');
    }
}

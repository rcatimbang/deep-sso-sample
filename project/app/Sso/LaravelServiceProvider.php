<?php

declare(strict_types=1);

namespace App\Sso;

use App\Providers\DynamicSingletonServiceProvider;
use App\Sso\Console\Commands\ProcessEvent;
use Illuminate\Contracts\Cache\Repository;
use Illuminate\Routing\Router;
use Multisys\AmqpEventbusLibrary\Consumer;
use Multisys\AmqpEventbusLibrary\Sender;
use SmartPldt\Deep\Sso\DeviceTokenWhitelist as BaseDeviceTokenWhitelist;
use SmartPldt\Deep\Sso\Event\EventBus;
use SmartPldt\Deep\Sso\Notifier;
use SmartPldt\Deep\Sso\UserTokenWhitelist as BaseUserTokenWhitelist;
use SmartPldt\Deep\Sso\VerificationCode\NullService;
use SmartPldt\Deep\Sso\VerificationCode\NullTransport;
use SmartPldt\Deep\Sso\VerificationCode\Service as VerificationCodeService;
use SmartPldt\Deep\Sso\VerificationCode\TimestampAwareService;
use SmartPldt\Deep\Sso\VerificationCode\Transport;

class LaravelServiceProvider extends DynamicSingletonServiceProvider
{
    private function registerParameters(): void
    {
        $this->registerServiceOrParameter('parameter.sso.supported_canonical_id_types', function () {
            return config('deep-sso.supported_canonical_id_types');
        });
        $this->registerServiceOrParameter('parameter.sso.hmac_salt', function () {
            return config('salt.sso_hmac_salt');
        });
        $this->registerServiceOrParameter('parameter.sso.event_store_index', function () {
            return config('elasticsearch.index.event_store');
        });
        $this->registerServiceOrParameter('parameter.sso.domain_prefix', function () {
            return config('deep-sso.domain_prefix', '');
        });
    }

    private function secondsToMilliseconds(int $seconds): int
    {
        return $seconds * 1000;
    }

    private function registerServices(): void
    {
        $this->registerServiceOrParameter(EventBus::class, function ($app) {
            return new MultisysAmqpLibEventBus(
                $app->get(Consumer::class),
                $app->get(Sender::class),
                $this->secondsToMilliseconds((int)config('deep-sso.event_consumer.execution_time_limit'))
            );
        });
        $this->registerServiceOrParameter(VerificationCodeService::class, function ($app) {
            return $app->get(TimestampAwareService::class);
        });
        $this->registerServiceOrParameter(Transport::class, [$this, 'createSmsTransport']);
        $this->registerServiceOrParameter(Notifier::class, [$this, 'createNotifier']);
        $this->registerServiceOrParameter(BaseUserTokenWhitelist::class, [$this, 'createUserTokenWhitelist']);
        $this->registerServiceOrParameter(BaseDeviceTokenWhitelist::class, [$this, 'createDeviceTokenWhitelist']);
        $this->registerServiceOrParameter(TimestampAwareService::class, [$this, 'createVerificationCodeService']);
        $this->commands(ProcessEvent::class);
    }

    public function createVerificationCodeService(): VerificationCodeService
    {
        return new NullService();
    }

    public function createSmsTransport(): Transport
    {
        return new NullTransport();
    }

    public function createNotifier(): Notifier
    {
        return new TxtBoxNotifier(new TxtBoxService());
    }

    public function createUserTokenWhitelist(): BaseUserTokenWhitelist
    {
        return new UserTokenWhitelist($this->app->get(Repository::class));
    }

    public function createDeviceTokenWhitelist(): BaseDeviceTokenWhitelist
    {
        return new DeviceTokenWhitelist($this->app->get(Repository::class));
    }

    public function register()
    {
        $this->registerServices();
        $this->registerParameters();
    }

    private function router(): Router
    {
        return $this->app->get('router');
    }

    public function boot()
    {
    }
}

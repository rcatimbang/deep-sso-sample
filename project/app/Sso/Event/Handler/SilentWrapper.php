<?php

declare(strict_types=1);

namespace App\Sso\Event\Handler;

use Multisys\AmqpEventbusLibrary\EventHandler;
use SmartPldt\Deep\Sso\Event\HandlesAnEventMessage;

class SilentWrapper implements EventHandler
{
    private HandlesAnEventMessage $ssoEventHandler;

    private string $eventNameToHandle;

    public function __construct(HandlesAnEventMessage $ssoEventHandler)
    {
        $this->ssoEventHandler = $ssoEventHandler;
        $this->eventNameToHandle = $ssoEventHandler->eventNameToHandle();
    }

    public function eventNameToHandle(): string
    {
        return $this->eventNameToHandle;
    }

    public function __invoke(string $encodedDomainEvent)
    {
        try {
            ($this->ssoEventHandler)($encodedDomainEvent);
        } catch (\Throwable $exception) {
        }
    }
}

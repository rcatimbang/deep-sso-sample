<?php

declare(strict_types=1);

namespace App\Sso;

use Illuminate\Contracts\Cache\Repository as Cache;
use SmartPldt\Deep\Sso\DeviceTokenWhitelist as BaseDeviceTokenWhitelist;

final class DeviceTokenWhitelist extends TokenWhitelist implements BaseDeviceTokenWhitelist
{
    private Cache $cache;

    public function __construct(Cache $cache)
    {
        $this->cache = $cache;
    }

    protected function cache(): Cache
    {
        return $this->cache;
    }

    protected function createKey(string $deviceId): string
    {
        return sprintf('device-token-whitelist-for-%s', $deviceId);
    }

    protected function calculateHash(string $token): string
    {
        return \hash('crc32', $token);
    }
}

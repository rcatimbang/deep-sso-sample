<?php

namespace App\Sso;

use Illuminate\Log\LogManager;
use Interop\Amqp\AmqpConsumer;
use Interop\Amqp\AmqpContext;
use Interop\Amqp\AmqpMessage;
use Interop\Amqp\AmqpProducer;
use Interop\Amqp\AmqpQueue;
use Interop\Amqp\AmqpTopic;
use Interop\Amqp\Impl\AmqpBind;
use SmartPldt\Deep\Sso\DomainEvent;
use SmartPldt\Deep\Sso\Event\EventBus;
use SmartPldt\Deep\Sso\Event\HandlesAnEventMessage;

class ManyConsumerAtOnceAmqpEventBus implements EventBus
{
    private AmqpContext $context;

    private LogManager $logger;

    private const MESSAGE_RECEIVE_TIMEOUT_IN_MILLISECONDS = 5000;

    private const MESSAGE_TIME_TO_LIVE_IN_MILLISECONDS = 120000;

    /** @var array AmqpConsumer[] */
    private array $consumers = [];

    public function __construct(AmqpContext $context, LogManager $logger)
    {
        $this->context = $context;
        $this->logger = $logger;
    }

    public function send(DomainEvent $domainEvent)
    {
        $topic = $this->createTopic($domainEvent::name());
        $this->producer()->send($topic, $this->context->createMessage((string)$domainEvent));
    }

    private function producer(): AmqpProducer
    {
        return $this->context->createProducer()->setTimeToLive(self::MESSAGE_TIME_TO_LIVE_IN_MILLISECONDS);
    }

    /**
     * @inheritDocs
     */
    public function receive(string $eventName, HandlesAnEventMessage $handleEvent): void
    {
        $this->delegateProcessingOfEventsToTheEventHandler($this->getOrCreateConsumer($eventName), $handleEvent);
    }

    public function delegateEventTo(HandlesAnEventMessage $eventHandler): void
    {
        $this->receive($eventHandler->eventNameToHandle(), $eventHandler);
    }

    private function getOrCreateConsumer(string $eventName): AmqpConsumer
    {
        if (!isset($this->consumers[$eventName])) {
            $this->context->bind(new AmqpBind(
                $topic = $this->createTopic($eventName),
                $this->createQueue($eventName)
            ));

            $this->consumers[$eventName] = $this->context->createConsumer($topic);
        }

        return $this->consumers[$eventName];
    }

    private function createTopic(string $routingKeyOrEventName): AmqpTopic
    {
        $topic = $this->context->createTopic($routingKeyOrEventName);
        $topic->setType(AmqpTopic::TYPE_FANOUT);
        $this->context->declareTopic($topic);

        return $topic;
    }

    private function createQueue(string $routingKeyOrEventName): AmqpQueue
    {
        $queue = $this->context->createQueue($routingKeyOrEventName);
        $this->context->declareQueue($queue);

        return $queue;
    }

    private function delegateProcessingOfEventsToTheEventHandler(
        AmqpConsumer $consumer,
        HandlesAnEventMessage $handleMessage
    ) {
        while ($message = $consumer->receive(self::MESSAGE_RECEIVE_TIMEOUT_IN_MILLISECONDS)) {
            $this->tryProcessingTheMessage($handleMessage, $message);
            $consumer->acknowledge($message);
        }
    }

    private function tryProcessingTheMessage(HandlesAnEventMessage $handleMessage, AmqpMessage $message): void
    {
        try {
            $handleMessage($message->getBody());
        } catch (\Throwable $exception) {
            $this->logger->error($exception);
        }
    }
}

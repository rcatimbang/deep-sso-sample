<?php

declare(strict_types=1);

namespace App\Sso;

use SmartPldt\Deep\Sso\NotificationRecipient;
use SmartPldt\Deep\Sso\Notifier;

class TxtBoxNotifier implements Notifier
{
    private TxtBoxService $service;

    public function __construct(TxtBoxService $txtBoxService)
    {
        $this->service = $txtBoxService;
    }

    public function notify(NotificationRecipient $recipient, string $message)
    {
        $this->service->sendSMS((string)$recipient, $message);
    }
}

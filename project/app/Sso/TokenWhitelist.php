<?php

declare(strict_types=1);

namespace App\Sso;

use Illuminate\Contracts\Cache\Repository as Cache;
use SmartPldt\Deep\Sso\TokenWhitelist as BaseTokenWhitelist;

abstract class TokenWhitelist implements BaseTokenWhitelist
{
    public function set(string $key, string $token): void
    {
        $this->cache()->put($this->createKey($key), $this->calculateHash($token));
    }

    public function isWhitelisted(string $key, string $token): bool
    {
        return $this->cache()->get($this->createKey($key)) === $this->calculateHash($token);
    }

    abstract protected function cache(): Cache;

    abstract protected function createKey(string $key): string;

    abstract protected function calculateHash(string $token): string;
}

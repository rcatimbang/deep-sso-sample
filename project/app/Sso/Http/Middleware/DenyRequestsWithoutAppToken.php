<?php

namespace App\Sso\Http\Middleware;

use Illuminate\Contracts\Container\Container;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Lcobucci\JWT\Configuration as JwtService;

class DenyRequestsWithoutAppToken
{
    private Container $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function handle(Request $request, \Closure $next)
    {
        if ($appJwt = $request->header('X-Application-Token', false)) {
            $jwtService = $this->jwtService();
            $token = $jwtService->parser()->parse($appJwt);

            if ($jwtService->validator()->validate($token, ...$jwtService->validationConstraints())) {
                return $next($request);
            }
        }

        return new Response(null, 401);
    }

    private function jwtService(): JwtService
    {
        return $this->container->get('service.sso.app_jwt_service');
    }
}

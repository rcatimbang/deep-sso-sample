<?php

declare(strict_types=1);

namespace App\Sso;

use App\Exceptions\TxtBoxServiceException;
use Illuminate\Http\Client\RequestException;
use Illuminate\Support\Facades\Http;

class TxtBoxService
{
    private $apiKey;

    private $url;

    public function __construct()
    {
        $this->apiKey = config('txtbox.api_key');
        $this->url = config('txtbox.base_url');
    }

    /**
     * @throws TxtBoxServiceException
     */
    public function sendSMS(string $number, string $message): void
    {
        try {
            Http::withHeaders(['X-TXTBOX-Auth' => $this->apiKey])->asForm()->post("$this->url/v1/sms/push", [
                'number' => $number,
                'message' => $message
            ])->throw();
        } catch (RequestException $e) {
            throw new TxtBoxServiceException($e->getMessage(), $e->getCode(), $e->getPrevious());
        }
    }
}

<?php

namespace App\Sso;

use App\Exceptions\InvalidOperatorException;
use SmartPldt\Deep\Sso\CriteriaParameter;

class TermCriteriaParam implements CriteriaParameter
{
    private string $key;

    private string $operator;

    private $value;

    /**
     * @throws InvalidOperatorException
     */
    public function __construct(string $key, string $operator, $value)
    {
        if (!\in_array($operator, self::VALID_OPERATORS)) {
            throw new InvalidOperatorException();
        }

        $this->key = $key;
        $this->operator = $operator;
        $this->value = $value;
    }

    /**
     * @throws InvalidOperatorException
     */
    public static function equality(string $key, $value): self
    {
        return new static($key, self::EQUALITY, $value);
    }

    public function toArray(): array
    {
        return [
            "term" => [
                $this->key => $this->value
            ]
        ];
    }

    public function __toString(): string
    {
        return \json_encode($this->toArray());
    }
}

<?php

declare(strict_types=1);

namespace App\Sso;

use Illuminate\Support\Facades\Crypt;
use SmartPldt\Deep\Sso\EncryptionService;

class LaravelEncryptionService implements EncryptionService
{
    public function encrypt(string $data): string
    {
        return Crypt::encrypt($data);
    }

    public function decrypt(string $encryptedData): string
    {
        return Crypt::decrypt($encryptedData);
    }
}

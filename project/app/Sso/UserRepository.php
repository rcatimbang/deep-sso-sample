<?php

namespace App\Sso;

use SmartPldt\Deep\Sso\{EventStore, UserRepository as BaseRepository};
use SmartPldt\Deep\Sso\Event\EventBus;

class UserRepository extends BaseRepository
{
    private EventStore $eventStore;

    private EventBus $eventBus;

    public function __construct(EventStore $eventStore, EventBus $eventBus)
    {
        $this->eventStore = $eventStore;
        $this->eventBus = $eventBus;
    }

    protected function eventStore(): EventStore
    {
        return $this->eventStore;
    }

    protected function eventBus(): EventBus
    {
        return $this->eventBus;
    }
}

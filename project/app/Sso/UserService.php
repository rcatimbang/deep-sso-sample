<?php

declare(strict_types=1);

namespace App\Sso;

use Illuminate\Contracts\Cache\Repository as Cache;
use Lcobucci\JWT\Configuration as JwtService;
use Psr\Container\ContainerInterface;
use Ramsey\Uuid\Uuid;
use SmartPldt\Deep\Sso\AccessTokenService;
use SmartPldt\Deep\Sso\AggregateId;
use SmartPldt\Deep\Sso\CanonicalUserId\CanonicalUserId;
use SmartPldt\Deep\Sso\CanonicalUserId\CanonicalUserIdFactory;
use SmartPldt\Deep\Sso\CanonicalUserId\Hash as CanonicalUserIdHash;
use SmartPldt\Deep\Sso\CanonicalUserId\HashFactory;
use SmartPldt\Deep\Sso\CanonicalUserId\InvalidCanonicalUserId;
use SmartPldt\Deep\Sso\CanonicalUserId\PlainSha256;
use SmartPldt\Deep\Sso\DeviceAccessToken;
use SmartPldt\Deep\Sso\DeviceTokenWhitelist;
use SmartPldt\Deep\Sso\EventStore;
use SmartPldt\Deep\Sso\Exception;
use SmartPldt\Deep\Sso\Hash\Argon2idPassword;
use SmartPldt\Deep\Sso\Hash\Hash;
use SmartPldt\Deep\Sso\Hash\PasswordHash;
use SmartPldt\Deep\Sso\Hash\Sha256Hmac;
use SmartPldt\Deep\Sso\InvalidUserId;
use SmartPldt\Deep\Sso\Password;
use SmartPldt\Deep\Sso\User;
use SmartPldt\Deep\Sso\UserAccessToken;
use SmartPldt\Deep\Sso\UserId;
use SmartPldt\Deep\Sso\UserNotFoundError;
use SmartPldt\Deep\Sso\UserRepository as BaseUserRepository;
use SmartPldt\Deep\Sso\UserService as BaseUserService;
use SmartPldt\Deep\Sso\UserTokenWhitelist;
use SmartPldt\Deep\Sso\VerificationCode\Service;

class UserService implements BaseUserService, HashFactory, AccessTokenService
{
    /** @var ContainerInterface */
    private $container;

    private string $salt;

    private CanonicalUserIdFactory $canonicalUserIdFactory;

    private UserTokenWhitelist $userTokenWhitelist;

    private DeviceTokenWhitelist $deviceTokenWhitelist;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->canonicalUserIdFactory = $this->canonicalUserIdFactory();
        $this->salt = $container->get('parameter.sso.hmac_salt');
        $this->userTokenWhitelist = $container->get(UserTokenWhitelist::class);
        $this->deviceTokenWhitelist = $container->get(DeviceTokenWhitelist::class);
    }

    private function canonicalUserIdFactory(): CanonicalUserIdFactory
    {
        if (!isset($this->canonicalUserIdFactory)) {
            $this->canonicalUserIdFactory = new CanonicalUserIdFactory(
                $this,
                ...$this->container->get('parameter.sso.supported_canonical_id_types')
            );
        }

        return $this->canonicalUserIdFactory;
    }

    public function generateUserId(): AggregateId
    {
        return UserId::fromString((string)Uuid::uuid4());
    }

    public function createCanonicalUserId(string $canonicalUserId): CanonicalUserId
    {
        return $this->canonicalUserIdFactory->create($canonicalUserId);
    }

    public function hash(CanonicalUserId $canonicalUserId): CanonicalUserIdHash
    {
        $canonicalUserIdWithSalt = "\u{1f315}" . (string)$canonicalUserId . "\u{1f311}";

        return PlainSha256::fromHash((string)$this->calculateHmacHash($canonicalUserIdWithSalt));
    }

    public function createPasswordHash(Password $password): PasswordHash
    {
        return Argon2idPassword::fromSha256Hmac($this->calculateSha256HmacHash((string)$password));
    }

    private function calculateSha256HmacHash(string $data): Sha256Hmac
    {
        return Sha256Hmac::create($data, $this->salt);
    }

    public function calculateHmacHash(string $data): Hash
    {
        return $this->calculateSha256HmacHash($data);
    }

    public function saveUser(User $user): void
    {
        $this->userRepository()->save($user);
    }

    private function userRepository(): BaseUserRepository
    {
        return $this->container->get(BaseUserRepository::class);
    }

    /**
     * @inheritDoc
     *
     * @throws InvalidCanonicalUserId
     */
    public function findUserByCanonicalUserId(string $canonicalUserIdString): User
    {
        $canonicalUserId = $this->canonicalUserIdFactory()->create($canonicalUserIdString);
        $eventStore = $this->eventStore();
        $domainEvents = $eventStore->findByCanonicalUserId($canonicalUserId);

        if (!$domainEvents->count()) {
            throw new UserNotFoundError("Cannot find user with canonical user id `{$canonicalUserIdString}`");
        }

        return User::reconstituteFrom($domainEvents);
    }

    /**
     * @throws UserNotFoundError
     * @throws InvalidUserId
     */
    public function findUserByUserId(string $userIdString): User
    {
        $domainEvents = $this->eventStore()->find(UserId::fromString($userIdString));

        if (!$domainEvents->count()) {
            throw new UserNotFoundError("Cannot find user with user id `{$userIdString}`");
        }

        return User::reconstituteFrom($domainEvents);
    }

    /**
     * @throws Exception
     * @throws InvalidUserId
     */
    public function verifyRegistration(string $userId, string $mran, string $verificationCode): void
    {
        $mran = $this->createCanonicalUserId($mran);
        if (!$this->verificationCodeService()->verificationCodeIsValid($mran, $verificationCode)) {
            throw new Exception('Invalid verification code.');
        }

        $user = User::reconstituteFrom($this->eventStore()->find(UserId::fromString($userId)))->verify($mran);

        $this->saveUser($user);
    }

    public function generateUserAccessToken(string $userIdAsString): UserAccessToken
    {
        $userId = UserId::fromString($userIdAsString);
        $jwtService = $this->jwtService();
        $jwtExpiryInSeconds = (int)$this->container->get('parameter.sso.jwt_expiry');

        $token = $jwtService->builder()
            ->identifiedBy((string)$userId)
            ->withClaim('user_id', (string)$userId)
            ->issuedAt($currentTimestamp = new \DateTimeImmutable())
            ->canOnlyBeUsedAfter($currentTimestamp)
            ->expiresAt($currentTimestamp->add(new \DateInterval(\sprintf('PT%dS', $jwtExpiryInSeconds))))
            ->getToken($jwtService->signer(), $jwtService->signingKey())
        ;

        $this->userTokenWhitelist->set((string)$userId, $token->toString());

        return new UserAccessToken($token->toString(), \microtime(true) + $jwtExpiryInSeconds);
    }

    public function generateDeviceAccessToken(string $userIdAsString): DeviceAccessToken
    {
        $userId = UserId::fromString($userIdAsString);
        $jwtService = $this->jwtService();
        $jwtExpiryInSeconds = (int)$this->container->get('parameter.sso.device_jwt_expiry');

        $token = $jwtService->builder()
            ->identifiedBy((string)$userId)
            ->withClaim('device_id', $deviceId = (string)Uuid::uuid4())
            ->withClaim('user_id', (string)$userId)
            ->issuedAt($currentTimestamp = new \DateTimeImmutable())
            ->canOnlyBeUsedAfter($currentTimestamp)
            ->expiresAt($currentTimestamp->add(new \DateInterval(\sprintf('PT%dS', $jwtExpiryInSeconds))))
            ->getToken($jwtService->signer(), $jwtService->signingKey())
        ;

        $this->deviceTokenWhitelist->set((string)$userId, $token->toString());

        return new DeviceAccessToken($token->toString(), \microtime(true) + $jwtExpiryInSeconds);
    }

    private function jwtService(): JwtService
    {
        return $this->container->get('service.sso.jwt_service');
    }

    private function eventStore(): EventStore
    {
        return $this->container->get(EventStore::class);
    }

    private function verificationCodeService(): Service
    {
        return $this->container->get(Service::class);
    }

    private function cache(): Cache
    {
        return $this->container->get(Cache::class);
    }
}

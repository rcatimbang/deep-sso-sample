<?php

namespace App\Sso;

use SmartPldt\Deep\Sso\CriteriaParameter;

class RangeCriteriaParam implements CriteriaParameter
{
    private $gt;

    private $gte;

    private $lt;

    private $lte;

    private string $key;

    public function __construct(
        string $key,
        $gt = null,
        $gte = null,
        $lt = null,
        $lte = null
    ) {
        $this->key = $key;
        $this->gt = $gt;
        $this->gte = $gte;
        $this->lt = $lt;
        $this->lte = $lte;
    }

    private function value(): array
    {
        return [
            'gte' => $this->gte,
            'gt' => $this->gt,
            'lt' => $this->lt,
            'lte' => $this->lte
        ];
    }

    public function toArray(): array
    {
        return [
            'range' => [
                $this->key => \array_filter($this->value())
            ]
        ];
    }

    public function __toString(): string
    {
        return \json_encode($this->toArray());
    }
}

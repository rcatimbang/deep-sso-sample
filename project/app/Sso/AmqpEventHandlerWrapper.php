<?php

namespace App\Sso;

use Multisys\AmqpEventbusLibrary\EventHandler;
use SmartPldt\Deep\Sso\Event\HandlesAnEventMessage;

/**
 * @deprecated
 */
class AmqpEventHandlerWrapper implements EventHandler
{
    private HandlesAnEventMessage $ssoEventHandler;

    private string $eventNameToHandle;

    public function __construct(HandlesAnEventMessage $ssoEventHandler)
    {
        $this->ssoEventHandler = $ssoEventHandler;
        $this->eventNameToHandle = $ssoEventHandler->eventNameToHandle();
    }

    public function eventNameToHandle(): string
    {
        return $this->eventNameToHandle;
    }

    public function __invoke(string $encodedDomainEvent)
    {
        ($this->ssoEventHandler)($encodedDomainEvent);
    }
}

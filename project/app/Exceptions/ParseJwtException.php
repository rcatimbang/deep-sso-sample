<?php

namespace App\Exceptions;

use Throwable;

class ParseJwtException extends \Exception
{
    public function __construct(
        $message = 'Error parsing JWT token, please try again.',
        $code = 400,
        Throwable $previous = null
    ) {
        parent::__construct($message, $code, $previous);
    }
}

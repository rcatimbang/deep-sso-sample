<?php

namespace App\Console\Commands;

use Elasticsearch\Client as ElasticSearchClient;
use Illuminate\Console\Command;
use SmartPldt\Deep\Sso\EncryptionService;
use SmartPldt\Deep\Sso\UserRegistrationReVerified;
use SmartPldt\Deep\Sso\UserService;

class DumpToFileUserRegistrationVerifiedEvents extends Command
{
    public function __construct()
    {
        parent::__construct();

        $this
            ->setName('sso:write-to-file-user-verification-events')
            ->setDescription('Dump to file UserRegistrationVerification event for re-processing by other services')
        ;
    }

    public function handle(): int
    {
        $results = $this->elasticSearchClient()->search($this->findUserRegistrationVerifiedQuery());

        $pathToFile = storage_path('verified_users.data');
        $file = fopen($pathToFile, 'w');

        while (count($results['hits']['hits'] ?? []) > 0) {
            $events = $this->encryptedHitsToUserRegistrationReverifiedEvent($results['hits']['hits']);

            foreach ($events as $event) {
                fwrite($file, (string)$event);
                fwrite($file, PHP_EOL);
            }

            $results = $this->elasticSearchClient()->scroll(self::scrollParameters($results['_scroll_id']));
        }

        fclose($file);

        return 0;
    }

    private function findUserRegistrationVerifiedQuery(): array
    {
        return \json_decode(
            <<<SEARCH_CRITERIA
            {
                "index": "{$this->getLaravel()->get('parameter.sso.event_store_index')}",
                "size": 1000,
                "scroll": "2m",
                "body": {
                    "query": {
                        "bool": {
                            "must": [
                                {
                                    "term": {
                                        "event_name": "pldt-smart.deep.sso.user_registration_verified"
                                    }
                                }
                            ]
                        }
                    },
                    "sort": {
                        "occurred_on": {
                            "order": "asc"
                        }
                    }
                }
            }
            SEARCH_CRITERIA,
            true
        );
    }

    /**
     * @return UserRegistrationReVerified[]
     */
    private function encryptedHitsToUserRegistrationReVerifiedEvent(array $hits): \Generator
    {
        foreach ($hits as $hit) {
            $eventAsArray = $hit['_source'];
            $eventAsArray['body'] = \json_decode($this->encryptionService()->decrypt($eventAsArray['body']), true);

            yield UserRegistrationReVerified::fromArray($eventAsArray, $this->userService());
        }
    }

    private function encryptionService(): EncryptionService
    {
        return $this->getLaravel()->get(EncryptionService::class);
    }

    private function userService(): UserService
    {
        return $this->getLaravel()->get(UserService::class);
    }

    private function elasticSearchClient(): ElasticSearchClient
    {
        return $this->getLaravel()->get('service.sso.elasticsearch_client');
    }

    private static function scrollParameters(string $scrollId): array
    {
        return \json_decode(
            <<<JSON
            {
                "scroll_id": "{$scrollId}",
                "scroll": "1m"
            }
            JSON,
            true
        );
    }
}

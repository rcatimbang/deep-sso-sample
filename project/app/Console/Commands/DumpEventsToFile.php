<?php

namespace App\Console\Commands;

use Elasticsearch\Client;
use Illuminate\Console\Command;
use ONGR\ElasticsearchDSL\Query\Compound\BoolQuery;
use ONGR\ElasticsearchDSL\Query\TermLevel\TermQuery;
use ONGR\ElasticsearchDSL\Search;
use ONGR\ElasticsearchDSL\Sort\FieldSort;
use Psr\Container\ContainerInterface;
use SmartPldt\Deep\Sso\EncryptionService;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class DumpEventsToFile extends Command
{
    public function __construct()
    {
        parent::__construct();
        $this
            ->setName('sso:dump-events-to-file')
            ->setDescription('Dump events payload to a file')
            ->addArgument('event-names', InputArgument::REQUIRED | InputArgument::IS_ARRAY, 'Event names to handle')
            ->addOption('output-file', 'o', InputOption::VALUE_REQUIRED, 'Output file')
        ;
    }

    public function handle(): int
    {
        $this->throwErrorIfNoOutputFileWasGiven($outputFile = $this->option('output-file'));
        $fileResource = \fopen($outputFile, 'w+');
        $this->writeEventsPayloadToFile(
            $fileResource,
            ...($this->elasticSearchClient()->search($this->buildElasticsearchParams())['hits']['hits'] ?? [])
        );
        \fclose($fileResource);

        return self::SUCCESS;
    }

    /**
     * @throws \Exception
     */
    private function throwErrorIfNoOutputFileWasGiven(?string $outputFile): void
    {
        if (!$outputFile) {
            throw new \Exception('No output file specified');
        }
    }

    /**
     * @param resource $resource
     */
    private function writeEventsPayloadToFile($resource, array ...$events): void
    {
        while ($event = \array_shift($events)) {
            $body = \json_decode($this->encryptionService()->decrypt($event['_source']['body']), true);
            $event['_source']['body'] = $body;
            fwrite($resource, \json_encode($event['_source']) . "\n");
            $this->writeEventsPayloadToFile($resource, ...$events);
        }
    }

    private function container(): ContainerInterface
    {
        return $this->getLaravel();
    }

    private function encryptionService(): EncryptionService
    {
        return $this->container()->get(EncryptionService::class);
    }

    private function elasticSearchClient(): Client
    {
        return $this->container()->get('service.sso.elasticsearch_client');
    }

    private function buildElasticsearchParams(): array
    {
        $this->addBooleanQueriesFromEventNames($search = new Search(), ...$this->argument('event-names'));
        $search->addSort(new FieldSort('occurred_on', FieldSort::ASC));

        return ['index' => $this->container()->get('parameter.sso.event_store_index'), 'body' => $search->toArray()];
    }

    private function addBooleanQueriesFromEventNames(Search $search, string ...$eventNames)
    {
        while ($eventName = \array_shift($eventNames)) {
            $search->addQuery(new TermQuery('event_name', $eventName), BoolQuery::SHOULD);
            $this->addBooleanQueriesFromEventNames($search, ...$eventNames);
        }
    }
}

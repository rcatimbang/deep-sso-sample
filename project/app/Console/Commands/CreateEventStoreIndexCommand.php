<?php

namespace App\Console\Commands;

use Elasticsearch\Client;
use Illuminate\Console\Command;

class CreateEventStoreIndexCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sso:create-event-store-index';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create index of event store';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $this->elasticsearchClient()->indices()->create([
            'index' => $this->eventStoreIndexName(),
            'body' => \json_decode(self::indexSettingAsJson())
        ]);

        return 0;
    }

    private function elasticsearchClient(): Client
    {
        return $this->getLaravel()->get('service.sso.elasticsearch_client');
    }

    private function indexSettingAsJson()
    {
        return <<<JSON
        {
            "settings": {
                "index": {
                    "sort.field": "occurred_on",
                    "sort.order": "asc"
                }
            },
            "mappings": {
                "doc": {
                    "_source": {
                        "enabled": true
                    },
                    "properties": {
                        "canonical_user_id_hash": {
                            "type": "keyword"
                        },
                        "aggregate_id": {
                            "type": "keyword"
                        },
                        "event_name": {
                            "type": "keyword"
                        },
                        "occurred_on": {
                            "type": "double"
                        }
                    }
                }
            }
        }
        JSON;
    }

    private function eventStoreIndexName(): string
    {
        return $this->getLaravel()->get('parameter.sso.event_store_index');
    }
}

<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;

class CreateEventStoreIndex extends Command
{
    private string $url = 'localhost:9200/deep-sso-event-store';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sso:create-event-store-index';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create index of event store';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $headers = [
            'Content-Type' => 'application/json',
        ];

        $body = '{
                   "settings":{
                      "index":{
                         "sort.field":"occurred_on",
                         "sort.order":"asc"
                      }
                   },
                   "mappings":{
                      "doc":{
                         "_source":{
                            "enabled":true
                         },
                         "properties":{
                            "canonical_user_id_hash":{
                               "type":"keyword"
                            },
                            "aggregate_id":{
                               "type":"keyword"
                            },
                            "event_name":{
                               "type":"keyword"
                            },
                            "occurred_on":{
                               "type":"double"
                            }
                         }
                      }
                   }
                }';

        Http::withHeaders($headers)->put($this->url, \json_decode($body, true));

        return 0;
    }
}

<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Contracts\Cache\Repository as Cache;
use Psr\Container\ContainerInterface;
use SmartPldt\Deep\Sso\Event\EventBus;
use SmartPldt\Deep\Sso\Event\Exception;
use SmartPldt\Deep\Sso\Event\HandlesAnEventMessage;

/**
 * @deprecated
 */
class ProcessEvents extends Command
{
    public const MINIMUM_INSTANCE_TTL_IN_SECONDS = 60;

    public const PROCESSING_INTERVAL_IN_MICROSECONDS = 1000;

    protected $signature = 'sso:process-events';

    protected $description = 'Delegate processing of events received from EventBus to its event handler';

    private string $instanceId;

    public function __construct()
    {
        parent::__construct();
    }

    public function handle(): int
    {
        try {
            if ($this->isStillRunning()) {
                return __LINE__;
            }

            $timestampToEndExecution = \time() + $this->executionTimeLimit();
            $eventHandlers = $this->getEventHandlers();

            do {
                $this->updateLastActiveTimestampOfInstance();
                $this->delegateProcessingOfEventsToEventHandlers($eventHandlers);
                usleep(self::PROCESSING_INTERVAL_IN_MICROSECONDS);
            } while (\time() < $timestampToEndExecution);
        } catch (\Throwable $exception) {
            return __LINE__;
        }

        $this->cache()->delete($this->instanceId());

        return 0;
    }

    private function isStillRunning(): bool
    {
        return $this->cache()->get($this->instanceId(), 0) >= (\time() - self::MINIMUM_INSTANCE_TTL_IN_SECONDS);
    }

    private function updateLastActiveTimestampOfInstance(): void
    {
        $this->cache()->put($this->instanceId(), \time(), self::MINIMUM_INSTANCE_TTL_IN_SECONDS);
    }

    private function cache(): Cache
    {
        return $this->serviceContainer()->get(Cache::class);
    }

    private function instanceId(): string
    {
        if (!isset($this->instanceId)) {
            $this->instanceId = \sprintf(
                'deep_sso_event_consumer_last_timestamp_%s',
                \hash('crc32', \get_class($this))
            );
        }

        return $this->instanceId;
    }

    private function serviceContainer(): ContainerInterface
    {
        return $this->getLaravel();
    }

    private function eventBus(): EventBus
    {
        return $this->serviceContainer()->get(EventBus::class);
    }

    private function executionTimeLimit(): int
    {
        return (int)$this->serviceContainer()->get('parameter.sso.console.minimum_execution_time');
    }

    /**
     * @return HandlesAnEventMessage[]
     */
    private function getEventHandlers(): array
    {
        return $this->serviceContainer()->get('parameter.sso.event_handlers');
    }

    /**
     * @param HandlesAnEventMessage[] $eventHandlers
     *
     * @throws Exception
     */
    private function delegateProcessingOfEventsToEventHandlers(array $eventHandlers): void
    {
        foreach ($eventHandlers as $eventHandler) {
            $this->eventBus()->receive($eventHandler->eventNameToHandle(), $eventHandler);
        }
    }
}

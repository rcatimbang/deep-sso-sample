<?php

declare(strict_types=1);

namespace SmartPldt\Deep\Sso;

use SmartPldt\Deep\Sso\CanonicalUserId\CanonicalUserId;
use SmartPldt\Deep\Sso\Hash\PasswordHash;
use SmartPldt\Deep\Sso\Hash\PasswordHistory;
use SmartPldt\Deep\Sso\Hash\PasswordHistoryHash;

// TODO: do not apply events unless 1) verified, OR 2) already marked deleted
final class User extends AggregateRoot
{
    private const MAX_LOGIN_ATTEMPTS_WITHIN_TIMESPAN = 3;

    private const LOGIN_FAILURE_NONE = 0;

    private const EVENT_COUNT_CEILING_UNTIL_SNAPSHOT = 5;

    private UserId $userId;

    private PasswordHash $password;

    private CanonicalUserId $canonicalUserId;

    private CanonicalUserId $nominatedMran;

    private CanonicalUserId $mran;

    private FailedLoginTimestamps $failedLoginTimestamps;

    private float $registeredOn;

    private int $lastDeclaredActive;

    private int $loginFailureCount = 0;

    private int $loginCount = 0;

    /** @var DomainEvent[] */
    private $recordedEvents = [];

    private ?float $lastLoginTimestamp = null;

    private ?float $lastLoginFailureTimestamp = null;

    private bool $isVerified = false;

    private int $lastLoginFailureCode = self::LOGIN_FAILURE_NONE;

    private ?float $lastPasswordChangeFailedTimestamp;

    private bool $isLastPasswordChanged = false;

    private bool $wasDeleted = false;

    private PasswordHistory $passwordHistory;

    /**
     * @throws Exception
     */
    private function __construct()
    {
        $this->failedLoginTimestamps = new FailedLoginTimestamps();
        $this->passwordHistory = PasswordHistory::create();
    }

    private function __clone()
    {
        $this->failedLoginTimestamps = clone $this->failedLoginTimestamps;
        $this->passwordHistory = clone $this->passwordHistory;
    }

    /**
     * @throws Exception
     */
    public static function reconstituteFrom(DomainEvents $events): self
    {
        $user = new static();

        foreach ($events as $event) {
            $user->applyThat($event);
        }

        if ($events->count() > self::EVENT_COUNT_CEILING_UNTIL_SNAPSHOT) {
            return $user->createSnapshot();
        }

        return $user;
    }

    /**
     * @throws Exception
     */
    public static function register(string $canonicalUserId, string $password, UserService $userService): self
    {
        $user = (new static())
            ->setUserId($userService->generateUserId())
            ->setPassword($userService->createPasswordHash(new Password($password)))
            ->setCanonicalUserId($userService->createCanonicalUserId($canonicalUserId))
        ;
        $passwordHistoryHash = (string)new PasswordHistoryHash($password);
        $user->passwordHistory = $user->passwordHistory->add($passwordHistoryHash);

        return $user->recordThat($event = UserWasRegistered::create($user));
    }

    private function setUserId(UserId $userId): self
    {
        $this->userId = $userId;

        return $this;
    }

    private function setPassword(PasswordHash $password): self
    {
        $this->password = $password;

        return $this;
    }

    private function setCanonicalUserId(CanonicalUserId $canonicalUserId): self
    {
        $this->canonicalUserId = $canonicalUserId;

        return $this;
    }

    private function incrementLoginCount()
    {
        return $this->loginCount++;
    }

    private function mergePasswordHistory(PasswordHistory $passwordHistory): self
    {
        $this->passwordHistory = $this->passwordHistory()->merge($passwordHistory);

        return $this;
    }

    private function setNominatedMran(CanonicalUserId $canonicalUserId): self
    {
        $this->nominatedMran = $canonicalUserId;

        return $this;
    }

    private function confirmNominatedMran(CanonicalUserId $canonicalUserId): self
    {
        $this->mran = $canonicalUserId;

        return $this;
    }

    // TODO: discussion - make this public for easier testing?
    private function applyThat(DomainEvent $event): self
    {
        $fullEventName = \get_class($event);
        $positionOfLastBackslash = \strrpos($fullEventName, '\\');

        return $this->{'applyThat' . \substr($fullEventName, ++$positionOfLastBackslash)}($event);
    }

    private function applyThatUserWasRegistered(UserWasRegistered $event): self
    {
        $this->setUserId($event->userId())
            ->setPassword($event->password())
            ->setCanonicalUserId($event->canonicalUserId())
            ->mergePasswordHistory($event->passwordHistory())
        ;
        $this->registeredOn = $event->occurredOnTimestamp();
        $this->lastDeclaredActive = $event->occurredOn()->getTimestamp();

        return $this;
    }

    private function applyThatMranWasNominated(MranWasNominated $event): self
    {
        $this->nominatedMran = $event->mran();
        $this->lastDeclaredActive = $event->occurredOn()->getTimestamp();

        return $this;
    }

    private function applyThatUserRegistrationVerified(UserRegistrationVerified $event): self
    {
        $this->isVerified = true;
        $this->mran = $event->mran();
        $this->lastDeclaredActive = $event->occurredOn()->getTimestamp();

        return $this;
    }

    private function applyThatUserHasLoggedIn(UserHasLoggedIn $event): self
    {
        $this->lastLoginTimestamp = $event->occurredOn()->getTimestamp();
        $this->lastLoginFailureCode = self::LOGIN_FAILURE_NONE;
        $this->loginCount = $event->loginCount();
        $this->lastDeclaredActive = $event->occurredOn()->getTimestamp();

        return $this;
    }

    private function applyThatUserFailedToLogin(UserFailedToLogin $event): self
    {
        $this->lastLoginFailureTimestamp = $event->occurredOnTimestamp();
        $this->failedLoginTimestamps = $this->failedLoginTimestamps->record($this->lastLoginFailureTimestamp);

        return $this;
    }

    private function applyThatNewPasswordWasSet(NewPasswordWasSet $event): self
    {
        return $this
            ->setPassword($event->password())
            ->mergePasswordHistory($event->passwordHistory())
            ->setLastDeclaredActive($event->occurredOn()->getTimestamp())
        ;
    }

    private function applyThatUserChangedPassword(UserChangedPassword $event): self
    {
        return $this
            ->setUserId($event->userId())
            ->setPassword($event->passwordHash())
            ->mergePasswordHistory($event->passwordHistory())
            ->setLastDeclaredActive($event->occurredOn()->getTimestamp())
        ;
    }

    private function applyThatPasswordChangeFailed(PasswordChangeFailed $event): self
    {
        $this->lastPasswordChangeFailedTimestamp = $event->occurredOn()->getTimestamp();

        return $this;
    }

    private function applyThatForgotPasswordRequested(ForgotPasswordRequested $event): self
    {
        return $this;
    }

    private function applyThatDeleteWasRequested(DeleteWasRequested $event): self
    {
        return $this;
    }

    private function applyThatSnapshotWasCreated(SnapshotWasCreated $event): self
    {
        $this->userId = $event->userId();
        $this->password = $event->password();
        $this->isVerified = $event->isVerified();
        $this->registeredOn = $event->registeredOnTimestamp();
        $this->lastLoginTimestamp = $event->lastLoginTimestamp();
        $this->lastLoginFailureCode = $event->lastLoginFailureCode();
        $this->lastLoginFailureTimestamp = $event->lastLoginFailureTimestamp();
        $this->loginFailureCount = $event->loginFailureCount();
        $this->failedLoginTimestamps = $event->failedLoginTimestamps();
        $this->canonicalUserId = $event->canonicalUserId();
        $this->passwordHistory = $event->passwordHistory();
        $this->wasDeleted = $event->wasDeleted();
        $this->nominatedMran = $event->canonicalUserId();
        $this->lastPasswordChangeFailedTimestamp = $event->lastPasswordChangeFailedTimestamp();
        $this->loginCount = $event->loginCount();
        $this->lastDeclaredActive = $event->lastDeclaredActive();

        return $this;
    }

    private function applyThatUserWasDeleted(UserWasDeleted $event): self
    {
        $this->wasDeleted = true;

        return $this;
    }

    private function applyThatUserDeclaredActive(UserDeclaredActive $event): self
    {
        $this->lastDeclaredActive = (int)$event->occurredOnTimestamp();

        return $this;
    }

    private function recordThat(DomainEvent $event): self
    {
        $this->recordedEvents[] = $event;

        return $this;
    }

    public function recordedEvents(): DomainEvents
    {
        if (!isset($this->nominatedMran)) {
            $this->setNominatedMran($this->canonicalUserId);
            $this->recordThat(MranWasNominated::fromUser($this));
        }

        return new DomainEvents(...$this->recordedEvents);
    }

    public function aggregateId(): AggregateId
    {
        return $this->userId();
    }

    public function userId(): UserId
    {
        return $this->userId;
    }

    public function password(): PasswordHash
    {
        return $this->password;
    }

    public function lastLoginFailureCode(): int
    {
        return $this->lastLoginFailureCode;
    }

    public function isLastPasswordChanged(): bool
    {
        return $this->isLastPasswordChanged;
    }

    private function calculateLoginFailureCode(string $password, UserService $userService): int
    {
        // TODO: Move this concern to another class
        $reasonsForLoginFailure = [
            [$this, 'isNotVerified'],
            [$this, 'wasDeleted'],
            [$this, 'maximumFailedLoginAttemptsReached'],
            function () use ($userService, $password) {
                return !$this->passwordIsCorrect($userService, $password);
            }
        ];

        foreach ($reasonsForLoginFailure as $position => $callable) {
            if ($callable()) {
                $this->lastLoginFailureCode = 1 << $position;

                return $this->lastLoginFailureCode();
            }
        }

        return $this->lastLoginFailureCode = 0;
    }

    public function login(string $password, UserService $service): self
    {
        $anotherInstance = clone $this;
        $loginFailureCode = $anotherInstance->calculateLoginFailureCode($password, $service);
        $anotherInstance->lastLoginFailureCode = $loginFailureCode;

        if ($loginFailureCode) {
            $anotherInstance->failedLoginTimestamps->record(\time());
            $userFailedToLogin = UserFailedToLogin::create($anotherInstance);

            return $anotherInstance->recordThat($userFailedToLogin);
        }

        $anotherInstance->failedLoginTimestamps = new FailedLoginTimestamps();
        $anotherInstance->incrementLoginCount();

        return $anotherInstance->recordThat(UserHasLoggedIn::create($anotherInstance));
    }

    public function declareActive(): self
    {
        $anotherInstance = clone $this;
        $anotherInstance->setLastDeclaredActive(\time());

        return $anotherInstance->recordThat(UserDeclaredActive::create($anotherInstance));
    }

    /**
     * @throws InvalidPassword
     */
    public function changePassword(string $oldPassword, string $newPassword, UserService $service): self
    {
        $anotherInstance = clone $this;

        $anotherInstance = $anotherInstance->login($oldPassword, $service);
        if ($anotherInstance->lastLoginFailureCode()) {
            return $anotherInstance->recordThat(PasswordChangeFailed::create($anotherInstance));
        }

        $passwordHistoryHash = (string)new PasswordHistoryHash($newPassword);
        if ($anotherInstance->passwordHistory()->contains($passwordHistoryHash)) {
            return $anotherInstance->recordThat(PasswordChangeFailed::create($anotherInstance));
        }

        $anotherInstance
            ->setPassword($service->createPasswordHash(new Password($newPassword)))
            ->mergePasswordHistory($anotherInstance->passwordHistory()->add($passwordHistoryHash))
        ;
        $anotherInstance->isLastPasswordChanged = true; // TODO: for discussion

        return $anotherInstance->recordThat(UserChangedPassword::create($anotherInstance));
    }

    /**
     * @throws InvalidPassword
     */
    public function setNewPassword(string $newPassword, UserService $userService): self
    {
        $anotherInstance = clone $this;
        $password = new Password($newPassword);

        $passwordHistoryHash = (string)new PasswordHistoryHash($newPassword);
        if ($anotherInstance->passwordHistory()->contains($passwordHistoryHash)) {
            return $anotherInstance->recordThat(PasswordChangeFailed::create($anotherInstance));
        }

        return $anotherInstance
            ->setPassword($userService->createPasswordHash($password))
            ->mergePasswordHistory($this->passwordHistory()->add((string)new PasswordHistoryHash($newPassword)))
            ->recordThat(NewPasswordWasSet::fromUser($anotherInstance))
        ;
    }

    public function canonicalUserId(): CanonicalUserId
    {
        return $this->canonicalUserId;
    }

    public function nominatedMran(): CanonicalUserId
    {
        return $this->nominatedMran;
    }

    public function clearEvents(): void
    {
        $this->recordedEvents = [];
    }

    public function loginFailureCount(): int
    {
        return $this->failedLoginTimestamps->count();
    }

    public function registeredOn(): \DateTimeInterface
    {
        return \DateTimeImmutable::createFromFormat('U', (string)(int)$this->registeredOn);
    }

    public function registeredOnTimestamp(): float
    {
        return $this->registeredOn;
    }

    public function lastLoginTimestamp(): ?float
    {
        return $this->lastLoginTimestamp;
    }

    public function lastLoginFailureTimestamp(): ?float
    {
        return $this->lastLoginFailureTimestamp;
    }

    public function lastPasswordChangeFailedTimestamp(): ?float
    {
        return $this->lastPasswordChangeFailedTimestamp ?? null;
    }

    public function lastLoginDate(): \DateTimeInterface
    {
        return \DateTimeImmutable::createFromFormat('U', (string)(int)$this->lastLoginTimestamp());
    }

    public function lastLoginFailureDate(): \DateTimeInterface
    {
        return \DateTimeImmutable::createFromFormat('U', (string)(int)$this->lastLoginFailureTimestamp);
    }

    private function passwordIsCorrect(UserService $service, string $password): bool
    {
        return \password_verify((string)$service->calculateHmacHash($password), (string)$this->password());
    }

    public function verify(CanonicalUserId $mran): self
    {
        if ((string)$this->nominatedMran() !== (string)$mran) {
            return $this;
        }

        $anotherInstance = clone $this;
        $anotherInstance->isVerified = true;
        $anotherInstance->mran = $anotherInstance->nominatedMran();

        return $anotherInstance->recordThat(UserRegistrationVerified::create($anotherInstance));
    }

    public function isVerified(): bool
    {
        return $this->isVerified;
    }

    private function isNotVerified(): bool
    {
        return !$this->isVerified;
    }

    private function maximumFailedLoginAttemptsReached(): bool
    {
        return $this->loginFailureCount() >= self::MAX_LOGIN_ATTEMPTS_WITHIN_TIMESPAN;
    }

    public function mran(): CanonicalUserId
    {
        if (!isset($this->mran)) {
            return $this->canonicalUserId();
        }

        return $this->mran;
    }

    public function nominateMran(CanonicalUserId $mran): self
    {
        $instance = (clone $this)->setNominatedMran($mran);

        return $instance->recordThat(MranWasNominated::fromUser($instance));
    }

    public function confirmMran(CanonicalUserId $mran): self
    {
        if ((string)$this->nominatedMran !== (string)$mran) {
            throw new Exception('The nominated mran is different from what is being set');
        }

        $instance = clone $this;

        return $instance->recordThat(MranWasConfirmed::fromUser($instance->confirmNominatedMran($mran)));
    }

    public function requestNewPassword(): self
    {
        $anotherInstance = clone $this;

        return $anotherInstance->recordThat(ForgotPasswordRequested::create($anotherInstance));
    }

    public function requestDeletion(): self
    {
        $anotherInstance = clone $this;

        return $anotherInstance->recordThat(DeleteWasRequested::fromUser($anotherInstance));
    }

    private function createSnapshot(): self
    {
        $anotherInstance = clone $this;

        return $anotherInstance->recordThat(SnapshotWasCreated::create($anotherInstance));
    }

    public function delete(): self
    {
        $anotherInstance = clone $this;
        $anotherInstance->wasDeleted = true;

        return $anotherInstance->recordThat(UserWasDeleted::fromUser($anotherInstance));
    }

    public function wasDeleted(): bool
    {
        return $this->wasDeleted;
    }

    public function passwordHistory(): PasswordHistory
    {
        return $this->passwordHistory;
    }

    public function failedLoginTimestamps(): FailedLoginTimestamps
    {
        return $this->failedLoginTimestamps;
    }

    public function loginCount(): int
    {
        return $this->loginCount;
    }

    private function setLastDeclaredActive(int $unixTimestamp)
    {
        $this->lastDeclaredActive = $unixTimestamp;

        return $this;
    }

    public function lastDeclaredActive(): \DateTimeInterface
    {
        return \DateTimeImmutable::createFromFormat('U', (string)(int)$this->lastDeclaredActive);
    }
}

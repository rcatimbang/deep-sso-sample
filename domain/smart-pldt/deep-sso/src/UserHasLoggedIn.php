<?php

declare(strict_types=1);

namespace SmartPldt\Deep\Sso;

use SmartPldt\Deep\Sso\CanonicalUserId\CanonicalUserId;
use SmartPldt\Deep\Sso\CanonicalUserId\InvalidCanonicalUserId;

final class UserHasLoggedIn implements DomainEvent
{
    private const EVENT_NAME = 'pldt-smart.deep.sso.user_has_logged_in';

    private UserId $userId;

    private float $occurredOn;

    private CanonicalUserId $mran;

    private int $loginCount;

    private function __construct()
    {
    }

    private function setUserId(UserId $userId): self
    {
        $this->userId = $userId;

        return $this;
    }

    private function setOccurredOn(float $unixTimestamp): self
    {
        $this->occurredOn = $unixTimestamp;

        return $this;
    }

    private function setMran(CanonicalUserId $mran)
    {
        $this->mran = $mran;

        return $this;
    }

    private function setLoginCount(int $loginCount)
    {
        $this->loginCount = $loginCount;

        return $this;
    }

    public static function create(User $user)
    {
        return (new static())
            ->setUserId($user->userId())
            ->setOccurredOn(\microtime(true))
            ->setMran($user->mran())
            ->setLoginCount($user->loginCount());
    }

    public function userId(): UserId
    {
        return $this->userId;
    }

    public function occurredOn(): \DateTimeInterface
    {
        return \DateTimeImmutable::createFromFormat('U', (string)(int)$this->occurredOn);
    }

    public function occurredOnTimestamp(): float
    {
        return $this->occurredOn;
    }

    public function mran(): CanonicalUserId
    {
        return $this->mran;
    }

    public function loginCount(): int
    {
        return $this->loginCount;
    }

    /**
     * @inheritDoc
     */
    public static function fromArray(array $eventAsArray, UserService $userService = null): DomainEvent
    {
        if (self::name() !== $eventAsArray['event_name']) {
            throw new EncodedEventMismatch();
        }

        return (new static())
            ->setUserId(UserId::fromString($eventAsArray['aggregate_id']))
            ->setOccurredOn($eventAsArray['occurred_on'])
            ->setMran(self::createCanonicalUserId($userService, $eventAsArray['body']['payload']['mran'] ?? ''))
            ->setLoginCount($eventAsArray['body']['payload']['login_count'] ?? 0)
        ;
    }

    public static function name(): string
    {
        return self::EVENT_NAME;
    }

    public function aggregateId(): AggregateId
    {
        return $this->userId();
    }

    public function __toString(): string
    {
        return \json_encode([
            'event_name' => self::name(),
            'aggregate_id' => (string)$this->userId(),
            'occurred_on' => $this->occurredOn,
            'body' => [
                'envelope' => [],
                'payload' => [
                    'mran' => (string)$this->mran(),
                    'login_count' => $this->loginCount()
                ]
            ]
        ]);
    }

    private static function createCanonicalUserId(?UserService $userService, string $mran): CanonicalUserId
    {
        return $mran ? $userService->createCanonicalUserId($mran) : InvalidCanonicalUserId::create();
    }
}

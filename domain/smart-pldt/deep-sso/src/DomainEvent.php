<?php

declare(strict_types=1);

namespace SmartPldt\Deep\Sso;

interface DomainEvent
{
    public function aggregateId(): AggregateId;

    public static function name(): string;

    public function occurredOn(): \DateTimeInterface;

    public function occurredOnTimestamp(): float;

    public function __toString(): string;

    /**
     * @throws EncodedEventMismatch
     */
    public static function fromArray(array $eventAsArray, UserService $userService = null): self;
}

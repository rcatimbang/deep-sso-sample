<?php

declare(strict_types=1);

namespace SmartPldt\Deep\Sso;

final class UserId implements AggregateId
{
    private string $userId;

    private function __construct(string $userId)
    {
        $this->userId = $userId;
    }

    /**
     * @throws InvalidUserId
     */
    public static function fromString(string $userId): self
    {
        $validUuid = '/^[0-9a-f]{8}-[0-9a-f]{4}-4[0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/i';
        if (!preg_match($validUuid, $userId)) {
            throw new InvalidUserId();
        }

        return new static(\strtolower($userId));
    }

    public function __toString(): string
    {
        return $this->userId;
    }
}

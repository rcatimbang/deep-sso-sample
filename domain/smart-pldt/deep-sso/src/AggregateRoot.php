<?php

declare(strict_types=1);

namespace SmartPldt\Deep\Sso;

abstract class AggregateRoot implements RecordsEvents, IsEventSourced
{
    abstract public function aggregateId(): AggregateId;
}

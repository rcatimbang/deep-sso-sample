<?php

namespace SmartPldt\Deep\Sso;

use SmartPldt\Deep\Sso\CanonicalUserId\CanonicalUserId;

interface EventStore
{
    public function find(AggregateId $aggregateId): DomainEvents;

    public function save(DomainEvent $domainEvent): void;

    /**
     * @return DomainEvent[]
     */
    public function findByCanonicalUserId(CanonicalUserId $canonicalUserId): DomainEvents;

    public function dormantUserIds(int $size): \Iterator;

    public function purgeEventsMarkedDeletedByUserIds(UserId ...$userIds): void;

    public function findByCriteria(Criteria $criteria): DomainEvents;
}

<?php

namespace SmartPldt\Deep\Sso;

interface AggregateId
{
    public function __toString();
}

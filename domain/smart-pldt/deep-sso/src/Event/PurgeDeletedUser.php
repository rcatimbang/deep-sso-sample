<?php

declare(strict_types=1);

namespace SmartPldt\Deep\Sso\Event;

use SmartPldt\Deep\Sso\EventStore;
use SmartPldt\Deep\Sso\MinNotificationRecipient;
use SmartPldt\Deep\Sso\Notifier;
use SmartPldt\Deep\Sso\User;
use SmartPldt\Deep\Sso\UserNotFoundError;
use SmartPldt\Deep\Sso\UserService;

final class PurgeDeletedUser implements HandlesAnEventMessage
{
    public const EVENT_NAME_TO_HANDLE = 'pldt-smart.deep.sso.user_was_deleted';

    private EventStore $eventStore;

    private UserService $userService;

    private Notifier $notifier;

    public function __construct(EventStore $eventStore, UserService $userService, Notifier $notifier)
    {
        $this->eventStore = $eventStore;
        $this->userService = $userService;
        $this->notifier = $notifier;
    }

    public function eventNameToHandle(): string
    {
        return self::EVENT_NAME_TO_HANDLE;
    }

    public function __invoke(string $encodedDomainEvent): void
    {
        $decodedEvent = \json_decode($encodedDomainEvent, true);
        $this->throwAnErrorIfEventNameIsNotSupported($decodedEvent);
        $this->purgeThenNotifyMatchingUser($decodedEvent['aggregate_id']);
    }

    /**
     * @throws Exception
     */
    private function throwAnErrorIfEventNameIsNotSupported(array $decodedEvent): void
    {
        if ($decodedEvent['event_name'] !== $this->eventNameToHandle()) {
            throw new Exception('Unable to process event payload');
        }
    }

    private function purgeThenNotifyMatchingUser(string $userId): void
    {
        try {
            $user = $this->userService->findUserByUserId($userId);
            $this->eventStore->purgeEventsMarkedDeletedByUserIds($user->userId());
            $this->notifyUser($user);
        } catch (UserNotFoundError $exception) {
        } catch (\Throwable $exception) {
            throw $exception;
        }
    }

    private function notifyUser(User $user): void
    {
        $recipient = MinNotificationRecipient::create((string)$user->mran());
        $notificationMessage = 'Your GigaLife App account has been deleted. All linked accounts were also '
            . 'unlinked and informed.';
        $this->notifier->notify($recipient, $notificationMessage);
    }
}

<?php

declare(strict_types=1);

namespace SmartPldt\Deep\Sso;

final class DomainEvents implements \IteratorAggregate, \Countable
{
    /** @var DomainEvent[] */
    private $events = [];

    // TODO: require a single DomainEvent
    public function __construct(DomainEvent ...$events)
    {
        $this->events = $events;
    }

    public function getIterator(): \Traversable
    {
        return new \ArrayIterator($this->events);
    }

    public function count(): int
    {
        return \count($this->events);
    }

    public function toArray(): array
    {
        return $this->events;
    }
}

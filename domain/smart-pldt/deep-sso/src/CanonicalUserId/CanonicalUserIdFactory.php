<?php

declare(strict_types=1);

namespace SmartPldt\Deep\Sso\CanonicalUserId;

final class CanonicalUserIdFactory
{
    /** @var string[] */
    private array $supportedTypes = [];

    private HashFactory $hashFactory;

    public function __construct(HashFactory $hashFactory, string ...$subtypesOfCanonicalUserId)
    {
        $this->hashFactory = $hashFactory;
        $this->supportedTypes = $subtypesOfCanonicalUserId;
    }

    /**
     * @throws InvalidCanonicalUserId
     */
    public function create(string $canonicalUserId): CanonicalUserId
    {
        return $this->createUserIdByType($canonicalUserId, ...$this->supportedTypes);
    }

    /**
     * @throws InvalidCanonicalUserId
     */
    private function createUserIdByType(string $canonicalUserId, string ...$canonicalUserIdTypes): CanonicalUserId
    {
        if (empty($canonicalUserIdTypes)) {
            throw new InvalidCanonicalUserId('No UserId type supports the given user id');
        }

        try {
            return (\array_shift($canonicalUserIdTypes))::createThroughHashFactory(
                $canonicalUserId,
                $this->hashFactory
            );
        } catch (InvalidCanonicalUserId $exception) {
        }

        return $this->createUserIdByType($canonicalUserId, ...$canonicalUserIdTypes);
    }
}

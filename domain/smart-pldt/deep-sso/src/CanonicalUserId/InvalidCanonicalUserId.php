<?php

declare(strict_types=1);

namespace SmartPldt\Deep\Sso\CanonicalUserId;

class InvalidCanonicalUserId extends \Exception implements CanonicalUserId
{
    public static function create(string $canonicalUserId = '', Hash $hash = null): CanonicalUserId
    {
        return new static('Invalid canonical user id');
    }

    public function hash(): string
    {
        throw $this;
    }

    public function __toString(): string
    {
        throw $this;
    }
}

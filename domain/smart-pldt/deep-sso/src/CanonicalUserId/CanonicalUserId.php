<?php

declare(strict_types=1);

namespace SmartPldt\Deep\Sso\CanonicalUserId;

interface CanonicalUserId
{
    public static function create(string $canonicalUserId, Hash $hash): self;

    public function hash(): string;

    public function __toString(): string;
}

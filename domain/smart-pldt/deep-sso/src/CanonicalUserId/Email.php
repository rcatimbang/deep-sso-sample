<?php

declare(strict_types=1);

namespace SmartPldt\Deep\Sso\CanonicalUserId;

final class Email implements CanonicalUserId
{
    private string $userId;

    private string $hash;

    /**
     * @throws InvalidCanonicalUserId
     */
    private function __construct(string $email)
    {
        self::throwErrorIfEmailIsInvalid($email);

        $this->userId = $email;
    }

    private static function throwErrorIfEmailIsInvalid(string $email): void
    {
        if (!\filter_var($email, \FILTER_VALIDATE_EMAIL)) {
            throw new InvalidCanonicalUserId();
        }
    }

    /**
     * @throws InvalidCanonicalUserId
     */
    public static function createThroughHashFactory(string $email, HashFactory $hashFactory): CanonicalUserId
    {
        $canonicalUserId = new static(\strtolower($email));
        $canonicalUserId->hash = (string)$hashFactory->hash($canonicalUserId);

        return $canonicalUserId;
    }

    /**
     * @throws InvalidCanonicalUserId
     */
    public static function create(string $email, Hash $hash): CanonicalUserId
    {
        $canonicalUserId = new static(\strtolower($email));
        $canonicalUserId->hash = (string)$hash;

        return $canonicalUserId;
    }

    public function hash(): string
    {
        return $this->hash;
    }

    public function __toString(): string
    {
        return $this->userId;
    }
}

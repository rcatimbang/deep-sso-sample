<?php

declare(strict_types=1);

namespace SmartPldt\Deep\Sso\CanonicalUserId;

interface Hash
{
    public function __toString(): string;
}

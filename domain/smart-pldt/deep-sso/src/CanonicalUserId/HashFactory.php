<?php

namespace SmartPldt\Deep\Sso\CanonicalUserId;

interface HashFactory
{
    public function hash(CanonicalUserId $canonicalUserId): Hash;
}

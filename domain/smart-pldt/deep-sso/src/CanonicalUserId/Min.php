<?php

declare(strict_types=1);

namespace SmartPldt\Deep\Sso\CanonicalUserId;

final class Min implements CanonicalUserId
{
    private string $canonicalUserId;

    private string $hash;

    private function __construct(string $min)
    {
        $this->canonicalUserId = $min;
    }

    /**
     * @throws InvalidCanonicalUserId
     */
    private static function throwErrorIfMinIsInvalid(string $min): void
    {
        if (!(\preg_match('/^(09|639)\d{9}$/', $min))) {
            throw new InvalidCanonicalUserId();
        }
    }

    /**
     * @throws InvalidCanonicalUserId
     */
    public static function create(string $min, Hash $hash): CanonicalUserId
    {
        self::throwErrorIfMinIsInvalid($minCandidate = \preg_replace('/[^\d]+/m', '', $min));
        $canonicalUserId = new static('0' . \substr($minCandidate, -10));
        $canonicalUserId->hash = (string)$hash;

        return $canonicalUserId;
    }

    /**
     * @throws InvalidCanonicalUserId
     */
    public static function createThroughHashFactory(string $min, HashFactory $hashFactory): CanonicalUserId
    {
        self::throwErrorIfMinIsInvalid($minCandidate = \preg_replace('/[^\d]+/m', '', $min));

        $canonicalUserId = new static('0' . \substr($minCandidate, -10));
        $canonicalUserId->hash = (string)$hashFactory->hash($canonicalUserId);

        return $canonicalUserId;
    }

    public function hash(): string
    {
        return $this->hash;
    }

    public function __toString(): string
    {
        return $this->canonicalUserId;
    }
}

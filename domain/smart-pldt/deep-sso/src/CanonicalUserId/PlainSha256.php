<?php

declare(strict_types=1);

namespace SmartPldt\Deep\Sso\CanonicalUserId;

final class PlainSha256 implements Hash
{
    private string $hash;

    private function __construct(string $hash)
    {
        $this->hash = $hash;
    }

    public static function fromHash(string $hash): self
    {
        if (!\preg_match('/^[a-z0-9]{64}$/i', $hash)) {
            throw new InvalidHash();
        }

        return new static(\strtolower($hash));
    }

    public static function create(): self
    {
        return new static(\hash('sha256', \random_bytes(64)));
    }

    public function __toString(): string
    {
        return $this->hash;
    }
}

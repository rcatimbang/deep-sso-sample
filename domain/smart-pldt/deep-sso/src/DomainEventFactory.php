<?php

declare(strict_types=1);

namespace SmartPldt\Deep\Sso;

/**
 * TODO: Perhaps refactor events such that they don't require UserService during reconstruction of event
 */
final class DomainEventFactory
{
    /** @var string[] */
    private $supportedTypes = [];

    private UserService $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function addSignature(string $eventClassName): self
    {
        $eventName = $eventClassName::name();

        if (isset($this->supportedTypes[$eventName])) {
            throw new Exception("A signature for `{$eventName}` already exists");
        }

        return $this->addSignatureOnAnotherInstance($eventName, $eventClassName);
    }

    public function create(string $eventName, array $eventAsArray): DomainEvent
    {
        return ($this->supportedTypes[$eventName])::fromArray($eventAsArray, $this->userService);
    }

    private function addSignatureOnAnotherInstance(string $eventName, string $eventClassName): self
    {
        $anotherInstance = clone $this;
        $anotherInstance->supportedTypes[$eventName] = $eventClassName;

        return $anotherInstance;
    }
}

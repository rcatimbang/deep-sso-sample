<?php

namespace spec\SmartPldt\Deep\Sso;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use SmartPldt\Deep\Sso\AggregateRoot;
use SmartPldt\Deep\Sso\CanonicalUserId\CanonicalUserId;
use SmartPldt\Deep\Sso\CanonicalUserId\Min;
use SmartPldt\Deep\Sso\CanonicalUserId\PlainSha256;
use SmartPldt\Deep\Sso\DeleteWasRequested;
use SmartPldt\Deep\Sso\DomainEvent;
use SmartPldt\Deep\Sso\DomainEvents;
use SmartPldt\Deep\Sso\Exception;
use SmartPldt\Deep\Sso\FailedLoginTimestamps;
use SmartPldt\Deep\Sso\ForgotPasswordRequested;
use SmartPldt\Deep\Sso\Hash\Argon2idPassword;
use SmartPldt\Deep\Sso\Hash\PasswordHash;
use SmartPldt\Deep\Sso\Hash\PasswordHistoryHash;
use SmartPldt\Deep\Sso\Hash\Sha256Hmac;
use SmartPldt\Deep\Sso\IsEventSourced;
use SmartPldt\Deep\Sso\MranWasConfirmed;
use SmartPldt\Deep\Sso\MranWasNominated;
use SmartPldt\Deep\Sso\NewPasswordWasSet;
use SmartPldt\Deep\Sso\Password;
use SmartPldt\Deep\Sso\PasswordChangeFailed;
use SmartPldt\Deep\Sso\RecordsEvents;
use SmartPldt\Deep\Sso\SnapshotWasCreated;
use SmartPldt\Deep\Sso\User;
use SmartPldt\Deep\Sso\UserChangedPassword;
use SmartPldt\Deep\Sso\UserDeclaredActive;
use SmartPldt\Deep\Sso\UserFailedToLogin;
use SmartPldt\Deep\Sso\UserHasLoggedIn;
use SmartPldt\Deep\Sso\UserId;
use SmartPldt\Deep\Sso\UserRegistrationVerified;
use SmartPldt\Deep\Sso\UserService;
use SmartPldt\Deep\Sso\UserWasDeleted;
use SmartPldt\Deep\Sso\UserWasRegistered;

/**
 * @mixin User
 *
 * TODO: no state changes should be allowed unless a user is verified
 */
class UserSpec extends ObjectBehavior
{
    use ObjectBehaviorHelper;

    private static int $loginCount;

    private string $canonicalUserIdAsString;

    private string $passwordAsString;

    function let(
        UserService $userService,
        PasswordHash $passwordHash,
        CanonicalUserId $canonicalUserId
    ) {
        $userService->generateUserId()->willReturn(UserId::fromString('de960b8a-1021-4189-9683-37ac9821fa20'));
        $userService->createPasswordHash(Argument::type(Password::class))->willReturn($passwordHash);
        $userService->createCanonicalUserId(Argument::type('string'))->willReturn($canonicalUserId);

        $this->beConstructedThrough(
            'register',
            [$this->canonicalUserIdAsString = '09191234567', $this->passwordAsString = 'p@s$W0rd', $userService]
        );
    }

    function it_should_be_an_AggregateRoot()
    {
        $this->shouldHaveType(AggregateRoot::class);
        $this->shouldHaveType(RecordsEvents::class);
        $this->shouldHaveType(IsEventSourced::class);
    }

    function it_should_return_the_AggregateId()
    {
        $this->shouldNotThrow(\Throwable::class)->during('userId');
        $this->shouldNotThrow(\Throwable::class)->during('aggregateId');
    }

    function it_should_return_the_Password()
    {
        $this->shouldNotThrow(\Throwable::class)->during('password');
    }

    function it_should_return_the_CanonicalUserId()
    {
        $this->shouldNotThrow(\Throwable::class)->during('canonicalUserId');
    }

    function it_should_return_the_FailedLoginTimestamps()
    {
        $this->shouldNotThrow(\Throwable::class)->during('failedLoginTimestamps');
    }

    function it_should_return_the_PasswordHistory()
    {
        $this->passwordHistory()->shouldHaveCount(1);
    }

    // TODO: BAD IDEA - better move this to controller - i.e., two method calls, register() and nominateMran()
    function it_should_record_UserWasRegistered_and_MranWasNominated_events_during_registration_if_no_mran_is_nominated(
        UserService $userService
    ) {
        $canonicalUserId = Min::create($this->canonicalUserIdAsString, PlainSha256::create());
        $userService->createCanonicalUserId($this->canonicalUserIdAsString)->willReturn($canonicalUserId);

        $domainEvents = $this->recordedEvents();
        $domainEvents->shouldContainType(UserWasRegistered::class);
        $domainEvents->shouldContainType(MranWasNominated::class);
        $this->passwordHistory()->shouldHaveCount(1);
    }

    function it_should_have_different_CanonicalUserIds_if_another_CanonicalUserId_was_nominated_as_mran(
        CanonicalUserId $anotherCanonicalUserId
    )
    {
        $anotherUser = $this->nominateMran($anotherCanonicalUserId);
        $anotherUser->canonicalUserId()->shouldNotBeLike($anotherUser->nominatedMran());
    }

    function it_should_return_the_CanonicalUserId_as_mran_if_none_is_set()
    {
        $this->mran()->shouldBe($this->canonicalUserId());
    }

    function it_should_apply_UserWasRegistered_and_MranWasNominated_events(UserService $userService)
    {
        $userWasRegistered = UserWasRegistered::fromArray(\json_decode(
            '{"event_name":"pldt-smart.deep.sso.user_was_registered","aggregate_id":"de960b8a-1021-4189-9683-37ac9'
            . '821fa20","occurred_on":1597936624.839982,"canonical_user_id_hash":"ang_hash_mo_naman","body":{"envelope"'
            . ':[],"payload":{"canonical_user_id":"09191234567","password_hash":"$argon2id$v=19$m=65536,t=4,p=1$THQ2dFh'
            . 'taVd4bmR1RXFjWg$bDKVAg+JfmrJMynLt0xPbuOl+HW0+ZI5\/R\/WCOOl4jI","password_history":["12345678"]}}}',
            true
        ), $userService->getWrappedObject());
        $mranWasNominated = MranWasNominated::fromArray(\json_decode(
            '{"event_name":"pldt-smart.deep.sso.mran_was_nominated","aggregate_id":"d36f1628-1021-4189-9683-37ac98'
            . '21fa20","occurred_on":1597243711,"body":{"envelope":[],"payload":{"mran":"09190003331"}}}',
            true
        ), $userService->getWrappedObject());

        $this->beConstructedThrough('reconstituteFrom', [new DomainEvents($userWasRegistered, $mranWasNominated)]);

        $this->recordedEvents()->shouldHaveCount(0);
        $this->userId()->shouldBe($userWasRegistered->aggregateId());
        $this->password()->shouldBe($userWasRegistered->password());
        $this->canonicalUserId()->shouldBe($userWasRegistered->canonicalUserId());
        $this->registeredOn()->getTimestamp()->shouldBe($userWasRegistered->occurredOn()->getTimestamp());
        $this->nominatedMran()->shouldBe($mranWasNominated->mran());
        $this->passwordHistory()->shouldHaveCount(1);
    }

    function it_should_record_UserSuccessfullyLoggedIn_event(UserService $userService)
    {
        $userId = UserId::fromString($aggregateId = '13028ac2-b374-4c91-99e4-eeceee6d1ebd');
        $correctHmacHash = Sha256Hmac::fromHash('65eb80b88c69e20b2174101a3e82070bf473cfd2b6847387b314a6127419ce1c');
        $userService
            ->calculateHmacHash($passwordValue = 'kunwaring-tama-na-P4s$w0rd')
            ->shouldBeCalledOnce()
            ->willReturn($correctHmacHash);

        $domainEvents = [
            $this->createUserWasRegisteredEvent(
                '$argon2id$v=19$m=65536,t=4,p=1$MjI4enhOdWlqQVdQNkdmRw$up21SANf1UkUjZfrbdPZfgOVgSAMqSDNToKzV5/cIAk',
                \time(),
                $userService
            ),
            $this->createUserRegistrationVerified($userId, $userService)
        ];

        $this->beConstructedThrough('reconstituteFrom', [new DomainEvents(...$domainEvents)]);
        $anotherUser = $this->login($passwordValue, $userService);
        $anotherUser->shouldNotBeLike($this);
        $anotherUser->recordedEvents()->shouldContainType(UserHasLoggedIn::class);
    }

    function it_should_count_the_number_of_successful_logins(UserService $userService)
    {
        $userId = UserId::fromString($aggregateId = '13028ac2-b374-4c91-99e4-eeceee6d1ebd');
        $correctHmacHash = Sha256Hmac::fromHash('65eb80b88c69e20b2174101a3e82070bf473cfd2b6847387b314a6127419ce1c');
        $userService
            ->calculateHmacHash($passwordValue = 'kunwaring-tama-na-P4s$w0rd')
            ->shouldBeCalledTimes(2)
            ->willReturn($correctHmacHash);

        $domainEvents = [
            $this->createUserWasRegisteredEvent(
                '$argon2id$v=19$m=65536,t=4,p=1$MjI4enhOdWlqQVdQNkdmRw$up21SANf1UkUjZfrbdPZfgOVgSAMqSDNToKzV5/cIAk',
                \time(),
                $userService
            ),
            $this->createUserRegistrationVerified($userId, $userService)
        ];

        $this->beConstructedThrough('reconstituteFrom', [new DomainEvents(...$domainEvents)]);
        $anotherUser = $this->login($passwordValue, $userService);
        $anotherUser->loginCount()->shouldBe(1);
        $anotherUser = $anotherUser->login($passwordValue, $userService);
        $anotherUser->loginCount()->shouldBe(2);
    }

    function it_should_record_UserFailedToLogin_event_when_User_is_not_verified(UserService $userService)
    {
        $passwordValue = 'kunwaring-tama-na-P4s$w0rd';

        $domainEvents = new DomainEvents(
            $this->createUserWasRegisteredEvent(
                '$argon2id$v=19$m=65536,t=4,p=1$MjI4enhOdWlqQVdQNkdmRw$up21SANf1UkUjZfrbdPZfgOVgSAMqSDNToKzV5/cIAk',
                \time(),
                $userService
            )
        );

        $this->beConstructedThrough('reconstituteFrom', [$domainEvents]);
        $anotherUser = $this->login($passwordValue, $userService);
        $anotherUser->isVerified()->shouldReturn(false);
        $anotherUser->shouldNotBeLike($this);
        $anotherUser->recordedEvents()->shouldContainType(UserFailedToLogin::class);
    }

    function it_should_record_UserFailedToLogin_event_when_login_attempts_exceeds_limit(UserService $userService)
    {
        $pwHash = '$argon2id$v=19$m=65536,t=4,p=1$MjI4enhOdWlqQVdQNkdmRw$up21SANf1UkUjZfrbdPZfgOVgSAMqSDNToKzV5/cIAk';
        $domainEvents = $this->createDomainEventsWithFiveFailedLogins($pwHash, $userService);
        $userService->calculateHmacHash($passwordValue = 'kunwaring-tama-na-P4s$w0rd')->shouldNotBeCalled();

        $this->beConstructedThrough('reconstituteFrom', [$domainEvents]);

        $this->loginFailureCount()->shouldBe(5);
        $anotherInstance = $this->login($passwordValue, $userService);
        $anotherInstance->recordedEvents()->shouldContainType(UserFailedToLogin::class);
        $anotherInstance->loginFailureCount()->shouldBe(6);
    }

    function it_should_record_UserFailedToLogin_event_when_password_is_incorrect(UserService $userService)
    {
        $passwordValue = 'kunwaring-maling-na-P4s$w0rd';

        $anotherUser = $this->login($passwordValue, $userService);
        $anotherUser->shouldNotBeLike($this);
        $anotherUser->recordedEvents()->shouldContainType(UserFailedToLogin::class);
    }

    function it_should_apply_UserFailedToLogin_event(UserService $userService)
    {
        $oldestAllowedTimestamp = \microtime(true) - FailedLoginTimestamps::TIMESPAN_IN_SECONDS;
        $events[] = UserWasRegistered::fromArray(\json_decode(
            \sprintf(
                '{"event_name":"pldt-smart.deep.sso.user_was_registered","aggregate_id":"de960b8a-1021-4189-9683-37ac98'
                . '21fa20","occurred_on":%f,"canonical_user_id_hash":"ang_hash_mo_naman","body":{"envelope":[],"payload'
                . '":{"canonical_user_id":"09191234567","password_hash":"$argon2id$v=19$m=65536,t=4,p=1$MjI4enhOdWlqQVd'
                . 'QNkdmRw$up21SANf1UkUjZfrbdPZfgOVgSAMqSDNToKzV5/cIAk","password_history":["12345678"]}}}',
                $oldestAllowedTimestamp
            ),
            true
        ), $userService->getWrappedObject());

        $events[] = $this->createUserFailedToLoggedIn(\end($events), 120);
        $events[] = $lastEvent = $this->createUserFailedToLoggedIn(\end($events), 120);

        $this->beConstructedThrough('reconstituteFrom', [new DomainEvents(...$events)]);

        $this->lastLoginFailureDate()->getTimestamp()->shouldBe($lastEvent->occurredOn()->getTimestamp());
        $this->loginFailureCount()->shouldBe(2);
    }

    function it_should_count_the_number_of_failed_login_attempts(UserService $userService)
    {
        $anotherInstance = $this
            ->login('sobR4ng-mal!ng-pas$w0RD', $userService)
            ->login('sobR4ng-mal!ng-pas$w0RD', $userService);

        $anotherInstance->loginFailureCount()->shouldBe(2);
    }

    function it_should_clear_the_number_of_failed_login_attempts(UserService $userService)
    {
        // TODO: awful test
        $min = $this->randomMin();
        $password = $this->randomPassword();
        $hmacHash = $this->hmacHashFor($password);
        $passwordHash = $this->passwordHashFor($hmacHash);
        $malingPassword = 'wow+m4Li';

        $userService
            ->createPasswordHash(Argument::which('__toString', $password))
            ->willReturn($passwordHash);
        $userService->createCanonicalUserId((string)$min)->willReturn($min);
        $userService->calculateHmacHash($password)->willReturn($hmacHash);
        $userService->calculateHmacHash($malingPassword)->willReturn($this->hmacHashFor($malingPassword));

        $this->beConstructedThrough('register', [(string)$min, $password, $userService]);


        $anotherInstance = $this
            ->nominateMran($min)
            ->verify($min)
            ->login($malingPassword, $userService)
            ->login($malingPassword, $userService)
        ;
        $anotherInstance->lastLoginFailureCode()->shouldBe(8);
        $anotherInstance
            ->login($password, $userService)
            ->login($malingPassword, $userService)
            ->login($malingPassword, $userService)
            ->login($password, $userService)
            ->lastLoginFailureCode()->shouldBe(0)
        ;
    }

    function it_should_return_login_failure_code_for_not_verified_user(UserService $userService)
    {
        $event = UserWasRegistered::fromArray(\json_decode(
            '{"event_name":"pldt-smart.deep.sso.user_was_registered","aggregate_id":"de960b8a-1021-4189-9683-37ac9'
            . '821fa20","occurred_on":1597936624.839982,"canonical_user_id_hash":"ang_hash_mo_naman","body":{"envelope"'
            . ':[],"payload":{"canonical_user_id":"09191234567","password_hash":"$argon2id$v=19$m=65536,t=4,p=1$MjI4enh'
            . 'OdWlqQVdQNkdmRw$up21SANf1UkUjZfrbdPZfgOVgSAMqSDNToKzV5/cIAk","password_history":["12345678"]}}}',
            true
        ), $userService->getWrappedObject());

        $this->beConstructedThrough('reconstituteFrom', [new DomainEvents($event)]);
        $anotherUser = $this->login('m4L!ngPas$w0rd', $userService);
        $anotherUser->lastLoginFailureCode()->shouldBe(1);
    }

    function it_should_return_login_failure_code_for_deleted_user(UserService $userService)
    {
        $userWasRegistered = UserWasRegistered::fromArray(\json_decode(
            '{"event_name":"pldt-smart.deep.sso.user_was_registered","aggregate_id":"de960b8a-1021-4189-9683-37ac9'
            . '821fa20","occurred_on":1597936624.839982,"canonical_user_id_hash":"ang_hash_mo_naman","body":{"envelope"'
            . ':[],"payload":{"canonical_user_id":"09191234567","password_hash":"$argon2id$v=19$m=65536,t=4,p=1$MjI4enh'
            . 'OdWlqQVdQNkdmRw$up21SANf1UkUjZfrbdPZfgOVgSAMqSDNToKzV5/cIAk","password_history":["12345678"]}}}',
            true
        ), $userService->getWrappedObject());
        $events = [
            $userWasRegistered,
            $this->createUserRegistrationVerified($userWasRegistered->userId(), $userService),
            $this->createUserWasDeletedEvent($userWasRegistered->userId(), $userService)
        ];

        $this->beConstructedThrough('reconstituteFrom', [new DomainEvents(...$events)]);
        $anotherUser = $this->login('m4L!ngPas$w0rd', $userService);
        $anotherUser->lastLoginFailureCode()->shouldBe(2);
    }

    function it_should_return_login_failure_code_for_reaching_login_limit(UserService $userService)
    {
        $incorrectHmacHash = Sha256Hmac::fromHash(\hash('sha256', \random_bytes(4)));
        $userService
            ->calculateHmacHash($malingPassword = 'm4L!ngPas$w0rd')
            ->willReturn($incorrectHmacHash);
        $correctHmacHash = Sha256Hmac::fromHash('65eb80b88c69e20b2174101a3e82070bf473cfd2b6847387b314a6127419ce1c');
        $userService
            ->calculateHmacHash($tamangPassword = 'kunwaring-tama-na-P4s$w0rd')
            ->willReturn($correctHmacHash);

        $events[] = UserWasRegistered::fromArray(\json_decode(
            '{"event_name":"pldt-smart.deep.sso.user_was_registered","aggregate_id":"de960b8a-1021-4189-9683-37ac9'
            . '821fa20","occurred_on":1597936624.839982,"canonical_user_id_hash":"ang_hash_mo_naman","body":{"envelope"'
            . ':[],"payload":{"canonical_user_id":"09191234567","password_hash":"$argon2id$v=19$m=65536,t=4,p=1$MjI4enh'
            . 'OdWlqQVdQNkdmRw$up21SANf1UkUjZfrbdPZfgOVgSAMqSDNToKzV5/cIAk","password_history":["12345678"]}}}',
            true
        ), $userService->getWrappedObject());
        $events[] = $this->createUserRegistrationVerified($events[0]->userId(), $userService);

        $this->beConstructedThrough('reconstituteFrom', [new DomainEvents(...$events)]);

        $user = $this->login($malingPassword, $userService);
        $user->lastLoginFailureCode()->shouldBe(8);

        $user = $user->login($malingPassword, $userService);
        $user->lastLoginFailureCode()->shouldBe(8);

        $user = $user->login($malingPassword, $userService);
        $user->lastLoginFailureCode()->shouldBe(8);

        $user = $user->login($tamangPassword, $userService);
        $user->lastLoginFailureCode()->shouldBe(4);
    }

    function it_should_return_login_failure_code_for_incorrect_password(UserService $userService)
    {
        $incorrectHmacHash = Sha256Hmac::fromHash(\hash('sha256', \random_bytes(4)));
        $userService
            ->calculateHmacHash($passwordValue = 'm4L!ngPas$w0rd')
            ->willReturn($incorrectHmacHash);

        $userWasRegistered = UserWasRegistered::fromArray(\json_decode(
            '{"event_name":"pldt-smart.deep.sso.user_was_registered","aggregate_id":"de960b8a-1021-4189-9683-37ac9'
            . '821fa20","occurred_on":1597936624.839982,"canonical_user_id_hash":"ang_hash_mo_naman","body":{"envelope"'
            . ':[],"payload":{"canonical_user_id":"09191234567","password_hash":"$argon2id$v=19$m=65536,t=4,p=1$MjI4enh'
            . 'OdWlqQVdQNkdmRw$up21SANf1UkUjZfrbdPZfgOVgSAMqSDNToKzV5/cIAk","password_history":["12345678"]}}}',
            true
        ), $userService->getWrappedObject());
        $events = [
            $userWasRegistered,
            $this->createUserRegistrationVerified($userWasRegistered->userId(), $userService)
        ];

        $this->beConstructedThrough('reconstituteFrom', [new DomainEvents(...$events)]);
        $anotherUser = $this->login($passwordValue, $userService);

        $anotherUser->lastLoginFailureCode()->shouldBe(8);
    }

    function it_should_not_get_verified_if_provided_with_an_mran_that_was_not_nominated(
        CanonicalUserId $yetAnotherCanonicalUserId,
        CanonicalUserId $wrongMran
    ) {
        $yetAnotherCanonicalUserId->__toString()->willReturn('foo');
        $wrongMran->__toString()->willReturn('bar');

        $anotherUser = $this->nominateMran($yetAnotherCanonicalUserId)->verify($wrongMran);
        $anotherUser->isVerified()->shouldBe(false);
        $anotherUser->recordedEvents()->shouldNotContainType(UserRegistrationVerified::class);
    }

    function it_should_get_verified_if_provided_with_the_nominated_mran(CanonicalUserId $theNominatedMran)
    {
        $theNominatedMran->__toString()->willReturn('foobar');

        $anotherUser = $this->nominateMran($theNominatedMran)->verify($theNominatedMran);
        $anotherUser->isVerified()->shouldBe(true);
        $anotherUser->recordedEvents()->shouldContainType(UserRegistrationVerified::class);
    }

    public function it_should_record_PasswordChangeFailed_event(UserService $userService)
    {
        $incorrectHmacHash = Sha256Hmac::fromHash(\hash('sha256', \random_bytes(4)));
        $userService
            ->calculateHmacHash($passwordValue = 'kunwaring-maling-na-P4s$w0rd')
            ->shouldBeCalledOnce()
            ->willReturn($incorrectHmacHash);

        $domainEvents = [
            $userWasRegistered = UserWasRegistered::fromArray(\json_decode(
                '{"event_name":"pldt-smart.deep.sso.user_was_registered","aggregate_id":"de960b8a-1021-4189-968'
                . '3-37ac9821fa20","occurred_on":1597936624.839982,"canonical_user_id_hash":"ang_hash_mo_naman","body'
                . '":{"envelope":[],"payload":{"canonical_user_id":"09191234567","password_hash":"$argon2id$v=19$m='
                . '65536,t=4,p=1$MjI4enhOdWlqQVdQNkdmRw$up21SANf1UkUjZfrbdPZfgOVgSAMqSDNToKzV5/cIAk", '
                . '"password_history":["12345678"]}}}',
                true
            ), $userService->getWrappedObject()),
            $this->createUserRegistrationVerified($userWasRegistered->userId(), $userService),
        ];

        $this->beConstructedThrough('reconstituteFrom', [new DomainEvents(...$domainEvents)]);
        $user = $this->changePassword($passwordValue, $passwordValue, $userService);
        $user->recordedEvents()->shouldContainType(PasswordChangeFailed::class);
        $user->passwordHistory()->shouldHaveCount(1);
    }

    public function it_should_record_PasswordChangeFailed_event_due_to_usage_of_recently_used_password(
        UserService $userService
    ) {
        $correctHmacHash = Sha256Hmac::fromHash('65eb80b88c69e20b2174101a3e82070bf473cfd2b6847387b314a6127419ce1c');
        $userService
            ->calculateHmacHash($passwordValue = 'kunwaring-tama-na-P4s$w0rd')
            ->shouldBeCalledOnce()
            ->willReturn($correctHmacHash);

        $domainEvents = [
            $userWasRegistered = UserWasRegistered::fromArray(
                \json_decode(
                    \sprintf(
                        '{"event_name":"pldt-smart.deep.sso.user_was_registered","aggregate_id":"de960b8a-1021'
                        . '-4189-9683-37ac9821fa20","occurred_on":1597936624.839982,"canonical_user_id_hash":"ang_hash'
                        . '_mo_naman","body":{"envelope":[],"payload":{"canonical_user_id":"09191234567","password_'
                        . 'hash":"$argon2id$v=19$m=65536,t=4,p=1$MjI4enhOdWlqQVdQNkdmRw$up21SANf1UkUjZfrbdPZfgOVgSAMq'
                        . 'SDNToKzV5/cIAk","password_history":["%s","%s","63e1ecd4","1d6bdfd9","b20df54f"]}}}',
                        new PasswordHistoryHash($previouslyUsedPassword = '_Xe<cu4q'),
                        new PasswordHistoryHash('%u3x\Bs!')
                    ),
                    true
                ),
                $userService->getWrappedObject()
            ),
            $this->createUserRegistrationVerified($userWasRegistered->userId(), $userService),
        ];

        $this->beConstructedThrough('reconstituteFrom', [new DomainEvents(...$domainEvents)]);
        $user = $this->changePassword($passwordValue, $previouslyUsedPassword, $userService);
        $user->recordedEvents()->shouldContainType(PasswordChangeFailed::class);
        $user->passwordHistory()->shouldHaveCount(5);
    }

    public function it_should_apply_UserRegistrationVerified(UserService $userService)
    {
        $userId = UserId::fromString($aggregateId = '13028ac2-b374-4c91-99e4-eeceee6d1ebd');
        $domainEvents = [
            $userWasRegistered = UserWasRegistered::fromArray(\json_decode(
                '{"event_name":"pldt-smart.deep.sso.user_was_registered","aggregate_id":"de960b8a-1021-4189-9683-37ac9'
                . '821fa20","occurred_on":1597936624.839982,"canonical_user_id_hash":"ang_hash_mo_naman","body":{"envelope"'
                . ':[],"payload":{"canonical_user_id":"09191234567","password_hash":"$argon2id$v=19$m=65536,t=4,p=1$MjI4enh'
                . 'OdWlqQVdQNkdmRw$up21SANf1UkUjZfrbdPZfgOVgSAMqSDNToKzV5/cIAk","password_history":["12345678"]}}}',
                true
            ), $userService->getWrappedObject()),
            $registrationVerifiedEvent = $this->createUserRegistrationVerified($userId, $userService)
        ];

        $domainEvents = new DomainEvents(...$domainEvents);
        $this->beConstructedThrough('reconstituteFrom', [$domainEvents]);
        $this->mran()->shouldBe($registrationVerifiedEvent->mran());
        $this->isVerified()->shouldReturn(true);
    }

    public function it_should_record_UserChangedPassword_event(UserService $userService)
    {
        $correctHmacHash = Sha256Hmac::fromHash('65eb80b88c69e20b2174101a3e82070bf473cfd2b6847387b314a6127419ce1c');
        $userService
            ->calculateHmacHash($passwordValue = 'kunwaring-tama-na-P4s$w0rd')
            ->shouldBeCalledOnce()
            ->willReturn($correctHmacHash);

        $passwordHash = Argon2idPassword::fromPasswordHash('$argon2id$v=19$m=65536,t=4,p=1$MjI4enhOdPASSWORD');
        $userService->createPasswordHash(Argument::type(Password::class))->willReturn($passwordHash);
        $domainEvents = [
            $userWasRegistered = UserWasRegistered::fromArray(\json_decode(
                '{"event_name":"pldt-smart.deep.sso.user_was_registered","aggregate_id":"de960b8a-1021-4189-9683-37ac9'
                . '821fa20","occurred_on":1597936624.839982,"canonical_user_id_hash":"ang_hash_mo_naman","body":{"envelope"'
                . ':[],"payload":{"canonical_user_id":"09191234567","password_hash":"$argon2id$v=19$m=65536,t=4,p=1$MjI4enh'
                . 'OdWlqQVdQNkdmRw$up21SANf1UkUjZfrbdPZfgOVgSAMqSDNToKzV5/cIAk","password_history":["12345678"]}}}',
                true
            ), $userService->getWrappedObject()),
            $this->createUserRegistrationVerified($userWasRegistered->userId(), $userService)
        ];


        $this->beConstructedThrough('reconstituteFrom', [new DomainEvents(...$domainEvents)]);
        $user = $this->changePassword($passwordValue, '1Kunwaring-bagong-password', $userService);
        $user->recordedEvents()->shouldContainType(UserChangedPassword::class);
        $user->password()->__toString()->shouldEndWith('PASSWORD');
//        $user->passwordHistory()->shouldHaveCount(2);
    }

    public function it_should_apply_UserChangedPassword_event(UserService $userService)
    {
        $event = UserChangedPassword::fromArray(\json_decode(
            '{"event_name":"pldt-smart.deep.sso.user_changed_password","aggregate_id":"de960b8a-1021-4189-9683-37a'
            . 'c9821fa20","occurred_on":1594285760.8134,"body":{"envelope":[],"payload":{"canonical_user_id":"091912345'
            . '67","password":"$argon2id$v=19$m=65536,t=4,p=1$MjI4enhOdPASSWORD","password_history":["66a815f8","f44c94'
            . '09"]}}}',
            true
        ), $userService->getWrappedObject());

        $this->beConstructedThrough('reconstituteFrom', [new DomainEvents($event)]);
        $this->password()->__toString()->shouldEndWith('PASSWORD');
        $this->passwordHistory()->shouldHaveCount(2);
    }

    function it_should_set_a_new_Password(UserService $userService)
    {
        $user = $this->setNewPassword('AngT!n0l4anGManokNaWalang-Sabaw', $userService);
        $domainEvents = $user->recordedEvents();
        $domainEvents->shouldContainType(NewPasswordWasSet::class);
        $domainEvents->shouldHaveCount(3);
        $domainEvents->toArray()[1]->passwordHistory()->shouldBe($user->passwordHistory());
    }

    function it_should_not_accept_previously_used_password_as_new_Password(UserService $userService)
    {
        $user = $this->setNewPassword($this->passwordAsString, $userService);
        $domainEvents = $user->recordedEvents();
        $domainEvents->shouldContainType(PasswordChangeFailed::class);
        $domainEvents->shouldHaveCount(3);
        $user->passwordHistory()->toArray()->shouldBe($this->passwordHistory()->toArray());
    }

    function it_should_record_MranWasNominated()
    {
        $anotherUser = $this->nominateMran(Min::create('09990001118', PlainSha256::create()));

        $anotherUser->shouldNotBe($this);
        $anotherUser->recordedEvents()->shouldContainType(MranWasNominated::class);
    }

    function it_should_not_accept_an_mran_that_is_different_from_what_was_nominated()
    {
        $anotherUser = $this->nominateMran(Min::create('09992222222', PlainSha256::create()));
        $exception = new Exception('The nominated mran is different from what is being set');
        $anotherUser->shouldThrow($exception)->during('confirmMran', [Min::create('09991111111', PlainSha256::create())]);
    }

    function it_should_record_MranWasConfirmed(UserService $userService)
    {
        $mran = Min::create('09990001119', PlainSha256::create());
        $anotherUser = $this->nominateMran($mran)->confirmMran($mran);

        $anotherUser->shouldNotBe($this);
        $anotherUser->recordedEvents()->shouldContainType(MranWasConfirmed::class);
    }

    public function it_should_apply_NewPasswordWasSetEvent(UserService $userService)
    {
        $newPassword = Argon2idPassword::fromSha256Hmac(Sha256Hmac::create('m0St-S3cUr3P@ssw0rdzxc', 'salty'));
        $event = NewPasswordWasSet::fromArray(\json_decode(\sprintf(
            '{"event_name":"pldt-smart.deep.sso.new_password_was_set","aggregate_id":"de960b8a-1021-4189-9683-37ac'
            . '9821fa20","occurred_on":1594285760.1441092,"body":{"envelope":[],"payload":{"canonical_user_id":"0919123'
            . '4567","password_hash":"%s","password_history":["8aa451a6","538212ab"]}}}', $newPassword),
            true
        ), $userService->getWrappedObject());

        $this->beConstructedThrough('reconstituteFrom', [new DomainEvents($event)]);
        $this->password()->__toString()->shouldBe((string)$newPassword);
        $this->passwordHistory()->shouldHaveCount(2);
    }

    function it_should_clear_recorded_events(UserService $userService) {
        $this->beConstructedThrough('register', ['09191234567', $password = 'p@s$W0rd', $userService]);

        $this->recordedEvents()->shouldHaveCount(2);
        $this->clearEvents();
        $this->recordedEvents()->shouldHaveCount(0);
    }

    function it_should_record_ForgotPasswordRequested_event()
    {
        $user = $this->requestNewPassword();
        $user->recordedEvents()->shouldContainType(ForgotPasswordRequested::class);
    }

    function it_should_record_DeleteWasRequested_event()
    {
        $user = $this->requestDeletion();
        $user->recordedEvents()->shouldContainType(DeleteWasRequested::class);
    }

    function it_should_apply_DeleteWasRequested_event()
    {
        $event = $this->createObjectBuilderFor(DeleteWasRequested::class)->build();

        $this->beConstructedThrough('reconstituteFrom', [new DomainEvents($event)]);
        $this->shouldNotThrow(\Throwable::class)->duringInstantiation();
    }

    function it_should_record_UserWasDeleted()
    {
        $user = $this->delete();
        $this->wasDeleted()->shouldBe(false);
        $user->wasDeleted()->shouldBe(true);
        $user->recordedEvents()->shouldContainType(UserWasDeleted::class);
    }

    function it_should_apply_UserWasDeleted_event()
    {
        $event = $this->createObjectBuilderFor(UserWasDeleted::class)->build();

        $this->beConstructedThrough('reconstituteFrom', [new DomainEvents($event)]);
        $this->shouldNotThrow(\Throwable::class)->duringInstantiation();
        $this->wasDeleted()->shouldReturn(true);
    }

    function it_should_apply_UserHasLogginIn_event(UserService $userService)
    {
        $domainEvents = [
            $userHasLoggedIn = UserHasLoggedIn::fromArray(\json_decode(
                <<<JSON
                {
                        "event_name": "pldt-smart.deep.sso.user_has_logged_in",
                        "aggregate_id": "13028ac2-b374-4c91-99e4-eeceee6d1ebd",
                        "occurred_on": 1597936624.839982,
                        "body": {
                            "envelope": [],
                            "payload": {
                                "mran": "09184542312",
                                "login_count": 1
                            }
                        }
                }
                JSON
                , true
            ), $userService->getWrappedObject()),
        ];

        $domainEvents = new DomainEvents(...$domainEvents);
        $this->beConstructedThrough('reconstituteFrom', [$domainEvents]);
        $this->loginCount()->shouldBe($userHasLoggedIn->loginCount());
    }

    function it_should_create_SnapshotWasCreated_event_if_events_are_more_than_5(UserService $userService)
    {
        // TODO: awful test
        $min = $this->randomMin();
        $password = $this->randomPassword();
        $hmacHash = $this->hmacHashFor($password);
        $passwordHash = $this->passwordHashFor($hmacHash);
        $malingPassword = 'wow+m4Li';

        $userService
            ->createPasswordHash(Argument::which('__toString', $password))
            ->willReturn($passwordHash);
        $userService->createCanonicalUserId((string)$min)->willReturn($min);
        $userService->calculateHmacHash($password)->willReturn($hmacHash);
        $userService->calculateHmacHash($malingPassword)->willReturn($this->hmacHashFor($malingPassword));

        $user = User::register((string)$min, $password, $userService->getwrappedObject())
            ->nominateMran($mran = $this->randomMin())
            ->verify($mran)
            ->login($malingPassword, $userService->getWrappedObject())
            ->login($password, $userService->getWrappedObject())
            ->changePassword($password, '1Kunwaring-bagong-password', $userService->getwrappedObject())
            ->delete();
        $this->beConstructedThrough('reconstituteFrom', [$user->recordedEvents()]);
        $recordedEvents = $this->recordedEvents();
        $recordedEvents->shouldContainType(SnapshotWasCreated::class);
        $recordedEvents->shouldHaveCount(1);
    }

    function it_should_apply_SnapshotWasCreated_event(UserService $userService)
    {
        $oldestAllowedTimestamp = \microtime(true) - FailedLoginTimestamps::TIMESPAN_IN_SECONDS;
        $correctHmacHash = Sha256Hmac::fromHash('65eb80b88c69e20b2174101a3e82070bf473cfd2b6847387b314a6127419ce1c');
        $userService
            ->calculateHmacHash($passwordValue = 'kunwaring-tama-na-P4s$w0rd')
            ->shouldBeCalledOnce()
            ->willReturn($correctHmacHash);
        $snapshotWasCreated = SnapshotWasCreated::fromArray(
            self::snapshotEventPayload($oldestAllowedTimestamp),
            $userService->getWrappedObject()
        );

        $this->beConstructedThrough('reconstituteFrom', [new DomainEvents($snapshotWasCreated)]);
        $this->isVerified()->shouldBe($snapshotWasCreated->isVerified());
        $this->lastLoginDate()->getTimestamp()->shouldBe((int)$snapshotWasCreated->lastLoginTimestamp());
        $this->lastLoginFailureCode()->shouldBe($snapshotWasCreated->lastLoginFailureCode());
        $this->lastLoginFailureDate()->getTimestamp()->shouldBe((int)$snapshotWasCreated->lastLoginFailureTimestamp());
        $this->loginFailureCount()->shouldBe($snapshotWasCreated->loginFailureCount());
        $this->mran()->shouldBe($this->canonicalUserId());
        $this->userId()->shouldBe($snapshotWasCreated->aggregateId());
        $this->passwordHistory()->shouldHaveCount(\count($snapshotWasCreated->passwordHistory()));
        $this->wasDeleted()->shouldBe($snapshotWasCreated->wasDeleted());
        $this->nominatedMran()->shouldBe($snapshotWasCreated->canonicalUserId());
        $this->canonicalUserId()->shouldBe($snapshotWasCreated->canonicalUserId());
        $this->failedLoginTimestamps()->shouldBe($snapshotWasCreated->failedLoginTimestamps());
        $this->lastPasswordChangeFailedTimestamp()->shouldBe($snapshotWasCreated->lastPasswordChangeFailedTimestamp());
        $this->loginCount()->shouldBe(self::$loginCount);

        $anotherUser = $this->login($passwordValue, $userService);
        $anotherUser->shouldNotBeLike($this);
        $anotherUser->recordedEvents()->shouldContainType(UserHasLoggedIn::class);
    }

    function it_should_record_UserDeclaredActive_event(UserService $userService)
    {
        $domainEvents = [
            $userHasLoggedIn = UserDeclaredActive::fromArray(\json_decode(
                <<<JSON
                {
                    "event_name": "pldt-smart.deep.sso.user_declared_active",
                    "aggregate_id": "13028ac2-b374-4c91-99e4-eeceee6d1ebd",
                    "occurred_on": 1597936624,
                    "body": {
                        "envelope": [],
                        "payload": []
                    }
                }
                JSON,
                true
            ), $userService->getWrappedObject()),
        ];

        $domainEvents = new DomainEvents(...$domainEvents);
        $this->beConstructedThrough('reconstituteFrom', [$domainEvents]);
        $this->lastDeclaredActive()->getTimestamp()->shouldBe($userHasLoggedIn->occurredOn()->getTimestamp());
    }

    function it_should_create_UserWasUpdated_event_when_updating_last_declared_active_timestamp()
    {
        $anotherInstance = $this->declareActive();

        $anotherInstance->recordedEvents()->shouldContainType(UserDeclaredActive::class);
    }

    private static function setEventPropertyValue(DomainEvent $event, string $property, $value): void
    {
        $property = (new \ReflectionProperty(\get_class($event), $property));
        $property->setAccessible(true);
        $property->setValue($event, $value);
    }

    private static function snapshotEventPayload(string $oldestAllowedTimestamp): array
    {
        self::$loginCount = \mt_rand(100, 200);
        return \json_decode(
            \sprintf(
                '{
                    "event_name": "pldt-smart.deep.sso.snapshot_was_created",
                    "aggregate_id": "13028ac2-b374-4c91-99e4-eeceee6d1ebd",
                    "occurred_on": 1594285760.1441092,
                    "body": {
                        "envelope":[],
                        "payload":{ 
                            "mran": "09191111112",
                            "canonical_user_id": "09191234568",
                            "password_hash": "$argon2id$v=19$m=65536,t=4,p=1$MjI4enhOdWlqQVdQNkdmRw$up21SANf1UkUjZfrbdPZfgOVgSAMqSDNToKzV5\/cIAk",
                            "password_history": ["078eced1","02de89cf"],
                            "registeredOn": 1599919085.418094,
                            "last_login_date": 1599919085.418096,
                            "last_login_failure_date": 1599919085.418096,
                            "is_verified": true,
                            "is_last_password_changed": false,
                            "last_password_change_failed_timestamp": 1599919085.418096,
                            "was_deleted": false,
                            "failed_login_timestamps": [%f],
                            "login_failure_count": 1,
                            "last_login_failure_code": 0,
                            "login_count": %d,
                            "last_declared_active": 0
                        }
                    }
                }',
                $oldestAllowedTimestamp,
                self::$loginCount
            ), true
        );
    }

    function getMatchers(): array
    {
        return [
            'containLike' => function ($subject, $item) {
                foreach ($subject as $itemFromSubject) {
                    if ($itemFromSubject == $item) {
                        return true;
                    }
                }

                return false;
            },
            'containType' => function ($subject, $type) {
                foreach ($subject as $itemFromSubject) {
                    if (\get_class($itemFromSubject) === $type) {
                        return true;
                    }
                }

                return false;
            }
        ];
    }

    /**
     * @throws \SmartPldt\Deep\Sso\CanonicalUserId\InvalidCanonicalUserId
     * @throws \SmartPldt\Deep\Sso\Hash\InvalidHash
     * @throws \SmartPldt\Deep\Sso\InvalidUserId
     */
    private function createDomainEventsWithFiveFailedLogins(
        string $pwHash,
        UserService $userService
    ): DomainEvents {
        $tenMinuteOldTimestamp = \time() - 600;

        $events[] = $this->createUserWasRegisteredEvent($pwHash, $tenMinuteOldTimestamp, $userService);
        $events[] = $this->createUserFailedToLoggedIn(end($events), 60);
        $events[] = $this->createUserFailedToLoggedIn(end($events), 60);
        $events[] = $this->createUserFailedToLoggedIn(end($events), 60);
        $events[] = $this->createUserFailedToLoggedIn(end($events), 60);
        $events[] = $this->createUserFailedToLoggedIn(end($events), 60);

        return new DomainEvents(...$events);
    }
}

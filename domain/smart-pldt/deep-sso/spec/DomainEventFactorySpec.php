<?php

namespace spec\SmartPldt\Deep\Sso;

use PhpSpec\ObjectBehavior;
use SmartPldt\Deep\Sso\CanonicalUserId\{Min, PlainSha256};
use SmartPldt\Deep\Sso\{DomainEventFactory, Exception, UserFailedToLogin, UserService, UserWasRegistered};

/**
 * @mixin DomainEventFactory
 */
class DomainEventFactorySpec extends ObjectBehavior
{
    use ObjectBehaviorHelper;

    function let(UserService $userService)
    {
        $this->beConstructedWith($userService);
    }

    function it_accept_DomainEvent_signatures_by_event_name()
    {
        $anotherInstance = $this->addSignature(UserFailedToLogin::class);
        $anotherInstance->shouldNotBe($this);
    }

    function it_should_not_accept_a_signature_that_conflicts_with_existing_event_name()
    {
        $eventName = UserFailedToLogin::name();
        $anotherInstance = $this->addSignature(UserFailedToLogin::class);
        $anotherInstance
            ->shouldThrow(new Exception("A signature for `{$eventName}` already exists"))
            ->during('addSignature', [UserFailedToLogin::class]);
    }

    function it_should_create_a_DomainEvent_by_given_event_name_and_array(UserService $userService)
    {
        $userService->createCanonicalUserId(Min::create($min = '09191234567', $hash = PlainSha256::create()));
        $anotherInstance = $this->addSignature(UserWasRegistered::class);
        $domainEvent = $anotherInstance->create('pldt-smart.deep.sso.user_was_registered', [
            'event_name' => 'pldt-smart.deep.sso.user_was_registered',
            'aggregate_id' => 'de960b8a-1021-4189-9683-37ac9821fa20',
            'occurred_on' => 1594030687,
            'canonical_user_id_hash' => $hash,
            'body' => [
                'envelope' => [],
                'payload' => [
                    'canonical_user_id' => $min,
                    'password_hash' => '$argon2id$-eto-ay-totoong-password-hash',
                    'password_history' => [$this->randomCrc32Hash(), $this->randomCrc32Hash()],
                ]
            ],
        ]);
        $domainEvent->shouldHaveType(UserWasRegistered::class);
    }
}

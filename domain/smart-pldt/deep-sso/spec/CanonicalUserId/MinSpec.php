<?php

declare(strict_types=1);

namespace spec\SmartPldt\Deep\Sso\CanonicalUserId;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use SmartPldt\Deep\Sso\CanonicalUserId\{CanonicalUserId, Hash, HashFactory, InvalidCanonicalUserId, Min};

/**
 * @mixin Min
 */
class MinSpec extends ObjectBehavior
{
    private string $min;

    private string $hashValue;

    /** @var Hash */
    private $hash;

    function let(Hash $hash)
    {
        $this->hashValue = 'ang-hash-ng-buhay-mo';
        $this->hash = $hash;

        $hash->__toString()->willReturn($this->hashValue);

        $this->beConstructedThrough('create', [$this->min = '09181234569', $this->hash]);
    }

    function it_should_be_a_CanonicalUserId()
    {
        $this->shouldHaveType(CanonicalUserId::class);
    }

    function it_should_return_the_hash()
    {
        $this->hash()->shouldBe($this->hashValue);
    }

    function it_should_return_the_hash_through_the_hash_factory(HashFactory $hashFactory)
    {
        $this->beConstructedThrough('createThroughHashFactory', [$this->min, $hashFactory]);
        $hashFactory->hash(Argument::type(Min::class))->shouldBeCalledOnce()->willReturn($this->hash);
        $this->hash()->shouldBe($this->hashValue);
    }

    function it_should_accept_min_with_whitespaces_and_will_be_equal_to_its_canonical_value()
    {
        $this->beConstructedThrough('create', ['+63 918 123 4569', $this->hash]);
        $this->__toString()->shouldBe('09181234569');
    }

    function it_should_accept_min_with_hyphens_and_will_be_equal_to_its_canonical_value()
    {
        $this->beConstructedThrough('create', ['+63-918-123-4569', $this->hash]);
        $this->__toString()->shouldBe('09181234569');
    }

    function it_should_accept_min_with_country_code_without_plus_character_and_will_be_equal_to_its_canonical_value()
    {
        $this->beConstructedThrough('create', ['63 918 123 4569', $this->hash]);
        $this->__toString()->shouldBe('09181234569');
    }

    function it_should_throw_if_min_length_is_not_12_characters_when_prefix_is_639()
    {
        $this->beConstructedThrough('create', ['6391812345678', $this->hash]);
        $this->shouldThrow(InvalidCanonicalUserId::class)->duringInstantiation();
    }

    function it_should_throw_if_min_length_is_not_11_characters_when_prefix_is_09()
    {
        $this->beConstructedThrough('create', ['091812345678', $this->hash]);
        $this->shouldThrow(InvalidCanonicalUserId::class)->duringInstantiation();
    }

    function it_should_throw_an_error_if_prefix_is_not_valid()
    {
        $this->beConstructedThrough('create', ['999181234567', $this->hash]);
        $this->shouldThrow(InvalidCanonicalUserId::class)->duringInstantiation();
    }
}

<?php

declare(strict_types = 1);

namespace spec\SmartPldt\Deep\Sso\CanonicalUserId;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use SmartPldt\Deep\Sso\CanonicalUserId\{CanonicalUserId, Email, Hash, HashFactory, InvalidCanonicalUserId};

/**
 * @mixin Email
 */
class EmailSpec extends ObjectBehavior
{
    private string $email;

    private string $hashValue;

    /** @var Hash */
    private $hash;

    function let(Hash $hash)
    {
        $this->email = 'Roy@ROY.com';
        $this->hashValue = 'ang-hash-kong-buhay';

        $hash->__toString()->willReturn($this->hashValue);

        $this->beConstructedThrough('create', [$this->email, $this->hash = $hash]);
    }

    function it_should_be_a_CanonicalUserId()
    {
        $this->shouldHaveType(CanonicalUserId::class);
    }

    function it_should_be_equal_to_the_email_string_in_lower_case()
    {
        $this->__toString()->shouldBe(\strtolower($this->email));
    }

    function it_should_return_the_hash_through_the_hash_factory(HashFactory $hashFactory)
    {
        $hashFactory->hash(Argument::type(Email::class))->shouldBeCalledOnce()->willReturn($this->hash);

        $this->beConstructedThrough('createThroughHashFactory', [$this->email, $hashFactory]);

        $this->hash()->shouldBe($this->hashValue);
    }

    function it_should_return_the_hash()
    {
        $this->hash()->shouldBe($this->hashValue);
    }

    function it_should_not_accept_an_invalid_email()
    {
        $this->beConstructedThrough('create', [$invalidEmail = 'obviously-not-a-valid-email', $this->hash]);
        $this->shouldThrow(InvalidCanonicalUserId::class)->duringInstantiation();
    }
}

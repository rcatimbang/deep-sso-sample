<?php

namespace spec\SmartPldt\Deep\Sso\CanonicalUserId;

use PhpSpec\ObjectBehavior;
use SmartPldt\Deep\Sso\CanonicalUserId\{CanonicalUserId,
    CanonicalUserIdFactory,
    Email,
    Hash,
    HashFactory,
    InvalidCanonicalUserId,
    Min};
use PhpSpec\Wrapper\Collaborator;
use Prophecy\Argument;

/**
 * @mixin CanonicalUserIdFactory
 */
class CanonicalUserIdFactorySpec extends ObjectBehavior
{
    /** @var HashFactory */
    private $hashFactory;

    /** @var Hash */
    private $hash;

    function let(HashFactory $hashFactory, Hash $hash): void
    {
        $hash->__toString()->willReturn('ang_hash_ng_buhay_nating_lahat');
        $this->beConstructedWith($this->hashFactory = $hashFactory, Min::class, Email::class);

        $this->hashFactory->hash(Argument::type(CanonicalUserId::class))->willReturn($this->hash = $hash);
    }

    function it_should_create_a_CanonicalUserId_using_email()
    {
        $this->create('raymart@raymart.com')->shouldHaveType(Email::class);
    }

    function it_should_create_a_CanonicalUserId_using_min()
    {
        $this->hashFactory->hash(Argument::type(CanonicalUserId::class))->shouldBeCalled()->willReturn($this->hash);
        $this->create('09181234567')->shouldHaveType(Min::class);
    }

    function it_should_not_accept_an_unsupported_CanonicalUserId()
    {
        $this->hashFactory->hash(Argument::any())->shouldNotBeCalled();
        $this->beConstructedWith($this->hashFactory, Email::class);
        $this->shouldThrow(InvalidCanonicalUserId::class)->during('create', ['09181234567']);
    }
}

<?php

namespace spec\SmartPldt\Deep\Sso\Event;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use SmartPldt\Deep\Sso\Event\Exception;
use SmartPldt\Deep\Sso\Event\HandlesAnEventMessage;
use SmartPldt\Deep\Sso\Event\PurgeDeletedUser;
use SmartPldt\Deep\Sso\EventStore;
use SmartPldt\Deep\Sso\MinNotificationRecipient;
use SmartPldt\Deep\Sso\Notifier;
use SmartPldt\Deep\Sso\User;
use SmartPldt\Deep\Sso\UserId;
use SmartPldt\Deep\Sso\UserNotFoundError;
use SmartPldt\Deep\Sso\UserService;
use spec\SmartPldt\Deep\Sso\ObjectBehaviorHelper;

/**
 * @mixin PurgeDeletedUser
 */
class PurgeDeletedUserSpec extends ObjectBehavior
{
    use ObjectBehaviorHelper;

    private User $user;

    function let(EventStore $eventStore, UserService $userService, Notifier $notifier)
    {
        $this->user = $this->createObjectBuilderFor(User::class)
            ->set('userId', $this->randomUserId())
            ->set('canonicalUserId', $this->randomMin())
            ->build()
        ;
        $userService->findUserByUserId((string)$this->user->userId())->willReturn($this->user);
        $notifier->notify(Argument::that(self::stringValueIsLike($this->user->mran())), Argument::type('string'));
        $eventStore->purgeEventsMarkedDeletedByUserIds($this->user->userId());

        $this->beConstructedWith($eventStore, $userService, $notifier);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(HandlesAnEventMessage::class);
    }

    function it_should_return_the_event_name_it_handles()
    {
        $this->eventNameToHandle()->shouldBe('pldt-smart.deep.sso.user_was_deleted');
    }

    function it_should_not_accept_a_different_event_name()
    {
        $eventAsJson = \json_encode(['event_name' => 'ang-tatay-mo-kalbo']);
        $this->shouldThrow(Exception::class)->during('__invoke', [$eventAsJson]);
    }

    function it_should_notify_the_user_upon_purging_of_its_account(
        EventStore $eventStore,
        UserService $userService,
        Notifier $notifier
    ) {
        $this(self::eventAsJson($this->user->userId()));

        $userService->findUserByUserId((string)$this->user->userId())->shouldHaveBeenCalledOnce();
        $notifier
            ->notify(
                Argument::that(self::stringValueIsLike(MinNotificationRecipient::create((string)$this->user->mran()))),
                'Your GigaLife App account has been deleted. All linked accounts were also unlinked and informed.'
            )
            ->shouldHaveBeenCalledOnce()
        ;
        $eventStore->purgeEventsMarkedDeletedByUserIds($this->user->userId())->shouldHaveBeenCalledOnce();
    }

    function it_should_skip_notification_and_purging_if_user_cannot_be_found(
        EventStore $eventStore,
        UserService $userService,
        Notifier $notifier
    ) {
        $userService
            ->findUserByUserId((string)$this->user->userId())
            ->shouldBeCalledOnce()
            ->willThrow(new UserNotFoundError());
        $this(self::eventAsJson($this->user->userId()));

        $notifier->notify(Argument::cetera())->shouldNotHaveBeenCalled();
        $eventStore->purgeEventsMarkedDeletedByUserIds(Argument::cetera())->shouldNotHaveBeenCalled();
    }

    function it_should_rethrow_other_errors(UserService $userService)
    {
        $userService
            ->findUserByUserId((string)$this->user->userId())
            ->shouldBeCalledOnce()
            ->willThrow($expectedException = new \RuntimeException())
        ;

        $this->shouldThrow($expectedException)->during('__invoke', [self::eventAsJson($this->user->userId())]);
    }

    private static function eventAsJson(UserId $userId): string
    {
        return <<<JSON
        {
          "event_name": "pldt-smart.deep.sso.user_was_deleted",
          "aggregate_id": "{$userId}",
          "occurred_on": 1597243711,
          "body": {
            "envelope": [],
            "payload": []
          }
        }
        JSON;
    }

    private static function stringValueIsLike(object $valueOfObject): callable
    {
        return function (object $spiedObject) use ($valueOfObject) {
            return (string)$spiedObject == (string)$valueOfObject;
        };
    }
}

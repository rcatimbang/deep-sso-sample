<?php

declare(strict_types=1);

namespace spec\SmartPldt\Deep\Sso;

use PhpSpec\ObjectBehavior;
use SmartPldt\Deep\Sso\{UserId, InvalidUserId, AggregateId};

/**
 * @mixin UserId
 */
class UserIdSpec extends ObjectBehavior
{
    function it_is_an_AggregateId()
    {
        $this->shouldHaveType(AggregateId::class);
    }

    function it_should_throw_an_error_when_UserId_is_not_uuid()
    {
        $this->beConstructedThrough('fromString', ['this-is-invalid-uuid']);
        $this->shouldThrow(InvalidUserId::class)->duringInstantiation();
    }

    function it_should_return_the_user_id()
    {
        $this->beConstructedThrough('fromString', ['DE960B8A-1021-4189-9683-37AC9821FA20']);
        $this->__toString()->shouldBe('de960b8a-1021-4189-9683-37ac9821fa20');
    }
}

<?php

declare(strict_types = 1);

namespace spec\SmartPldt\Deep\Sso;

use PhpSpec\ObjectBehavior;
use SmartPldt\Deep\Sso\{DomainEvent, DomainEvents};

/**
 * @mixin DomainEvents
 */
class DomainEventsSpec extends ObjectBehavior
{
    /** @var DomainEvent */
    private $domainEvent;

    function let(DomainEvent $event)
    {
        $this->beConstructedWith($this->domainEvent = $event);
    }

    function it_is_an_IteratorAggregate()
    {
        $this->shouldHaveType(\IteratorAggregate::class);
    }

    function it_should_return_an_Iterator(DomainEvent $anotherEvent)
    {
        $iterator = $this->getIterator();
        $iterator->shouldHaveType(\ArrayIterator::class);
        $iterator->shouldContainThisEvent($this->domainEvent);
        $iterator->shouldNotContainThisEvent($anotherEvent);
    }

    function it_should_be_countable()
    {
        $this->shouldHaveType(\Countable::class);
    }

    function it_should_accept_an_event()
    {
        $this->shouldNotThrow(\Throwable::class)->duringInstantiation();
        $this->count()->shouldBe(1);
    }

    function it_should_return_an_array_of_events(DomainEvent $anotherEvent)
    {
        $this->beConstructedWith($this->domainEvent, $anotherEvent);

        $arrayOfEvents = $this->toArray();
        $arrayOfEvents[0]->shouldBe($this->domainEvent);
        $arrayOfEvents[1]->shouldBe($anotherEvent);
   }

   function getMatchers(): array
   {
       return [
           'containThisEvent' => function(\ArrayIterator $subject, $expectedEvent) {
               foreach ($subject as $event) {
                   if ($event == $expectedEvent) {
                       return true;
                   }
               }

               return false;
           },
           'notContainThisEvent' => function(\ArrayIterator $subject, $expectedEvent) {
               $eventsFound = 0;
               foreach ($subject as $event) {
                   $eventsFound += (int)($event == $expectedEvent);
               }

               return $eventsFound === 0;
           }
       ];
   }
}

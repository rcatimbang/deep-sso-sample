<?php

namespace spec\SmartPldt\Deep\Sso;

use PhpSpec\ObjectBehavior;
use SmartPldt\Deep\Sso\CanonicalUserId\CanonicalUserId;
use SmartPldt\Deep\Sso\CanonicalUserId\InvalidCanonicalUserId;
use SmartPldt\Deep\Sso\CanonicalUserId\Min;
use SmartPldt\Deep\Sso\CanonicalUserId\PlainSha256;
use SmartPldt\Deep\Sso\DomainEvent;
use SmartPldt\Deep\Sso\EncodedEventMismatch;
use SmartPldt\Deep\Sso\User;
use SmartPldt\Deep\Sso\UserHasLoggedIn;
use SmartPldt\Deep\Sso\UserId;
use SmartPldt\Deep\Sso\UserService;

/**
 * @mixin UserHasLoggedIn
 */
class UserHasLoggedInSpec extends ObjectBehavior
{
    use ObjectBehaviorHelper;

    private User $user;

    private CanonicalUserId $mran;

    private int $loginCount;

    function let()
    {
        $this->user = $this->createFakeInstanceOf(User::class);
        $this->setPropertyOf(
            $this->user,
            'userId',
            UserId::fromString('de960b8a-1021-4189-9683-37ac9821fa20')
        );
        $this->setPropertyOf(
            $this->user,
            'canonicalUserId',
            Min::create('09191234567', PlainSha256::create())
        );
        $this->setPropertyOf(
            $this->user,
            'mran',
            $this->mran = Min::create('09191234789', PlainSha256::create())
        );
        $this->setPropertyOf(
            $this->user,
            'loginCount',
            $this->loginCount = 1
        );

        $this->beConstructedThrough('create', [$this->user]);
    }

    function it_should_be_a_DomainEvent()
    {
        $this->shouldHaveType(DomainEvent::class);
    }

    function it_should_return_its_UserId()
    {
        $this->userId()->shouldBe($this->user->userId());
    }

    function it_should_return_the_DateTime_when_the_event_was_triggered()
    {
        $this->occurredOn()->shouldHaveType(\DateTimeInterface::class);
    }

    function it_should_return_the_event_name()
    {
        $this->name()->shouldBe('pldt-smart.deep.sso.user_has_logged_in');
    }

    function it_should_return_the_mran()
    {
        $this->mran()->shouldBe($this->mran);
    }

    function it_should_return_the_loginCount()
    {
        $this->loginCount()->shouldBe($this->loginCount);
    }

    function should_throw_an_error_if_event_name_is_incorrect(UserService $userService)
    {
        $encodedEventHavingDifferentName = [
            'event_name'=> 'pldt-smart.deep.sso.naiiba_talaga_event_name_nito',
        ];

        $this->beConstructedThrough('fromArray', [$encodedEventHavingDifferentName, $userService]);
        $this->shouldThrow(EncodedEventMismatch::class)->duringInstantiation();
    }

    function it_should_be_a_json_representation_of_the_event(UserService $userService)
    {
        $eventAsArray = [
            'event_name' => 'pldt-smart.deep.sso.user_has_logged_in',
            'aggregate_id' => 'de960b8a-1021-4189-9683-37ac9821fa20',
            'occurred_on' => 1594285760.1920312,
            'body' => [
                'envelope' => [],
                'payload' => [
                    'mran' => '09191234789',
                    'login_count' => 1
                ]
            ]
        ];

        $userService->createCanonicalUserId($eventAsArray['body']['payload']['mran'])->shouldBeCalledOnce()
            ->willReturn($min = Min::create($eventAsArray['body']['payload']['mran'], PlainSha256::create()));
        $this->beConstructedThrough('fromArray', [$eventAsArray, $userService]);
        $this->mran()->shouldBe($min);
        $this->loginCount()->shouldBe($eventAsArray['body']['payload']['login_count']);
        $this->occurredOnTimestamp()->shouldBe($eventAsArray['occurred_on']);
        $this->__toString()->shouldBe(\json_encode($eventAsArray));
    }

    function it_should_accept_old_event_payload(UserService $userService)
    {
        $eventAsArray = [
            'event_name' => 'pldt-smart.deep.sso.user_has_logged_in',
            'aggregate_id' => 'de960b8a-1021-4189-9683-37ac9821fa20',
            'occurred_on' => 1594285760.1920312,
            'body' => [
                'envelope' => [],
                'payload' => []
            ]
        ];

        $this->beConstructedThrough('fromArray', [$eventAsArray, $userService]);
        $this->mran()->shouldHaveType(InvalidCanonicalUserId::class);
    }
}
